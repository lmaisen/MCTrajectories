# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definition of function to load configuration file,
and import of definition of experimental counter (time) delays to be used in Monte Carlo
simulations.
"""

# pyha
import pyha.config

load_config_file = pyha.config.load_config_file

# Import delay definitions
from pyha.def_delays import *
