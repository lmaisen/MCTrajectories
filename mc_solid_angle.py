# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 16:45:30 2015

@author: Lothar Maisenbacher/MPQ

Monte Carlo integration to determine solid angle through apertures and/or blocked
by apertures.
"""

import numpy as np
import random
import matplotlib.pylab as plt
import scipy.constants
import scipy.optimize
import scipy.integrate

import random_traj

# Seed random number generator, either using system time or OS-dependent sources
# For Windows, CryptGenRandom is used
random.seed()

## Create random trajectories
def random_xy_pos(params):
    """Random position on source."""
    if params['SourceType'] == 'RectangularEdges':
        return random_xy_pos_rect_edges(params)
    elif params['SourceType'] == 'Circular':
        return random_xy_pos_circular(params)
    else:
        raise Exception(f'Unknown source type \'{params["SourceType"]}\'')

def max_source_ext(params):
    """"Max. transverse extension of source."""
    if params['SourceType'] == 'RectangularEdges':
        width = params['SourceWidth']
        height = params['SourceHeight']
    elif params['SourceType'] == 'Circular':
        width = 2*params['SourceRadius']
        height = 2*params['SourceRadius']
    else:
        raise Exception(f'Unknown source type \'{params["SourceType"]}\'')

    return width, height

def random_xy_pos_circular(params):
    """"Random position on circular source."""
    radius = params['SourceRadius']
    condition = True
    while condition:
        x = random.uniform(-radius, radius)
        y = random.uniform(-radius, radius)
        condition = x**2 + y**2 > radius**2
    return x, y

def random_xy_pos_rect_edges(params):
    """"Random position on edges of rectangular source."""
    a = params['SourceWidth']
    b = params['SourceHeight']
    length_weighting = a/(a+b)
    random_edge = random.uniform(0, 1)
    if random_edge > length_weighting:
        x = a/2 * np.sign(random.uniform(-1, 1))
        y = random.uniform(-b/2, b/2)
    else:
        x = random.uniform(-a/2, a/2)
        y = b/2 * np.sign(random.uniform(-1, 1))
    return x, y

def pos_in_aperture(params, key_prefix, pos):
    """Check whether position is within aperture."""
    if params[f'{key_prefix}Type'] == 'Rectangular':
        width = params[f'{key_prefix}Width']
        height = params[f'{key_prefix}Height']
        condition = (
            (pos[0]**2 >= (width/2)**2)
            or (pos[1]**2 >= (height/2)**2))
    elif params[f'{key_prefix}Type'] == 'Circular':
        radius = params[f'{key_prefix}Radius']
        condition = pos[0]**2 + pos[1]**2 > radius**2
    else:
        raise Exception(f'Unknown source type \'{params["SourceType"]}\'')

    return condition

def vector_thru_aperture(params, key_prefix):

    width = params[f'{key_prefix}Width']
    height = params[f'{key_prefix}Height']
    dist = params[f'{key_prefix}Dist']
    pass_ = params.get(f'{key_prefix}Pass', True)

    # Random angle
    # Calculate the maximum angle which passes through the aperture
    source_width, source_height = max_source_ext(params)
    theta_max = np.arctan(
        np.max([
            source_width+width,
            source_height+height])
        /2/dist
        )
    condition = pass_
    num_trials = 0
    while condition == pass_:
        pos_source = random_xy_pos(params)
        theta_source = random_traj.random_theta_cos_law(theta_max)
        phi_source = random.uniform(0, 2*np.pi)
        transverse_dist = (
            np.array([np.cos(phi_source), np.sin(phi_source)]) * np.tan(theta_source))
        pos_aperture = transverse_dist * dist + pos_source
        condition = pos_in_aperture(params, key_prefix, pos_aperture)

        num_trials += 1

    return pos_source, theta_source, phi_source, pos_aperture, num_trials, theta_max

def vector_thru_apertures(params):

    width_1 = params['Aperture1Width']
    height_1 = params['Aperture1Height']
    dist_1 = params['Aperture1Dist']
    pass_1 = params.get('Aperture1Pass', True)
    width_2 = params['Aperture2Width']
    height_2 = params['Aperture2Height']
    dist_2 = params['Aperture2Dist']
    pass_2 = params.get('Aperture2Pass', True)

    # Random angle
    # Calculate the maximum angle which passes through the apertures
    source_width, source_height = max_source_ext(params)
    theta_max_1 = np.arctan(
        np.max([
            source_width+width_1,
            source_height+height_1])
        /2/dist_1
        )
    theta_max_2 = np.arctan(
        np.max([
            source_width+width_2,
            source_height+height_2])
        /2/dist_2
        )
    # Use the maximum angle, not the minimum, since we here want to know the
    # separate probabilities to pass through both apertures and the combined
    # probability.
    # If only the combined probability is of interest, one may use the minimum
    # angle.
    theta_max = np.max([theta_max_1, theta_max_2])
    condition1 = pass_1
    condition2 = pass_2
    iC1 = 0
    iC2 = 0
    num_trials = 0
    while (condition1 == pass_1 or condition2 == pass_2):
        pos_source = random_xy_pos(params)
        theta_source = random_traj.random_theta_cos_law(theta_max)
        phi_source = random.uniform(0, 2*np.pi)
        trajAngles = np.array([np.cos(phi_source), np.sin(phi_source)]) * np.tan(theta_source)
        posAperture1 = trajAngles * dist_1 + pos_source
        posAperture2 = trajAngles * dist_2 + pos_source
        condition1 = pos_in_aperture(params, 'Aperture1', posAperture1)
        iC1 += not condition1
        condition2 = pos_in_aperture(params, 'Aperture2', posAperture2)
        iC2 += not condition2
        num_trials += 1

    return (
        pos_source, theta_source, phi_source, posAperture1, posAperture2,
        num_trials, iC1, iC2, theta_max)

def calc_fraction_thetas(params):
    """
    Calculate the fraction of atoms taken into account in the simulation,
    assuming the cosine emission law.
    """
    # Fraction of angular distribution included in simulation
    angular_dist = lambda theta: np.sin(theta)*np.cos(theta)
    prob_angle_total = scipy.integrate.quad(
        angular_dist, 0, np.pi/2)[0]
    prob_angle_trajs = scipy.integrate.quad(
        angular_dist, 0, params['ThetaMax'])[0]

    # Fraction of trajectories making it into spectroscopy region
    return prob_angle_trajs/prob_angle_total

## Define test routines
def test_xy_pos(params):
    # Test seeding of random position

    nTraj = params['NTrajs']
    XYpos = np.zeros((nTraj, 2))
    for i in range(nTraj):
        XYpos[i] = random_xy_pos(params)
    plt.figure('Trajectory test: Source spatial distribution')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(XYpos[:, 0], XYpos[:, 1])
    plt.xlabel('x (mm)')
    plt.ylabel('y (mm)')

def testVectorThruAperture(params):

    nTraj = params['NTrajs']

    nums_trials_1 = 0
    nums_trials_2 = 0
    nums_trials_12 = 0
    nums_trials_12_1 = 0
    nums_trials_12_2 = 0
    xy_1 = np.zeros((nTraj, 2))
    xy_2 = np.zeros((nTraj, 2))
    xy_12_1 = np.zeros((nTraj, 2))
    xy_12_2 = np.zeros((nTraj, 2))
    theta = np.zeros((nTraj, 1))
    for i in range(nTraj):

        pos_source_1, _, _, pos_aperture_1, num_trials_1, theta_max_1 = vector_thru_aperture(
            params, 'Aperture1')
        _, _, _, pos_aperture_2, num_trials_2, theta_max_2 = vector_thru_aperture(
            params, 'Aperture2')

        (posNozzle, thetaNozzle, phiNozzle, pos_aperture_12_1, pos_aperture_12_2,
         num_trials_12, num_trials_12_1, num_trials_12_2, theta_max) = (
            vector_thru_apertures(
                params))

        xy_1[i] = pos_aperture_1
        xy_2[i] = pos_aperture_2
        xy_12_1[i] = pos_aperture_12_1
        xy_12_2[i] = pos_aperture_12_2
        theta[i] = thetaNozzle
        nums_trials_1 += num_trials_1
        nums_trials_2 += num_trials_2
        nums_trials_12 += num_trials_12
        nums_trials_12_1 += num_trials_12_1
        nums_trials_12_2 += num_trials_12_2

    params['ThetaMax'] = theta_max
    fraction_thetas = calc_fraction_thetas(params)

    print(
        f'Fraction of trajectories included in emission cone: {fraction_thetas:.2e}')

#    print(
#        f'Total trials aperture 1 by itself: {nums_trials_1:.2e}')
    print(
        f'Trial probability aperture 1 by itself: {nTraj/nums_trials_1:.2e}')
    print(
        f'Total probability aperture 1 by itself: {fraction_thetas*nTraj/nums_trials_1:.2e}')

#    print(
#        f'Total trials aperture 2 by itself: {nums_trials_2:.2e}')
    print(
        f'Trial probability aperture 2 by itself: {nTraj/nums_trials_2:.2e}')
    print(
        f'Total probability aperture 2 by itself: {fraction_thetas*nTraj/nums_trials_2:.2e}')

#    print(
#        f'Total trials apertures 1 and 2: {nums_trials_12:.2e}')
    print(
        f'Trial probability apertures 1 and 2: {nTraj/nums_trials_12:.2e}')
    print(
        f'Trial probability aperture 1 for combined apertures: {nTraj/nums_trials_12_1:.2e}')
    print(
        f'Trial probability aperture 2 for combined apertures: {nTraj/nums_trials_12_2:.2e}')

    print(
        f'Total probability apertures 1 and 2: {fraction_thetas*nTraj/nums_trials_12:.2e}')

    plt.figure('Trajectory test: Spatial distribution through aperture 1 by itself')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(xy_1[:, 0], xy_1[:, 1])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')

    plt.figure('Trajectory test: Spatial distribution through aperture 2 by itself')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(xy_2[:, 0], xy_2[:, 1])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')

    plt.figure('Trajectory test: Spatial distribution through aperture 1')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(xy_12_1[:, 0], xy_12_1[:, 1])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')

    plt.figure('Trajectory test: Spatial distribution through aperture 2')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(xy_12_2[:, 0], xy_12_2[:, 1])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')

    plt.figure('Trajectory test: Angular distribution through aperture 1')
    plt.clf()
    _ = plt.hist(
        theta, bins=50)
    plt.xlabel('theta (rad)')
    plt.ylabel('counts')

    plt.figure('Trajectory test: Angular distribution through aperture 2')
    plt.clf()
    _ = plt.hist(
        theta, bins=50)
    plt.xlabel('theta (rad)')
    plt.ylabel('counts')


## Do Monte Carlo integration
num_trajs = 1000

# Probability of photons scattered on variable aperture
# to hit inside of detector, i.e. passing first atomic beam
# aperture to the inside of the inner region, then *not*
# passing the second atomic beam aperture, but hitting the wall.
params = {
    'NTrajs': num_trajs,
    'SourceType': 'RectangularEdges',
    'SourceWidth': 2,
    'SourceHeight': 2,
    'Aperture1Type': 'Rectangular',
    'Aperture1Width': 16.5,
    'Aperture1Height': 7.,
    'Aperture1Dist': 204-153.6-26.2,
    'Aperture1Pass': True,
    'Aperture2Type': 'Rectangular',
    'Aperture2Width': 16.5,
    'Aperture2Height': 7.,
    'Aperture2Dist': 2*26.2+204-153.6-26.2,
    'Aperture2Pass': False,
    }

# Atomic beam
#params = {
#    'NTrajs': num_trajs,
#    'SourceType': 'Circular',
#    'SourceRadius': 1.,
#    'Aperture1Type': 'Circular',
#    'Aperture1Width': 2.4,
#    'Aperture1Height': 2.4,
#    'Aperture1Radius': 1.2,
#    'Aperture1Dist': 69.0,
#    'Aperture2Type': 'Rectangular',
#    'Aperture2Width': 1.2,
#    'Aperture2Height': 2.,
#    'Aperture2Radius': 1.2,
#    'Aperture2Dist': 153.6,
#    }

test_xy_pos(params)
testVectorThruAperture(params)
