# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 21:30:06 2015

@author: Lothar Maisenbacher/MPQ

Function for generating 2S trajectory sets.
Takes into account 1S-2S excitation using two-level OBEs.
"""

import scipy.integrate
import scipy.constants
import scipy.optimize
import scipy.stats
import numpy as np
import random
import time
import os

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# Load OBEs
import cython_obe.cOBE_1S2S as OBE_1S2S # Using cythonized 1S-2S OBEs
import random_traj
import traj_settings

def get_traj_filename(param, ext=None, legacy=False):
    """
    Generate filename for trajectory set with metadata `param` (dict or pandas Series).
    `ext` (str, default is None) gives the filename extension.
    If `legacy` (bool) is False (default), the filename is the `TrajUID`, as defined by
    `param['TrajUID']`, and if `ext` is None it is replaced with 'npz'.
    If `legacy` is True, a verbose filename is used, and if `ext` is None it is replaced with `gz`.
    """
    if not legacy:
        ext = 'npz' if ext is None else ext
        filebase = param['TrajUID']
    else:
        ext = 'gz' if ext is None else ext
        filebase = (
            '{} {} 2S traj {:.0f}mW dnua={:.0f}Hz {:.1f}M Aper{}x{} v{:.1f}{} {:.1f}K {}'
            .format(
                param['Timestamp'].strftime('%Y-%m-%d %H-%M-%S'),
                param['Isotope'],
                param['LaserPower']*1e3,
                param['AtomicDetuning'],
                param['NTrajs']*1e-6,
                param['Aperture2Width']*1e3,
                param['Aperture2Height']*1e3,
                param['SpeedDistExp'],
                ' vcut{:.0f}'.format(
                    param['SpeedDistExpSupprCutOff'])
                    if param['SpeedDistExpSupprCutOff'] != 0 else '',
                param['NozzleTemp'],
                param['TrajUID']
                ))
    filename = filebase+'.'+ext
    return filename, filebase, ext

def gen_traj_set(param, legacy=False):
    """
    Generate trajectory set with metadata `param` (dict or pandas Series).
    If legacy (bool, default is False) is set to True, the generated trajectory set is also saved as
    a gzipped CSV file.
    """
    # Seed random number generator,
    # using random number supplied by operating system
    seed = int.from_bytes(os.urandom(2500), 'big')
    random.seed(seed)

    # Copy series param to modify it here
    param = param.copy()

    # For all delays
    myOBE1s2s = OBE_1S2S.OBE_1S2S(param['NucleusMass'])
    myOBE1s2s.atomic_detuning = float(param['AtomicDetuning'])
    myOBE1s2s.w0 = float(param['LaserWaistRadius'])
    myOBE1s2s.power_laser = float(param['LaserPower'])
    myOBE1s2s.z_waist = float(param['LaserWaistPos'])

    # Switches to turn off certain physics
    #myOBE1s2s.ionizationOn = 0
    #myOBE1s2s.dopplerOn = 0
    #myOBE1s2s.ACStarkOn = 0

    detection_dist = param['DetectionDist']
    chopper_freq = param['ChopperFreq']

    # Load delays
    delays = traj_settings.delay_sets[param['DelaySetID']]
    # Use only analysis delays
    analysis_delays = {
        delay_id: delay for delay_id, delay in delays.items() if delay['Analysis']}

    # Delays to use in numerical integration
    dt = param['IntDelayWidth']
    delays_min_start_time = np.min([
        delay['StartTime'] for _, delay in analysis_delays.items()])
    delays_max_end_time = np.max([
        delay['EndTime'] for _, delay in analysis_delays.items()])
    num_int_delays = int(np.ceil((delays_max_end_time-delays_min_start_time)/dt))
    int_delays_end_time = np.linspace(
        delays_min_start_time+dt, delays_max_end_time, num_int_delays)
    int_delays_start_time = int_delays_end_time-dt

    # Time at which trajectory starts, with zero time corresponding to position of
    # nozzle output orifice
    t_begin = 0

    # Initial density matrix, all population in 1S level
    initial = [1, 0, 0, 0]

    start_time = time.time()
    run_time_ind = 0
    num_delays_int_ind = 0
    num_trajs = int(param['NTrajs'])
    num_trials = 0
    num_multiple_chopper_cycles = 0
    velList = np.zeros((num_trajs, 3))
    posList = np.zeros((num_trajs, 3))
    exc_probs = np.zeros((num_trajs, num_int_delays))
    ioni_probs = np.zeros((num_trajs, num_int_delays))
    for i in range(num_trajs):

        pos_vector, vel_vector, vel_magnitude, trials, theta_max = random_traj.random_traj(
            param['NozzleRadius'], param['Aperture1Width'], param['Aperture1Height'],
            param['Aperture2Width'], param['Aperture2Height'], param['Aperture1Dist'],
            param['Aperture2Dist'], param['NozzleTemp'],
            speed_dist_exp=param['SpeedDistExp'], exp_suppr_cutoff=param['SpeedDistExpSupprCutOff'],
            atomic_mass=param['AtomicMass']
            )
        num_trials += trials

        velList[i, :] = vel_vector
        posList[i, :] = pos_vector
        myOBE1s2s.velVector = vel_vector
        myOBE1s2s.startVector = pos_vector

        # Time at which trajectory crosses 2S-nP laser after leaving the nozzle
        t_detect = detection_dist/vel_vector[2]

        # Neglecting the effect of very slow trajectories seeing more than one chopper cycle,
        # i.e. only last chopper cycle included
        if t_detect  > 1/chopper_freq:
            num_multiple_chopper_cycles += 1

        # Generate random time within integration delay constraints
        taus_random = np.array([
            random.uniform(int_delays_start_time[i], int_delays_end_time[i])
            for i in range(num_int_delays)])
        ts_in_laser = t_detect-taus_random

        # Density matrices for each integration delay
        rhos = np.zeros((len(taus_random), 4))

        # Integrate delays for which trajectories start when laser is on, i.e., at
        # t <= 0. These delays can be integrated in one calculation, as the start time
        # is identical for all and only the end time of the integration differs.

        mask_starts_in_laser = (taus_random >= t_detect-1/chopper_freq/2)

        if np.sum(mask_starts_in_laser) > 0:

            # Build integration times, always integrating for t > t_begin
            ts_to_integrate = np.flipud(ts_in_laser[mask_starts_in_laser])
            ts_to_integrate[ts_to_integrate < t_begin] = t_begin
            ts_to_integrate = np.insert(ts_to_integrate, 0, t_begin)

            ts_to_integrate_unique, unique_indices = np.unique(
                ts_to_integrate, return_inverse=True)
            odeint_results_unique = scipy.integrate.odeint(
                myOBE1s2s.derivs, initial,
                ts_to_integrate_unique,
                atol=1e-8, rtol=1e-8,
                )
            odeint_results = odeint_results_unique[unique_indices]

            # Flip results back to delay order and remove time zero
            rhos[mask_starts_in_laser] = np.flipud(odeint_results[1:, :])

        # Integrate delays for which laser is switched on after trajectory has left
        # nozzle, i.e., for t > 0. Here, every delay has to be integrated individually,
        # as both start and end time are different for each delay

        if np.sum(~mask_starts_in_laser) > 0:

            start_time_ind = time.time()

            # logger.info(
            #     'Integrating %d delay(s) '
            #     + 'individually for trajectory with vz = %.2f m/s',
            #     np.sum(~mask_starts_in_laser, vel_vector[2])
            #     )

            for i_tau, tau in enumerate(taus_random[~mask_starts_in_laser]):

                t_max = t_detect-tau
                t_min = np.max([
                    0, t_max-1/(2*chopper_freq)
                    ])
                ts_to_integrate = np.array([
                    t_min, t_max])

                odeint_result = scipy.integrate.odeint(
                    myOBE1s2s.derivs, initial,
                    ts_to_integrate,
                    atol=1e-8, rtol=1e-8,
                    )
                rhos[i_tau] = odeint_result[-1]

                num_delays_int_ind += 1

            run_time_ind += time.time()-start_time_ind

        # Decay over time not spent in laser beam
        exc_prob = myOBE1s2s.decayOnly(rhos[:, 3], taus_random)
        # Ionization probabiliy is equal to population loss from system
        ioni_probs[i, :] = 1 - (rhos[:, 0] + rhos[:, 3])
        exc_probs[i, :] = exc_prob

    param['TrialProb'] = 1/(num_trials/num_trajs)
    param['ThetaMax'] = theta_max
    param['NMultipleChopperCycles'] = num_multiple_chopper_cycles
    param['FractionTrajsSpectroscopyRegion'] = (
        random_traj.calc_fraction_spectroscopy_region(param))

    # Built experimental delays to export
    int_delays_start_ind = np.array([
        np.argmin(np.abs(int_delays_start_time-delay['StartTime']))
        for _, delay in analysis_delays.items()])
    int_delays_end_ind = np.array([
        np.argmin(np.abs(int_delays_end_time-delay['EndTime']))
        for _, delay in analysis_delays.items()])
    # Number of integration delays per experimental delay
    num_int_delays_per_delay = int_delays_end_ind-int_delays_start_ind+1
    param['NMinIntDelays'] = np.min(
        num_int_delays_per_delay)

    exp_exc_probs = np.zeros((num_trajs, len(analysis_delays)))
    exp_ioni_probs = np.zeros((num_trajs, len(analysis_delays)))
    for i_delay in range(len(analysis_delays)):
        exp_exc_probs[:, i_delay] = np.sum(
            exc_probs[:, int_delays_start_ind[i_delay]:int_delays_end_ind[i_delay]+1],
            axis=1)
        exp_ioni_probs[:, i_delay] = np.sum(
            ioni_probs[:, int_delays_start_ind[i_delay]:int_delays_end_ind[i_delay]+1],
            axis=1)

    logger.info('Computation took %.2f s', time.time()-start_time)
    logger.info(
        'Calculated %d delay(s) individually'
        + ', taking %.2f s in total',
        num_delays_int_ind, run_time_ind
        )
    logger.info('%d trajectories saw more than one chopper cycle', num_multiple_chopper_cycles)
    avg_exp_ext_probs = (
        np.mean(exp_exc_probs, axis=0)/num_int_delays_per_delay)
    logger.info(
        'Avg. excitation probabilities for exp. delays: %s',
        ', '.join([f'{elem:.2e}' for elem in avg_exp_ext_probs]))
    avg_exp_ioni_probs = (
        np.mean(exp_ioni_probs, axis=0)/num_int_delays_per_delay)
    logger.info(
        'Avg. ionization probabilities for exp. delays: %s',
        ', '.join([f'{elem:.2e}' for elem in avg_exp_ioni_probs]))

    # Write output
    header = []
    header.append(
        'Monte Carlo simulation of trajectories of 2S atoms, geometry {}'.format(param['Geometry'])
        +' (by Lothar Maisenbacher/MPQ, 05.08.2020)')
    header.append(
        'File created on {}, UID {}'
        .format(param['Timestamp'].strftime('%Y-%m-%d %H-%M-%S'), param.name))
    header.append(
        'Number of trajectories: {:d}, delay set ID: {}, width of integration delays: {:.0f} µs'
        .format(num_trajs, param['DelaySetID'], 1e6*param['IntDelayWidth']))
    header.append(
        'Beam model: Cosine law, speed distribution: P(v) ~ v^{:.1f} Exp[-v^2] Exp[-{:.1f}/v]'
        .format(param['SpeedDistExp'], param['SpeedDistExpSupprCutOff']))
    header.append('243 nm laser power per direction: {:.3f} W'.format(param['LaserPower']))
    header.append('Atomic detuning: {:.0f} Hz'.format(param['AtomicDetuning']))
    header.append('243 nm laser cavity waist radius: {:.6e} m'.format(param['LaserWaistRadius']))
    header.append(
        '243 nm waist position relative to nozzle orifice: {:.6e} m'.format(myOBE1s2s.z_waist))
    header.append('Nozzle orifice radius: {:.6e} m'.format(param['NozzleRadius']))
    header.append(
        'Rectangular front aperture: Position relative to nozzle orifice:'
        +' {:.6e} m, Height: {:.6e} m, Width: {:.6e} m'.format(
            param['Aperture1Dist'], param['Aperture1Height'], param['Aperture1Width']))
    header.append(
        'Rectangular back aperture: Position relative to nozzle orifice:'
        +' {:.6e} m, Height: {:.6e} m, Width: {:.6e} m'.format(
            param['Aperture2Dist'], param['Aperture2Height'], param['Aperture2Width']))
    header.append(
        'Detection point relative to nozzle orifice: {:.6e} m'.format(param['DetectionDist']))
    header.append(
        'Nozzle temperature: {:.2f} K, isotope {}, nucleus mass: {:.10f} u, atomic mass: {:.10f} u'
        .format(param['NozzleTemp'], param['Isotope'], param['NucleusMass'], param['AtomicMass']))
    header.append('Chopper frequency: {:.2f} Hz'.format(param['ChopperFreq']))
    columnNames = (
        ['x', 'y', 'z', 'vx', 'vy', 'vz']
        + ['{:.0f}-{:.0f}us'.format(delay['StartTime']*1e6, delay['EndTime']*1e6)
           for _, delay in analysis_delays.items()])
    header.append(' '.join(columnNames))
    header = '\n'.join(header)
    if legacy:
    # Save results to text file (legacy)
        np.savetxt(
            os.path.join(
                param['Folder'], os.path.splitext(param['Filename'])[0]+'.gz'),
            np.hstack((posList, velList, exp_exc_probs)),
            header=header,
            fmt='%.8e')
    # Save results to compressed NumPy archive
    np.savez_compressed(
        os.path.join(
            param['Folder'],
            os.path.splitext(param['Filename'])[0]),
        **{
            'TrajUID': param.name,
            'Positions': posList,
            'Vels': velList,
            '2SExcProbs_IntDelaySum': exp_exc_probs,
            'IoniProbs_IntDelaySum': exp_ioni_probs,
            'Seed': seed,
            'Param': dict(param),
            }
        )

    return param, myOBE1s2s
