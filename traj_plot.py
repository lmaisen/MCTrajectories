# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 11:52:58 2015

@author: Lothar Maisenbacher/MPQ

Plot analysis of 2S trajectory sets, such as excitation probability and mean or FWHM velocity
versus detuning of the 1S-2S laser.
"""

import numpy as np
import matplotlib.pylab as plt
import pandas as pd
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()
import pyhs.fit

# pyha
import pyha.plot
import pyha.trajs

import pyha.plts as plts
plts.set_params()

import traj_settings
config = traj_settings.load_config_file()
dir_metadata = config['MonteCarloTrajectories']['DirMetadataInput']

myFit = pyhs.fit.Fit()

Trajs = pyha.trajs.Trajs()

#%% Load results

# filename_prefix = '2020-06-30 H 900-1700mW dnua=-8000-10000Hz 1.0M Aper1.2x2.0 v3.0 vcut65 4.8K'
# filename_prefix = '2021-07-28 10-39-22 2021-07-27 21-24-40 H 1.1W'
filename_prefix = '2022-06-23 20-00-24 H 2S traj 1100mW dnua=970Hz 0.0M Aper1.2x2.0 v3.0 vcut65 4.8K Ym0zaavy.gz'

Trajs.load_trajs_metadata(Path(dir_metadata, filename_prefix + ' Params.dat'))
Trajs.load_trajs_analysis_results(Path(dir_metadata, filename_prefix + ' Analysis.dat'))
dfTrajParams = Trajs.dfTrajParams
dfTrajAnalysis = Trajs.dfTrajAnalysis
# Merge of `dfTrajParams` and `dfTrajAnalysis`
dfTrajsAnalysis = Trajs.dfTrajsAnalysis

#%% Fit and plot data to find optimal laser detuning
# Define delays to be fitted with delayListList
# Define beam waists to be fitted with w0List
# Define powers to be fitted with pList

dfTrajsAnalysis.sort_values(
    by='AtomicDetuning', inplace=True)

active_delay_ids = [str(elem) for elem in range (13, 17)]
#active_delay_ids = ['2', '10', '13', '16']
active_delay_ids = ['13']
mask = pd.Series(True, index=dfTrajsAnalysis.index)
mask = (
    mask
    & (dfTrajsAnalysis['DelayID'].isin(active_delay_ids))
#    & (dfTrajsAnalysis['LaserPower'].isin([1.1]))
    )

plot_params = {
    'NumAxes': 4,
    'NumRows': 2,
    'FigName': '1S-2S trajectory analysis',
    'FigSize': [plts.pagewidth*1, 3.5],
    'ShareX': True,
    'LegParams': {'fontsize': plts.params['FontsizeMed'], 'ncol': 2},
    'ShowTitle': False,
    }
plot_params['AxParams0'] = {
    'ShowLegend': True,
#    'XLim': [0, 2000],
    }
plot_params['AxParams1'] = {
    'ShowLegend': False,
    'CustomXLabel': '1S-2S atomic detuning (Hz)',
    }
plot_params['AxParams2'] = {
    'ShowLegend': False,
    }
plot_params['AxParams3'] = {
    'ShowLegend': False,
    'CustomXLabel': '1S-2S atomic detuning (Hz)',
    }

fig_traj = pyha.plot.PlotGen(plot_params)
ax1 = fig_traj.axs[0]
ax2 = fig_traj.axs[2]
ax3 = fig_traj.axs[1]
ax4 = fig_traj.axs[3]

isotope = np.unique(dfTrajParams['Isotope'])[0]

#ax1.set_title('Longitudinal %s 2S velocity distribution mean\nfor P = %.2f W, delay %i to %i'%(isotope, pList[0], np.min(delayList), np.max(delayList)), fontsize=16)
#ax3.set_title('Transverse %s 2S velocity distribution FWHM\nfor P = %.2f W, delay %i to %i'%(isotope, pList[0], np.min(delayList), np.max(delayList)), fontsize=16)
#ax2.set_title('%s 2S excitation probability (not weighted)\nfor P = %.2f W, delay %i to %i'%(isotope, pList[0], np.min(delayList), np.max(delayList)), fontsize=16)
#ax4.set_title(
#    '{} 2S excitation probability '.format(isotope) + \
#    r'(weighted with $1/v_z$)' + \
#    ('\nfor P = {:.2f} W, delay {:d} to {:d}'.format(pList[0], np.min(delayList), np.max(delayList))),
#    fontsize=16)

#plt.tight_layout(pad=1, w_pad=2, h_pad=2)
#plt.subplots_adjust(top=0.88)
#width_scaling_leg = 0.6
#width_scaling = 1
#box = ax1.get_position()
#ax1.set_position([box.x0, box.y0, box.width * width_scaling_leg, box.height])
#box = ax2.get_position()
#ax2.set_position([box.x0, box.y0, box.width * width_scaling, box.height])
#box = ax3.get_position()
#ax3.set_position([box.x0, box.y0, box.width * width_scaling_leg, box.height])
#box = ax4.get_position()
#ax4.set_position([box.x0, box.y0, box.width * width_scaling, box.height])

# Init empty DataFrame
df_traj_detunings = pd.DataFrame()

columns = ['LaserWaistRadius', 'LaserPower', 'DelayID', 'SpeedDistExp']
df_traj_unique = dfTrajsAnalysis[mask].drop_duplicates(subset=columns)
df_traj_unique['DelayIDInt'] = [int(elem) for elem in df_traj_unique['DelayID']]
df_traj_unique.sort_values(by='DelayIDInt', inplace=True)
leg_handles = {}
leg_labels = {}
for i_unique_param, unique_param in df_traj_unique.iterrows():
    mask_unique = mask.copy()
    for column in columns:
        mask_unique &= (dfTrajsAnalysis[column]==unique_param[column])

    x = dfTrajsAnalysis[mask_unique]['AtomicDetuning'].values
    x_fit = np.arange(np.min(x), np.max(x), 0.1)

    fits_gaussian = {}
    for i_plot, (param_name, param_column, is_fit_result, peak_invert, label, ax, y_scale) in enumerate(zip(
            [
             'Vel_z_2S_Gaussian_CFR',
             'Vel_x_2S_Gaussian_GammaG',
             'ExcProbPerTraj',
             'ExcProbPerInterTraj',
            ],
            [
             'Vel_z_2S_Gaussian_CFR',
             'Vel_x_2S_Gaussian_GammaG',
             'ExcProbPerTraj',
             'ExcProbPerInterTraj',
            ],
            [True, True, False, False],
            [True, True, False, False],
            [
             'Mean of forward (z)\n2S velocity dist. (m/s)',
             'FWHM of transverse (x)\n2S velocity dist. (m/s)',
             '2S excitation probability (%)',
             r'Weighted'+' 2S excitation\nprobability (arb. u)',
            ],
            fig_traj.axs,
            [1, 1, 100, 100],
            )):

        if is_fit_result:
            y = dfTrajsAnalysis[mask_unique][param_column + '_Value'].values
            y_sigma = dfTrajsAnalysis[mask_unique][param_column + '_Sigma'].values
        else:
            y = dfTrajsAnalysis[mask_unique][param_column].values * y_scale
            y_sigma = np.ones(len(y))

        # Plot result of trajectory analysis
        ax.set_ylabel(label)
        if is_fit_result:
            h1 = ax.errorbar(
                x, y, y_sigma,
                **plts.get_point_params(**{
                    'linestyle': '-',
                    'markersize': 1,
                    }))
        else:
            h1 = ax.plot(
                x,
                y,
                **plts.get_line_params())
        label = (
            f'Delay {unique_param["DelayID"]}'
#            + pyha_def_latex.defs['PowerOneSTwoS']
            f', {unique_param["LaserPower"]:.1f} W'
            )
        leg_handles[f'P{i_unique_param}A{i_plot}'] = h1[0]
        leg_labels[f'P{i_unique_param}A{i_plot}'] = label

        # Fit Gaussian
        pstart = [
            (x[np.argmin(y)] if peak_invert else x[np.argmax(y)]),
            np.max(y)-np.min(y) * (-1 if peak_invert else 1),
            (np.max(y) if peak_invert else np.min(y)),
            np.sum(x*y)/np.sum(y),
            ]
        fit_func_id = 'Gaussian'
        fit_func = myFit.fit_funcs[fit_func_id]
        fit_gaussian, _ = myFit.do_fit(
            fit_func, x, y, y_sigma,
            warning_as_error=False, pstart=pstart
            )
        popt = fit_gaussian['Popt']
        ax.plot(
            x_fit, fit_func['Func'](x_fit, *popt),
            **plts.get_line_params(**{
                'color': h1[0].get_color(),
                'linestyle': '--',
                }))
        fits_gaussian = {
            **fits_gaussian,
            **myFit.fit_result_to_dict(
                fit_gaussian, fit_func, column_prefix=fit_func_id+'_'+param_name)
            }

        # Fit parabola
#        pstart = [
#            1,
#            (x[np.argmin(y)] if peak_invert else x[np.argmax(y)]),
#            (np.max(y) if peak_invert else np.min(y)),
#            ]
#        fit_func_id = 'Parabola'
#        fit_func = myFit.fit_funcs[fit_func_id]
#        fit_parabola, _ = myFit.do_fit(
#            fit_func, x, y, y_sigma,
#            warning_as_error=False, pstart=pstart
#            )
#        popt = fit_parabola['Popt']
#        print(popt)
#        ax.plot(
#            x_fit, fit_func['Func'](x_fit, *popt),
#            color=h1[0].get_color(), linestyle='--'
#            )
#        fits_gaussian = {
#            **fits_gaussian,
#            **myFit.fit_result_to_dict(fit_gaussian, fit_func, column_prefix=fit_func_id+'_'+param_name)
#            }

    trajs_dict = dict(unique_param[columns])
    trajs_dict = {**trajs_dict, **fits_gaussian}

    df_traj_detunings = df_traj_detunings.append(
        trajs_dict,
        ignore_index=True
        )

#plt.setp(ax1.get_xticklabels(), visible=False)
#plt.setp(ax2.get_xticklabels(), visible=False)

#legend1 = ['%.1f'%(w0List[i]*1e6) for i in range(len(w0List))]
#legend2 = ['Fit' for i in range(len(w0List))]
#legendC = [''] * 2 * len(legend1)
#legendC[::2] = legend1
#legendC[1::2] = legend2
#plt.figure(figzMean.number)
#ax1.legend(legendC, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
#legendParams = {
#    'fontsize': 12,
#    'title': 'Delay',
#    'loc': 2,
#    'bbox_to_anchor': (1.05, 1)
#    }
#ax1.legend(
#    **plts.get_leg_params(**{
#        'ncol': 2,
##        'loc': 2,
##        'bbox_to_anchor': (1.05, 1)
#        }))
if plot_params.get('ShowTitle', False):
    plt.suptitle(
        filename_prefix
    #    + r'$p(v) \propto v^{n}e^{-v^2}$'
        + ', $n$ = ' + ', '.join([str(elem) for elem in dfTrajsAnalysis[mask]['SpeedDistExp'].unique()])
        )

plot_params = {
    **plot_params,
    'LegHandles': leg_handles,
    'LegLabels': leg_labels,
    }

axs_params = fig_traj.set_axs_params()

#plt.tight_layout(pad=2, w_pad=12, h_pad=2)
#ax1.set_xticks(np.linspace(-1000, 2500, 8))
#ax2.legend(legendC, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
#ax2.legend( **legendParams)
#plt.subplot(3, 1, 1)
#ax3.legend(legendC, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
#ax3.legend( **legendParams)
#plt.subplot(3, 1, 2)
#ax4.legend(legendC, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
#ax4.legend( **legendParams)
#filename_out = folder + filename_prefix + \
#    ' exc prob P = {:.2f} W, delay {:d} to {:d}'.format(pList[0], np.min(delayListList), np.max(delayListList))
filename = \
    filename_prefix \
    + ' n=' + ', '.join([str(elem) for elem in dfTrajsAnalysis[mask]['SpeedDistExp'].unique()])
#plt.savefig(input_dir + filename + '.pdf')
#plt.savefig(input_dir + filename + '.png', dpi=300)

#%% Plot results (each at optimal detuning) as a function of laser power, for the delay defined in the section above in delayListList

#figAll, ((axs1, axs2), (axs3, axs4)) = plt.subplots(2, 2, figsize =(16, 12))
#axs1.set_xlabel('Intracavity power per direction (W)')
#axs1.set_ylabel('Mean of longitudinal 2S velocity distribution (m/s)')
#axs1.set_title('Mean of longitudinal 2S velocity distribution (weighted with ' r'1/$v_z$' ')\nat 243 nm laser detuning for minimal value')
#axs3.set_xlabel('Intracavity power per direction (W)')
#axs3.set_ylabel('FWHM of transversal 2S velocity distribution (m/s)')
#axs3.set_title('FWHM of transverse 2S velocity distribution (weighted with ' r'1/$v_z$' ')\nat 243 nm laser detuning for minimal value')
#axs2.set_xlabel('Intracavity power per direction (W)')
#axs2.set_ylabel('2S excitation probability per delay')
#axs2.set_title('2S excitation probability (not weighted)\nat 243 nm laser detuning for maximal value')
#axs4.set_xlabel('Intracavity power per direction (W)')
#axs4.set_ylabel('Weighted 2S excitation probability per delay')
#axs4.set_title('2S excitation probability (weighted with ' r'1/$v_z$' ')\nat 243 nm laser detuning for maximal value')
#
#plt.tight_layout(pad=1, w_pad=2, h_pad=2)
#
#nTraj = np.mean(dfParam.nTraj)
#plotHandles = [''] * len(w0List)
#minvxFWHM = fitresultvxFWHM[:,:,2]+fitresultvxFWHM[:,:,3]
#for w0Index in range(len(w0List)):
#    plotHandles[w0Index], = axs1.plot(pList, fitresultvzMean[w0Index,:,2]+fitresultvzMean[w0Index,:,3])
#    axs3.plot(pList[minvxFWHM[w0Index,:] > 0],
#              minvxFWHM[w0Index,:][minvxFWHM[w0Index,:] > 0])
#    axs2.plot(pList, (fitresultExcProb[w0Index,:,2]+fitresultExcProb[w0Index,:,3])/(nTraj*len(delayList)))
#    axs4.plot(pList, (fitresultExcProbWeighted[w0Index,:,2]+fitresultExcProbWeighted[w0Index,:,3])/(nTraj*len(delayList)))
#axs1.set_xlim([min(pList), max(pList)])
#axs3.set_xlim([min(pList), max(pList)])
#axs2.set_xlim([min(pList), max(pList)])
#axs4.set_xlim([min(pList), max(pList)])
#bscH = 0.85
#bscV = 0.88
#box = axs1.get_position()
##axs1.set_position([box.x0, box.y0 - (1-bscH) * box.height, box.width * bscV, box.height * bscH])
#axs1.set_position([box.x0, box.y0, box.width, box.height * bscV])
#box = axs2.get_position()
##axs2.set_position([box.x0 - (1-bscV) * box.width, box.y0 - (1-bscH) * box.height, box.width * bscV, box.height * bscH])
#axs2.set_position([box.x0, box.y0, box.width, box.height * bscV])
#box = axs3.get_position()
##axs3.set_position([box.x0, box.y0, box.width * bscV, box.height * bscH])
#axs3.set_position([box.x0, box.y0 + (1-bscV) * box.height, box.width, box.height * bscV])
#box = axs4.get_position()
##axs4.set_position([box.x0 - (1-bscV) * box.width, box.y0, box.width * bscV, box.height * bscH])
#axs4.set_position([box.x0, box.y0 + (1-bscV) * box.height, box.width, box.height * bscV])
#legend1 = ['%.1f'%(w0List[i]*1e6) for i in range(len(w0List))]
##axs1.legend(legend1, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
##axs2.legend(legend1, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
##axs3.legend(legend1, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
##axs4.legend(legend1, title=r'Beam waist $w_0$ (µm)', loc=2,  bbox_to_anchor=(1, 1))
#plt.figlegend(plotHandles, legend1, loc = 'lower center', ncol=5, labelspacing=0., title=r'Beam waist $w_0$ (µm)')
#plt.suptitle('2S trajectories for different 243 nm beam waist radii and intracavity power for experimental delay(s) %i to %i (%.0f µs to %.0f µs)'%(np.min(delayList), np.max(delayList), delayStartTime[delayList[0]]*1e6, delayEndTime[delayList[-1]]*1e6),
#             fontsize=16)
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories delay %i to %i.pdf'%(np.min(delayList), np.max(delayList)))
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories delay %i to %i.png'%(np.min(delayList), np.max(delayList)))

#%%

#FigfitresultvzMean = plt.figure()
#FigfitresultvxFWHM = plt.figure()
#FigfitresultExcProb = plt.figure()
#FigfitresultExcProbWeighted = plt.figure()

#%% Plot results for a given laser power (pIndex) as a function of delay (for this all delays have to be analyzed in the fit section above)

#pIndex = 1

#plt.figure(FigfitresultvzMean.number)
#plt.clf()
#axc1 = plt.subplot(111)
#box = axc1.get_position()
#axc1.set_position([box.x0, box.y0, box.width, box.height * 0.93])
#axc1.set_xlabel('Experimental delay')
##axc1.set_xlabel('Experimental delay start time (µs)')
#axc1.set_ylabel('Mean of longitudinal 2S velocity distribution (m/s)')
##axc1.set_title('Mean of longitudinal 2S velocity distribution (weighted with ' r'1/$v_z$' ')\nfor %.1f W intracavity power and detuning for minimal value'%pList[pIndex], y=1.07)
#
#plt.figure(FigfitresultvxFWHM.number)
#plt.clf()
#axc2 = plt.subplot(111)
#box = axc2.get_position()
#axc2.set_position([box.x0, box.y0, box.width, box.height * 0.93])
#axc2.set_xlabel('Experimental delay')
##axc2.set_xlabel('Experimental delay start time (µs)')
#axc2.set_ylabel('FWHM of transversal 2S velocity distribution (m/s)')
##axc2.set_ylabel('FWHM of transversal 2S velocity distribution (MHz)')
##axc2.set_title('FWHM of transverse 2S velocity distribution (weighted with ' r'1/$v_z$' ')\nfor %.1f W intracavity power and detuning for minimal value'%pList[pIndex], y=1.07)
#
#plt.figure(FigfitresultExcProb.number)
#plt.clf()
#axc3 = plt.subplot(111)
#box = axc3.get_position()
#axc3.set_position([box.x0, box.y0, box.width, box.height * 0.93])
#axc3.set_xlabel('Experimental delay')
##axc3.set_xlabel('Experimental delay start time (µs)')
#axc3.set_ylabel('2S excitation probability')
##axc3.set_title('2S excitation probability (not weighted)\nfor %.1f W intracavity power and detuning for maximal value'%pList[pIndex], y=1.07)
#
#plt.figure(FigfitresultExcProbWeighted.number)
#plt.clf()
#axc4 = plt.subplot(111)
#box = axc4.get_position()
#axc4.set_position([box.x0, box.y0, box.width, box.height * 0.93])
#axc4.set_xlabel('Experimental delay')
#axc4.set_ylabel('Weighted 2S excitation probability')
##axc4.set_title('2S excitation probability (weighted with ' r'1/$v_z$' ')\nfor %.1f W intracavity power and detuning for maximal value'%pList[pIndex], y=1.07)
#
#
#xDelay = np.ravel(delayListList)
#nTraj = np.mean(dfParam.nTraj)
##xDelay = np.mean([delayStartTime], 0)[xDelay]*1e6
#legendHandles = []
#legendLabels = []
#for w0Index, w0Item in enumerate(w0List):
#    for pIndex, pItem in  enumerate(pList):
#        axc1.plot(xDelay, fitresultvzMeanDelays[:,w0Index,pIndex,2]+fitresultvzMeanDelays[:,w0Index,pIndex,3])
#        minvxFWHM = fitresultvxFWHMDelays[:,:,pIndex,2]+fitresultvxFWHMDelays[:,:,pIndex,3]
##        minvxFWHM = minvxFWHM/410e-3
#        h, = axc2.plot(xDelay, minvxFWHM[:,w0Index])
#        legendHandles.append(h)
#        axc3.plot(xDelay, (fitresultExcProbDelays[:,w0Index,pIndex,2]+fitresultExcProbDelays[:,w0Index,pIndex,3])/nTraj)
#        axc4.plot(xDelay, (fitresultExcProbWeightedDelays[:,w0Index,pIndex,2]+fitresultExcProbWeightedDelays[:,w0Index,pIndex,3])/nTraj)
#        legendLabels.append('$w_0$ = %.1f, $r_N$ = %.2f mm'%(w0Item*1e6,pItem*1e3))
##legend1 = ['%.1f'%(w0List[i]*1e6) for i in range(len(w0List))]
#legend1 = ['$w_0$ = %.1f, $r_N$ = %.2f mm'%(w0List[i]*1e6,pList[pIndex]*1e3) for i in range(len(w0List))]
#plt.figure(FigfitresultvzMean.number)
#axc1.legend(legendLabels, loc = 'upper right')
#plt.tight_layout()
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories vz mean vs delay P = %.2f W.pdf'%pList[pIndex])
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories vz mean vs delay P = %.2f W.png'%pList[pIndex])
#plt.figure(FigfitresultvxFWHM.number)
#axc2.legend(legendLabels, loc = 'lower left')
#plt.tight_layout()
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories vx FWHM vs delay P = %.2f W.pdf'%pList[pIndex])
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories vx FWHM vs delay P = %.2f W.png'%pList[pIndex])
#plt.figure(FigfitresultExcProb.number)
#axc3.legend(legendLabels, loc = 'lower left')
#plt.tight_layout()
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories exc prob vs delay P = %.2f W.pdf'%pList[pIndex])
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories ex prob vs delay P = %.2f W.png'%pList[pIndex])
#plt.figure(FigfitresultExcProbWeighted.number)
#axc4.legend(legendLabels, loc = 'lower left')
#plt.tight_layout()
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories weighted exc prob vs delay P = %.2f W.pdf'%pList[pIndex])
##plt.savefig('Plots\\' + outFilenamePrefix + '2S trajectories weighted exc prob vs delay P = %.2f W.png'%pList[pIndex])
