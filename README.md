# MCTrajectories

Monte Carlo simulation of atomic trajectories and laser excitation, used to simulate the 1S-2S excitation of an atomic beam of hydrogen or deuterium. The 1S-2S excitation is taken into account by numerical integration of the corresponding optical Bloch equations (OBEs).

Written by Lothar Maisenbacher for the 2S-nP hydrogen spectroscopy project at MPQ.

The code has been tested with Python 3.10.9 and 3.11.5.

## Preparing environment

The custom packages [`pyhs`](https://gitlab.mpcdf.mpg.de/lmaisen/pyhs) and [`pyha`](https://gitlab.mpcdf.mpg.de/lmaisen/pyha) are required. To install these and the remaining dependencies, clone this repository, and run (preferentially in a virtual environment)
```
pip install -r requirements.txt
```

## Preparing compiled version of OBEs

[Cython](https://cython.org/) is used to compile the OBEs to speed up the numerical integration. The OBEs are defined in Python in `cython_obe/cOBE_1S2S.pyx` and are compiled to files `cython_obe/cOBE_1S2S.cy*`, which are different for each major version of Python and operating system. Thus, depending on your Python version and operating system, a compiled version of `cOBE_1S2S.pyx` needs to be generated first. If the corresponding file is not present, this will lead to an error such as

```
ImportError: cannot import name 'cOBE_1S2S' from 'MCTrajectories.cython_obe'
```

To compile, navigate to the `cython_obe` subdirectory in the terminal and run the compilation with

```
cd <path_to_this_repository>/cython_obe
python setup.py build_ext --inplace
```

where the directory of this repository is `<path_to_this_repository>`.
This requires a C/C++ compiler to be present. On Windows, Visual Studio Community Edition can be used. If the compiler is not found in the terminal, one can try using the command prompt available from start menu, e.g., 'x64 Native Tools Command Prompt for VS 2022'.

## Configuration file

Most scripts require a configuration (config) file in TOML format. An example config file is included as `example_config.toml`. The scripts will first search for the file `config.toml` in the directory of the repository (and thus, to get started, it is sufficient to make a copy of `example_config.toml` and rename it to `config.toml`). If `config.toml` is not found, the scripts will next search for `pyh.toml` in the user home directory.