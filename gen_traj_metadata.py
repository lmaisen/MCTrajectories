# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 17:42:37 2020

@author: Lothar Maisenbacher/MPQ

Generate metadata for generation of multiple 2S trajectory sets, scanning various parameters.
"""

import numpy as np
import time
import datetime
import pandas as pd
import itertools
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.trajs

# Load OBEs
import odeint_1s2s

import traj_settings
config = traj_settings.load_config_file()
dir_metadata_input = config['MonteCarloTrajectories']['DirMetadataInput']

Trajs = pyha.trajs.Trajs()

legacy = False

#%% Load common trajectory set parameters (metadata) from JSON file

common_json_file = 'H2S6P2019_default.json'
filepath = Path(dir_metadata_input, common_json_file)
sr_param_common = Trajs.read_series_from_json(filepath, 'dfTrajParams')

#%% Generate metadata for multiple trajectory sets, scanning various parameters

aperture_widths = np.array([1.2e-3])
powers_laser = np.array([1.1])
atomic_detunings = np.array([970])

w0s = np.array([sr_param_common['LaserWaistRadius']])
speed_dist_exps = np.array([3])
nozzle_temps = np.array([4.8])
num_reps = 1

set_list = list(itertools.product(
    aperture_widths,
    powers_laser,
    atomic_detunings,
    w0s,
    speed_dist_exps,
    nozzle_temps,
    range(num_reps),
    ))

# Add to DataFrame
dfTrajParams = Trajs.df_templates['dfTrajParams']

subdir = ''
filename_prefix = (
    '{} {}'.format(time.strftime("%Y-%m-%d %H-%M-%S"), sr_param_common['Isotope'])
    + f' {powers_laser[0]:.1f}W'
    )

print(f'Generating metadata for {len(set_list)} trajectory set(s)')

for i_set, (aperture_width, power_laser, atomic_detuning, w0, speed_dist_exp, nozzle_temp, num_rep) in enumerate(set_list):

    sr_param = sr_param_common.copy()

    sr_param['Aperture2Width'] = aperture_width
    sr_param['LaserPower'] = power_laser
    sr_param['AtomicDetuning'] = atomic_detuning
    sr_param['LaserWaistRadius'] = w0
    sr_param['SpeedDistExp'] = speed_dist_exp
    sr_param['NozzleTemp'] = nozzle_temp
    sr_param['Subdir'] = subdir
    sr_param['Timestamp'] = pd.Timestamp(datetime.datetime.utcnow())
    traj_uid = pyhs.gen.get_uid()
    sr_param.name = traj_uid

    # Add to DataFrame
    dfTrajParams = dfTrajParams.append(sr_param)

dfTrajParams = Trajs.cast_containers_to_data_format(dfTrajParams, 'dfTrajParams')

# Set output dir and filename
filename, filebase, ext = odeint_1s2s.get_traj_filename(
    dfTrajParams.loc[traj_uid], legacy=legacy)
dfTrajParams.loc[traj_uid, 'Filename'] = filename

# Write trajectory set parameters to file
filepath = Path(
    dir_metadata_input, filename_prefix + ' Params.dat')
Trajs.write_df_to_csv(dfTrajParams, 'dfTrajParams', filepath)

print(f'Saved generated metadata to file \'{filepath}\'')
