# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 17:44:39 2015

@author: Lothar Maisenbacher/MPQ

Analyzes (e.g. mean velocities, velocity distribution width, ...)
2S trajectory sets created with `gen_traj.py`,
writing results to DataFrame/file and generating plots.
"""

import numpy as np
import pandas as pd
import copy
import argparse
import glob
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()
import pyhs.fit

# pyha
import pyha.plot
import pyha.def_latex
import pyha.trajs
import pyha.util.data

# Load OBEs
# Using cythonized 1S-2S OBEs
import cython_obe.cOBE_1S2S as OBE_1S2S
import traj_settings

import pyha.plts as plts
plts.set_params()

myFit = pyhs.fit.Fit(out_func=logger.info)
myFit.fit_opt['MaxFEv'] = 10000

myOBE1s2s = OBE_1S2S.OBE_1S2S(np.nan)

Trajs = pyha.trajs.Trajs()

## Shorthands
# Latex
ltx = pyha.def_latex.defs

#%% (Default) settings and parameters

default_metadata_file = None
# default_metadata_file = 'test3 Params.data'

#%% Get input parameters

parser = argparse.ArgumentParser(
    description='Analysis of 2S trajectory set(s).')
parser.add_argument(
    '-p', '--MetadataFile', help='Filename of trajectory set metadata file to process',
    default=default_metadata_file, type=str)
config = traj_settings.load_config_file(parser=parser)
command_line_args = vars(parser.parse_args())

dir_metadata = config['MonteCarloTrajectories']['DirMetadata']
dir_sets = config['MonteCarloTrajectories']['DirSets']
dir_plots = config['MonteCarloTrajectories']['DirPlots']
metadata_file = (
    Path(command_line_args['MetadataFile'])
    if command_line_args['MetadataFile'] is not None else None)

# Legacy
#dir_trajs = '../Archive/results/2020-06 V9-3.2 2S6P Thesis'
#dir_metadata = Path(dir_trajs, '')
#dir_sets = Path(dir_trajs, '')
#dir_plots = Path(dir_trajs, '')

#%% Load trajectory set metadata

if metadata_file is not None:
    if not metadata_file.is_absolute():
        metadata_filepath = Path(dir_metadata, metadata_file)
    metadata_filepaths = [metadata_filepath]
else:
    metadata_filepaths = None

# Explicitly give list of metadata files located in metadata directory
# metadata_filenames = [
#     'test_req.xlsx Params.dat',
#     ]
# metadata_filepaths = [
#     Path(dir_metadata, elem) for elem in metadata_filenames]

if metadata_filepaths is None:

    # Search for metadata files in given metadata directory
    metadata_filepaths = glob.glob(str(Path(dir_metadata, '* Params.dat')))
    # Sort files by last modified time
    metadata_mtimes = [Path(path).stat().st_mtime for path in metadata_filepaths]
    metadata_filepaths = np.array(metadata_filepaths)[np.argsort(metadata_mtimes, )]
    metadata_filepaths = metadata_filepaths[-1:]

metadata_filepaths = [
    Path(filepath) for filepath in metadata_filepaths]
metadata_filenames = [
    filepath.name for filepath in metadata_filepaths]

logger.info(
    'Loading %d metadata file(s): %s',
    len(metadata_filepaths), ', '.join([f'\'{s}\'' for s in metadata_filepaths]))

Trajs.init_data_containers()
metadata_filepaths = [filepath for filepath in metadata_filepaths if filepath.exists()]
Trajs.load_trajs_metadata(metadata_filepaths)

dfTrajParams = Trajs.dfs['dfTrajParams']

mask_params = (
    pd.Series(True, index=dfTrajParams.index)
#    & (dfTrajParams.index.isin(['iBchOE0c']))
    )

logger.info('Analyzing %d trajectory set(s)', np.sum(mask_params))

#%% Analyze trajectory sets and plot results

# Init empty DataFrame for analysis results
dfTrajAnalysis = Trajs.df_templates['dfTrajAnalysis']
# Init empty dict for analysis results
dfTrajAnalysis_dict = {}

delay_colors = plts.get_delay_colors()

delay_ids_to_show = [
    '2',
    '6',
    '10',
    '13',
    '14',
    '16',
    ]

plot_params_common = {
    'FigName': '1S-2S trajectory analysis: Number of 2S atoms',
    'FigSize': [plts.pagewidth*0.8, 5],
    'Save': True,
    'ShareX': False,
    'XLabelType': 'Dataset',
    'SaveDir': dir_plots,
    'CreateDir': True,
    'SavePrefixTime': False,
    'SaveOverwrite': True,
    'SubplotLabelX': 0.01,
    'SubplotLabelY': 0.03,
    }

plot_params_common['LegParams'] = {
    'ncol': 1,
    'handlelength': 0.5,
    'columnspacing': 0.5,
#        'fontsize': plts.params['FontsizeMed'],
    }
plot_params_common['AxParams0'] = {
    'ShowLegend': True,
#        'CustomYLabel': '2S amplitude (arb. u.)',
    'CustomYLabel': 'Number of simulated\n2S atoms '+ltx['NTwoSSim'],
    'SubplotLabel': 'A',
    'LegParams': {
        'title': (
            r'Delay: '
            + f'{ltx["DeltavxFWHM"]}' + r'$(\mathrm{2S})$'
            + f' ({ltx["DeltavxFWHM"]}' + r'$(\mathrm{1S})$)'
            ),
        },
    'XLim': [-5, 5],
    }
plot_params_common['AxParams1'] = {
    'ShowLegend': True,
#        'CustomYLabel': '2S amplitude (arb. u.)',
    'CustomYLabel': 'Number of simulated\n2S atoms '+ltx['NTwoSSim'],
    'SubplotLabel': 'B',
    'LegParams': {
#            'title': (
#                r'Delay: '
#                + f'{ltx["vmeanz"]}' + r'$(\mathrm{2S})$'
#                + f' ({ltx.["vmeanz"]}' + r'$(\mathrm{1S})$)'
#                )
        'title': (
            r'Delay: '
            + f'{ltx["vmean"]}' + r'$(\mathrm{2S})$'
            + f' ({ltx["vmean"]}' + r'$(\mathrm{1S})$)'
            ),
        },
    'XLim': [0, 800],
    }
plot_params_common['AxParams2'] = {
    'CustomYLabel': 'Number of simulated\n2S atoms '+ltx['NTwoSSim'],
    'SubplotLabel': 'C',
    'LegParams': {
        'title': (
            r'Delay: '
            + f'{ltx["DeltaalphaFWHM"]}' + r'$(\mathrm{2S})$'
            + f' ({ltx["DeltaalphaFWHM"]}' + r'$(\mathrm{1S})$)'
            ),
        },
    'YLim': [np.nan, 550],
    'XLim': [-15, 15],
    }
plot_params_common['AxParams3'] = {
    'CustomYLabel': 'Number of simulated\n2S atoms '+ltx['NTwoSSim'],
    'SubplotLabel': 'D',
    }

plot_params_2_common = {
    **plot_params_common.copy(),
    'FigSize': [plts.pagewidth*0.8, 3.2],
    }
leg_params_2 = {'title': 'Delay', 'ncol': 2,}

plot_params_2s_exc_prob_common = {
    **plot_params_2_common,
    'FigName': '1S-2S trajectory analysis: 2S excitation probability',
    }
plot_params_2s_exc_prob_common['AxParams0'] = {
    **plot_params_2s_exc_prob_common['AxParams0'],
    'LegParams': leg_params_2,
    'CustomYLabel': '2S excitation\nprobability '+ltx['ProbTwoS']+' (%)',
    }
plot_params_2s_exc_prob_common['AxParams1'] = {
    **plot_params_2s_exc_prob_common['AxParams1'],
    'LegParams': leg_params_2,
    'CustomYLabel': '2S excitation\nprobability '+ltx['ProbTwoS']+' (%)',
    'XLim': [0, 400],
    }

plot_params_ioni_common = {
    **plot_params_2_common,
    'FigName': '1S-2S trajectory analysis: Ionization probability',
    }
plot_params_ioni_common['AxParams0'] = {
    **plot_params_ioni_common['AxParams0'],
    'LegParams': leg_params_2,
    'CustomYLabel': 'Ionization\nprobability '+ltx['ProbIoni']+' (%)',
    }
plot_params_ioni_common['AxParams1'] = {
    **plot_params_ioni_common['AxParams1'],
    'LegParams': leg_params_2,
    'CustomYLabel': 'Ionization\nprobability '+ltx['ProbIoni']+' (%)',
    'XLim': [0, 400],
    }

for i_traj, (traj_uid, sr_param) in enumerate(dfTrajParams[mask_params].iterrows()):

    # Set plot parameters for this trajectory set
    plot_params = copy.deepcopy(plot_params_common)
    plot_params['SaveSuffix'] = Path(sr_param['Filename']).stem
    plot_params_2s_exc_prob = copy.deepcopy(plot_params_2s_exc_prob_common)
    plot_params_2s_exc_prob['SaveSuffix'] = plot_params['SaveSuffix']+' 2S exc prob'
    plot_params_ioni = copy.deepcopy(plot_params_ioni_common)
    plot_params_ioni['SaveSuffix'] = plot_params['SaveSuffix']+' 2S ioni prob'

    # Load trajectory set from file
    try:
        traj_set = Trajs.load_traj_set(traj_uid, dir_sets)

        positions = traj_set['Positions']
        vels = traj_set['Vels']
        # Sum of 2S excitation probabilities over the integration delay
        # contained in each experimental delay
        exc_probs_2s_int_delay_sum = traj_set['2SExcProbs_IntDelaySum']
        # Sum of ionization probabilities over the integration delay
        # contained in each experimental delay
        ioni_probs_int_delay_sum = traj_set['IoniProbs_IntDelaySum']
        seed = traj_set['Seed'].item()
    finally:
        # Close trajectory set file
        Trajs.close_traj_set(traj_uid)

    # Load delays
    delays = traj_settings.delay_sets[sr_param['DelaySetID']]
    # Use only analysis delays
    analysis_delays = {
        delay_id: delay for delay_id, delay in delays.items()
        if delay['Analysis']}
    analysis_delays = pyha.util.data.addIIDToDictofDicts(analysis_delays)
    # Select delays
    delay_ids = analysis_delays.keys()
#    delay_ids = [
#        '16',
#        ]
    analysis_delays = {
        delay_id: delay for delay_id, delay in analysis_delays.items()
        if delay_id in delay_ids}
    nums_int_delays = {
        delay_id: int(np.round(
            (delay['EndTime']-delay['StartTime'])/sr_param['IntDelayWidth']))
        for delay_id, delay in analysis_delays.items()}
    min_num_int_delays = sr_param['NMinIntDelays']

    data_sets = {}
    data_sets_2s_exc_prob = {}
    data_sets_ioni = {}
    hist_maxs = {}
    colors = plts.get_line_colors(len(analysis_delays), cmap='tab10')
    for i_delay, (delay_id, delay) in enumerate(analysis_delays.items()):

        # logger.info(f'Analyzing delay {delay_id}')

        analysis_delay_iid = delay['IID']
        total_exc_probs_2s = np.sum(
            exc_probs_2s_int_delay_sum[:, analysis_delay_iid], 0)
        total_ioni_probs = np.sum(
            ioni_probs_int_delay_sum[:, analysis_delay_iid], 0)
        # Number of trajectories that have interacted with laser
        mask_zero = exc_probs_2s_int_delay_sum[:, analysis_delay_iid] != 0.
        num_trajs_inter = np.sum(mask_zero)

        avg_amplitudes_delay = (
            exc_probs_2s_int_delay_sum[:, analysis_delay_iid]
            / min_num_int_delays
            )
        # Weighted with excitation probabilities (gives 2S trajectories)
        hist_weights = avg_amplitudes_delay

        hist_weights_2s_exc_prob = (
            exc_probs_2s_int_delay_sum[:, analysis_delay_iid]
            / nums_int_delays[delay_id]
            )

        hist_weights_ioni = (
            ioni_probs_int_delay_sum[:, analysis_delay_iid]
            / nums_int_delays[delay_id]
            )

        # Mean time spent in laser
        ts_in_laser = (
            sr_param['DetectionDist']/vels[:, 2]
            -(delay['StartTime']+delay['EndTime'])/2)
        ts_in_laser[ts_in_laser > 1/sr_param['ChopperFreq']/2] = (
            1/sr_param['ChopperFreq']/2)
        ts_in_laser[ts_in_laser < 0] = 0.
        t_in_laser = np.mean(ts_in_laser)
        t_in_laser_inter_traj = np.mean(ts_in_laser[ts_in_laser > 0])

        directions = ['Vel_x', 'Vel_y', 'Vel_z', 'Speed', 'Div_x']
        v_means_full = np.zeros(len(directions))
        v_rmss_full = np.zeros(len(directions))
        v_fwhms_full = np.zeros(len(directions))
        v_means_unweighted = np.zeros(len(directions))
        v_rmss_unweighted = np.zeros(len(directions))
        v_fwhms_unweighted = np.zeros(len(directions))
        v_means = np.zeros(len(directions))
        v_rmss = np.zeros(len(directions))
        v_fwhms = np.zeros(len(directions))
        decay_prob_det_avg = np.nan
        fits_gaussian = {}
        fits_maxwell = {}
        for i_dir, (analysis_id, hist_limits, plot_limits, hist_data, direction,
                    label, i_ax, i_ax_2s_exc_prob, i_ax_ioni, fit_maxwell,
                    show_unweighted_hist, hist_num_bins) \
                    in enumerate(zip(
                ['VelX', 'VelY', 'VelZ', 'Speed', 'DivX'],
                [[-5, 5], [-6, 6], [0, 1000], [0, 1000], [-15, 15]],
                [[-5, 5], [-6, 6], [0, 800], [0, 800], [-15, 15]],
                [
                 vels[:, 0],
                 vels[:, 1],
                 vels[:, 2],
                 np.sqrt(np.sum(vels**2, 1)),
                 1e3*np.arctan(vels[:, 0]/vels[:, 2]),
                ],
                directions,
                [
                 r'Transverse ($x$) velocity '+ltx['vx']+' (m/s)',
                 r'Vertical ($y$) velocity '+ltx['vy']+' (m/s)',
                 r'Longitudinal ($z$) velocity '+ltx['vz']+' (m/s)',
                 r'Speed $v$ (m/s)',
                 r'Transverse ($x/z$) angle '+ltx['dalpha']+' (mrad)',
                ],
                [0, -1, -1, 1, 2],
                [0, -1, -1, 1, -1],
                [0, -1, -1, 1, -1],
                [False, False, True, True, False],
                [True, True, True, True, True],
                [200, 200, 400, 400, 100],
                )):

            show_plots = delay_id in delay_ids_to_show

            if show_plots:
                plot_params[f'AxParams{i_ax:d}'] = {
                    **plot_params.get(f'AxParams{i_ax:d}', {}),
                    'CustomXLabel': label,
                    }
                plot_params_2s_exc_prob[f'AxParams{i_ax_2s_exc_prob:d}'] = {
                    **plot_params_2s_exc_prob.get(f'AxParams{i_ax_2s_exc_prob:d}', {}),
                    'CustomXLabel': label,
                    }
                plot_params_ioni[f'AxParams{i_ax_ioni:d}'] = {
                    **plot_params_ioni.get(f'AxParams{i_ax_ioni:d}', {}),
                    'CustomXLabel': label,
                    }

            # Unweighted histogram of underlying 1S trajectory distribution,
            # taking into account all trajectories independent on whether
            # they have interacted with the 1S-2S preparation laser
            full_hist, _ = np.histogram(
                hist_data,
                bins=hist_num_bins,
                range=hist_limits,
                )

            # Unweighted histogram of underlying 1S trajectory distribution,
            # only taking into account trajectories that have interacted with the
            # 1S-2S preparation laser,
            # i.e. have an excitation probability > 0.
            # unweighted_hist is then the number of trajectories that have
            # interacted with the laser in each bin.
            mask_unweighted = hist_weights != 0
            unweighted_hist, _ = np.histogram(
                hist_data[mask_unweighted],
                bins=hist_num_bins,
                range=hist_limits,
                )
            mask_unweighted_hist_nonzero = unweighted_hist != 0

            # Histogram of number of 2S atoms
            weighted_hist, bin_edges = np.histogram(
                hist_data,
                bins=hist_num_bins,
                range=hist_limits,
                weights=hist_weights
                )

            # Histogram of 2S excitation probability
            # Get sum of 2S excitation probability in each bin
            weighted_hist_2s_exc_prob_sum, _ = np.histogram(
                hist_data[mask_unweighted],
                bins=hist_num_bins,
                range=hist_limits,
                weights=hist_weights_2s_exc_prob[mask_unweighted]
                )
            # Get 2S excitation probability per bin,
            # which is the sum of the 2S excitation probability of the trajectories
            # in each bin, divided by the number of trajectories in each bin
            weighted_hist_2s_exc_prob = np.zeros(len(weighted_hist_2s_exc_prob_sum))
            weighted_hist_2s_exc_prob[mask_unweighted_hist_nonzero] = (
                weighted_hist_2s_exc_prob_sum[mask_unweighted_hist_nonzero]
                /unweighted_hist[mask_unweighted_hist_nonzero])

            # Histogram of ionization probability
            # Get sum of ionization probability in each bin
            weighted_hist_ioni_prob_sum, _ = np.histogram(
                hist_data[mask_unweighted],
                bins=hist_num_bins,
                range=hist_limits,
                weights=hist_weights_ioni[mask_unweighted]
                )
            # Get ionization probability per bin,
            # which is the sum of the ionization probability of the trajectories
            # in each bin, divided by the number of trajectories in each bin
            weighted_hist_ioni_prob = np.zeros(len(weighted_hist_ioni_prob_sum))
            weighted_hist_ioni_prob[mask_unweighted_hist_nonzero] = (
                weighted_hist_ioni_prob_sum[mask_unweighted_hist_nonzero]
                /unweighted_hist[mask_unweighted_hist_nonzero])

            x_bins = bin_edges[:-1] + np.diff(bin_edges)/2

            hists = {
                'Full': full_hist,
                'Unweighted': unweighted_hist,
                'Weighted': weighted_hist,
                '2SExcProb': weighted_hist_2s_exc_prob,
                'IoniProb': weighted_hist_ioni_prob,
                }

            # Find probability of 2S decay within detector
            if analysis_id == 'VelZ':
                # Time spent in detector
                time_in_det = sr_param['DetectorLength']/x_bins
                # 2S decay probability during time in detector
                decay_prob_det = 1-np.exp(-time_in_det*myOBE1s2s.gamma_s)
                # Average 2S decay probability
                decay_prob_det_avg = np.sum(
                    weighted_hist*decay_prob_det)/np.sum(weighted_hist)

            # Fit Gaussian
            fit_func_id = 'Gaussian'
            fit_func = myFit.fit_funcs[fit_func_id]
            fit_results_gaussian = {}
            for hist_id, hist in hists.items():

                pstart = [
                    np.mean(hist*x_bins)/np.mean(hist),
                    np.max(hist),
                    0,
                    np.std(hist*x_bins)/(2*np.mean(hist)),
                    ]
                fit_result_gaussian, _ = myFit.do_fit(
                    fit_func, x_bins, hist, np.ones(len(x_bins)),
                    warning_as_error=False, pstart=pstart
                    )
                fit_results_gaussian[hist_id] = fit_result_gaussian

            fits_gaussian = {
                **fits_gaussian,
                **myFit.fit_result_to_dict(
                    fit_results_gaussian['Full'], fit_func,
                    column_prefix=direction+'_Full_'+fit_func_id),
                **myFit.fit_result_to_dict(
                    fit_results_gaussian['Unweighted'], fit_func,
                    column_prefix=direction+'_1S_'+fit_func_id),
                **myFit.fit_result_to_dict(
                    fit_results_gaussian['Weighted'], fit_func,
                    column_prefix=direction+'_2S_'+fit_func_id),
                **myFit.fit_result_to_dict(
                    fit_results_gaussian['2SExcProb'], fit_func,
                    column_prefix=direction+'_2SExcProb_'+fit_func_id),
                **myFit.fit_result_to_dict(
                    fit_results_gaussian['IoniProb'], fit_func,
                    column_prefix=direction+'_2SIoniProb_'+fit_func_id),
                }

            v_means_full[i_dir] = (np.mean(full_hist*x_bins)/np.mean(full_hist))
            v_rmss_full[i_dir] = np.sqrt(np.mean(full_hist*x_bins**2)/np.mean(full_hist))
            v_fwhms_full[i_dir] = fit_results_gaussian['Full']['Popt'][3]

            v_means_unweighted[i_dir] = (
                np.mean(unweighted_hist*x_bins)/np.mean(unweighted_hist))
            v_rmss_unweighted[i_dir] = np.sqrt(
                np.mean(unweighted_hist*x_bins**2)/np.mean(unweighted_hist))
            v_fwhms_unweighted[i_dir] = fit_results_gaussian['Unweighted']['Popt'][3]

            v_means[i_dir] = (np.mean(weighted_hist*x_bins)/np.mean(weighted_hist))
            v_rmss[i_dir] = np.sqrt(np.mean(weighted_hist*x_bins**2)/np.mean(weighted_hist))
            v_fwhms[i_dir] = fit_results_gaussian['Weighted']['Popt'][3]

            # Plot histograms
            if show_plots:
                label = f'{delay_id}'
                if analysis_id in ['VelX', 'VelY']:
                    label += f': {v_fwhms[i_dir]:.1f} m/s ({v_fwhms_unweighted[i_dir]:.1f} m/s)'
                if analysis_id in ['VelZ']:
                    label += f': {v_means[i_dir]:.0f} m/s ({v_means_unweighted[i_dir]:.0f} m/s)'
                if analysis_id in ['Speed']:
                    label += f': {v_means[i_dir]:.0f} m/s ({v_means_unweighted[i_dir]:.0f} m/s)'
                if analysis_id in ['DivX']:
                    label += f': {v_fwhms[i_dir]:.1f} mrad ({v_fwhms_unweighted[i_dir]:.1f} mrad)'
                unweighted_hist_norms = {}
                for hist_id, hist in hists.items():
                    mask_zero = (hist != 0.)
                    if not fit_results_gaussian[hist_id]['FitError']:
                        mask_zero &= (
                            (np.abs(x_bins) < fit_results_gaussian[hist_id]['Popt'][3]))
                    hist_maxs[hist_id] = hist_maxs.get(hist_id, {})
                    hist_maxs[hist_id][analysis_id] = np.nanmax(
                        [hist_maxs[hist_id].get(analysis_id, np.nan),
                         np.nanmax(hist)])
                    if analysis_id in ['VelX', 'VelY', 'DivX']:
                        if np.sum(mask_zero) == 0:
                            unweighted_hist_norms[hist_id] = unweighted_hist
                        else:
                            unweighted_hist_norms[hist_id] = (
                                unweighted_hist
                                / np.min(unweighted_hist[mask_zero]/hist[mask_zero])
                                )
                    else:
                        unweighted_hist_norms[hist_id] = (
                            unweighted_hist/np.max(unweighted_hist)
                            * np.max(hist)
                            )
                if show_unweighted_hist:
                    key = f'D{delay_id}UnwHist{i_dir}'
                    data_set = {
                        'PlotType': 'XY',
                        'BasisParams': 'Line',
                        'XData': x_bins,
                        'XYParams': {
                            'color': delay_colors.get(delay_id),
                            'linestyle': '--',
                            'alpha': 0.5,
                            },
                        }
                    data_sets[key] = {
                        **data_set,
                        'Axes': i_ax,
                        'YData': unweighted_hist_norms['Weighted'],
                        }
                    data_sets_2s_exc_prob[key] = {
                        **data_set,
                        'Axes': i_ax_2s_exc_prob,
                        'YData': 100*unweighted_hist_norms['2SExcProb'],
                        }
                    data_sets_ioni[key] = {
                        **data_set,
                        'Axes': i_ax_ioni,
                        'YData': 100*unweighted_hist_norms['IoniProb'],
                        }

                key = f'D{delay_id}Hist{i_dir}'
                data_set = {
                    'PlotType': 'XY',
                    'BasisParams': 'Line',
                    'Axes': i_ax,
                    'XData': x_bins,
                    'XYParams': {
                        'color': delay_colors.get(delay_id),
                        },
#                    'XLim': plot_limits,
                    'YLim': [np.nan, hist_maxs['Weighted'][analysis_id]*1.1],
                    }
                data_sets[key] = {
                    **data_set,
                    'Axes': i_ax,
                    'Label': label,
                    'YData': weighted_hist,
                    'YLim': [np.nan, hist_maxs['Weighted'][analysis_id]*1.1],
                    }
                data_sets_2s_exc_prob[key] = {
                    **data_sets[key],
                    'Axes': i_ax_2s_exc_prob,
                    'Label': f'{delay_id}',
                    'YData': 100*weighted_hist_2s_exc_prob,
                    'YLim': [
                        -0.05*100*hist_maxs['2SExcProb'][analysis_id],
                        100*hist_maxs['2SExcProb'][analysis_id]*1.1],
                    }
                data_sets_ioni[key] = {
                    **data_sets[key],
                    'Axes': i_ax_ioni,
                    'Label': f'{delay_id}',
                    'YData': 100*weighted_hist_ioni_prob,
                    'YLim': [
                        -0.05*100*hist_maxs['IoniProb'][analysis_id],
                        100*hist_maxs['IoniProb'][analysis_id]*1.1],
                    }
#                data_sets[f'D{delay_id}Gaussian{i_dir}'] = {
#                    'Fig': fig,
#                    'PlotType': 'XY',
#                    'BasisParams': 'Line',
#                    'Axes': i_ax,
#                    'XData': x_bins,
#                    'YData': fit_func['Func'](x_bins, *popt),
#                    'XYParams': {
#                        'color': delay_colors.get(delay_id),
#                        'linestyle': '--',
#                        }
#                    }

            # Fit Maxwellian distribution
            if fit_maxwell:
                fit_func_id = 'GenMaxwellBoltzmann'
                fit_func = myFit.fit_funcs[fit_func_id]
                fit_results_maxwell = {}
                for hist_id, hist in hists.items():

                    pstart = [
                        3,
                        np.max(hist),
                        x_bins[np.argmax(hist)]/np.sqrt(4/2)
                        ]
                    fit_result_maxwell, _ = myFit.do_fit(
                        fit_func, x_bins, hist, np.ones(len(x_bins)),
                        warning_as_error=False, pstart=pstart
                        )
                    fit_results_maxwell[hist_id] = fit_result_maxwell

                if plot_params.get('ShowMaxwellFit', False):
                    data_sets[f'D{delay_id}MBFit{i_dir}'] = {
                        'PlotType': 'XY',
                        'BasisParams': 'Line',
                        'Axes': i_ax,
                        'XData': x_bins,
                        'YData': fit_func['Func'](x_bins, *fit_results_maxwell['Weighted']['Popt']),
                        'XYParams': {
                            'color': delay_colors.get(delay_id),
                            'linestyle': ':',
                            },
                        }

                fits_maxwell = {
                    **fits_maxwell,
                    **myFit.fit_result_to_dict(
                        fit_results_maxwell['Full'], fit_func,
                        column_prefix=direction+'_Full_'+fit_func_id),
                    **myFit.fit_result_to_dict(
                        fit_results_maxwell['Unweighted'], fit_func,
                        column_prefix=direction+'_1S_'+fit_func_id),
                    **myFit.fit_result_to_dict(
                        fit_results_maxwell['Weighted'], fit_func,
                        column_prefix=direction+'_2S_'+fit_func_id),
                    }

        # Connection to the notation used in my thesis:
        #
        # ExcProbPerTraj: \tilde P^{}_{\text{2S,}i}
        # ExcProbPerInterTraj: P^{}_{\text{2S,}i}
        # N2SAtoms: N^{}_{\text{2S,sim,i}}
        # DecayProbDet: P^{}_{\text{2S,dcy}}
        # FractionInterTraj: \eta^{}_{\text{2S,}i}
        # LaserInterTimeInterTraj: \bar{T}^{}_{\text{1S-2S},i}
        trajs_dict = {
            'TrajUID': sr_param.name,
            'DelayID': delay_id,
            'ExcProbPerTraj': (
                total_exc_probs_2s/nums_int_delays[delay_id]/sr_param['NTrajs']),
            'ExcProbPerInterTraj': (
                total_exc_probs_2s/nums_int_delays[delay_id]/num_trajs_inter),
            'IoniProbPerTraj': (
                total_ioni_probs/nums_int_delays[delay_id]/sr_param['NTrajs']),
            'IoniProbPerInterTraj': (
                total_ioni_probs/nums_int_delays[delay_id]/num_trajs_inter),
            'NIntDelays': nums_int_delays[delay_id],
            'NTrajsInter': num_trajs_inter,
            'N2SAtoms': total_exc_probs_2s/min_num_int_delays,
            'DecayProbDet': decay_prob_det_avg,
            'FractionInterTraj': num_trajs_inter/sr_param['NTrajs'],
            'LaserInterTime': t_in_laser,
            'LaserInterTimeInterTraj': t_in_laser_inter_traj,
            }
        mean_dict = {
            **{f'{direction}_Full_Mean': v for direction, v in zip(directions, v_means_full)},
            **{f'{direction}_1S_Mean': v for direction, v in zip(directions, v_means_unweighted)},
            **{f'{direction}_2S_Mean': v for direction, v in zip(directions, v_means)},
            }
        rms_dict = {
            **{f'{direction}_Full_RMS': v for direction, v in zip(directions, v_rmss_full)},
            **{f'{direction}_1S_RMS': v for direction, v in zip(directions, v_rmss_unweighted)},
            **{f'{direction}_2S_RMS': v for direction, v in zip(directions, v_rmss)},
            }
        fwhm_dict = {
            **{f'{direction}_Full_FWHM': v for direction, v in zip(directions, v_fwhms_full)},
            **{f'{direction}_1S_FWHM': v for direction, v in zip(directions, v_fwhms_unweighted)},
            **{f'{direction}_2S_FWHM': v for direction, v in zip(directions, v_fwhms)},
            }
        trajs_dict = {
            **trajs_dict,
            **mean_dict, **rms_dict, **fwhm_dict,
            **fits_gaussian, **fits_maxwell}

        dfTrajAnalysis_dict[f'{traj_uid}_{delay_id}'] = trajs_dict

    data_sets_n2s = data_sets
    data_sets_n2s_ = {
        key: elem for key, elem in data_sets_n2s.items()
        if elem['Axes'] != -1}
    fig_traj_n2s = pyha.plot.PlotGen(
        plot_params=plot_params,
        data_sets=data_sets_n2s_)
    fig_traj_n2s.gen_plot()
    fig_traj_n2s.fig.set_constrained_layout_pads(h_pad=0.015)
    fig_traj_n2s.save_plot()

    data_sets_2s_exc_prob_ = {
        key: elem for key, elem in data_sets_2s_exc_prob.items()
        if elem['Axes'] != -1}
    fig_traj_2s_exc_prob = pyha.plot.PlotGen(
        plot_params=plot_params_2s_exc_prob,
        data_sets=data_sets_2s_exc_prob_)
    fig_traj_2s_exc_prob.gen_plot()
    fig_traj_2s_exc_prob.fig.set_constrained_layout_pads(h_pad=0.015)
    fig_traj_2s_exc_prob.save_plot()

    data_sets_ioni_ = {
        key: elem for key, elem in data_sets_ioni.items()
        if elem['Axes'] != -1}
    fig_traj_ioni = pyha.plot.PlotGen(
        plot_params=plot_params_ioni,
        data_sets=data_sets_ioni_)
    fig_traj_ioni.gen_plot()
    fig_traj_ioni.fig.set_constrained_layout_pads(h_pad=0.015)
    fig_traj_ioni.save_plot()

    dfTrajAnalysis = pd.DataFrame.from_dict(dfTrajAnalysis_dict, orient='index')
    # Rename indices to only trajectory set ID, without delay identifier
    # (needed above to make dict keys unique)
    dfTrajAnalysis.index = dfTrajAnalysis.index.map(lambda x: x.split('_')[0])
    dfTrajAnalysis = Trajs.cast_containers_to_data_format(dfTrajAnalysis, 'dfTrajAnalysis')
    Trajs.dfs['dfTrajAnalysis'] = dfTrajAnalysis

#%% Save analysis results

#filename_prefix = time.strftime("%Y-%m-%d %H-%M-%S")
#filename_out = filename_prefix + ' ' + metadata_filenames[0].split(' Params.dat')[0]
filename_out = metadata_filenames[0].split(' Params.dat')[0]
dir_out = dir_metadata
filepath_out = Path(dir_out, filename_out+' Analysis.dat')
# Write analysis results to CSV file
if len(dfTrajAnalysis) == 0:
    logger.info('No trajectory sets analyzed, no data written')
elif filepath_out.exists():
    logger.info('Analysis data file \'%s\' already exists, data not written', filepath_out)
else:
    Trajs.write_df_to_csv(
        dfTrajAnalysis, 'dfTrajAnalysis', filepath_out)
    logger.info('Wrote analysis data file \'%s\'', filepath_out)
#dfTrajParams.to_csv(
#    Path(dir_out, filename_out + ' Params.dat'))
