# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 16:45:30 2015

@author: Lothar Maisenbacher/MPQ

Functions for generating random atomic trajectory for Monte Carlo simulation
of 2S trajectories.

Tests are implemenented in `tests/test_random_traj.py`.
"""

import numpy as np
import random
import scipy.constants
import scipy.optimize
import scipy.integrate

# Seed random number generator, either using system time or OS-dependent sources
# For Windows, CryptGenRandom is used
random.seed()

## Create random trajectories

def random_xy_pos(radius):
    """
    Generate random `(x, y)` position within circle of radius `radius` and centered at `(x=0, y=0)`.
    """
    condition = True
    while condition:
        x = random.uniform(-radius, radius)
        y = random.uniform(-radius, radius)
        condition = x**2 + y**2 > radius**2
    return x, y

def random_theta_cos_law(theta_max):
    """
    Generate random polar angle (`theta`) in spherical coordinates,
    assuming a cosine emission law distribution.
    Cf. Eq. (4.28) of my thesis and comments therein.
    The maximum value of `theta` is given by `theta_max` and cannot exceed pi.
    """
    Amax = np.square(np.sin(theta_max))
    return np.arcsin(np.sqrt(random.uniform(0, Amax)))

def random_gauss(xMax, sigma):
    """
    Draw random number from Gaussian distribution with standard deviation `sigma`,
    for the condition that the absolute value of the random number is small than `xMax`.
    """
    return (
        np.sign(random.uniform(-1, 1))*sigma*np.sqrt(2)
        *scipy.special.erfinv(random.uniform(0, scipy.special.erf(xMax/np.sqrt(2)/sigma))))

def random_vel_maxwell(T, n, atomic_mass):
    """
    Draws a random speed `v` from the distribution
    `p(v) ~ v^n Exp[-v^2] Exp[-atomic_mass v^2/2 k_B T]`.
    For `v = 3`, this is the Maxwell distribution for a effusive beam at temperature `T` as used in
    our experiment
    (sampling a thermal distribution `~ v^2 Exp[-v^2]` through a small hole,
    see N.F. Ramsey - Molecular Beams, p. 152).
    The Rejection Method with the envelope function `A * v * Exp[-B*v^2]` is used as detailed in
    Numerical Recipes, 3rd Ed., Chapter 7.3.6.
    """
    # Parameters for envelope function
    if n <= 3:
        A = 0.8
        B = 0.5
    elif n <= 4:
        A = 0.8
        B = 0.35
    elif n <= 5:
        A = 0.9
        B = 0.22

    condition = True
    while condition:
        # Choose a random number between 0 and 1 - y value for integrated envelope function
        F0 = random.uniform(0, 1)
        # Get the corresponding x value
        x0 = np.sqrt(-np.log(1-F0)/B)
        # Calculate f(x) envelope for random x0
        fx = A*x0*np.exp(-B*x0**2)
        # Choose random y value beneath the envelope y value
        y = fx * random.uniform(0, 1)
        # If y value is bigger than p(x) value continue
        condition = y > x0**n * np.exp(-x0**2)
    # Calculate hydrogen mass
    macrMass = atomic_mass * scipy.constants.physical_constants['atomic mass constant'][0]
    # Scale random velocity by most probable speed of gas atoms in three dimensions, v = sqrt(2kT/m)
    gasdev = x0 * np.sqrt(2*scipy.constants.k*T/macrMass)
    return gasdev

def random_vel_maxwell_exp_suppr(T, n, atomic_mass, v_cut):
    """
    Draws a random speed `v` from the distribution
    `p(v) ~ v^n Exp[-atomic_mass v^2/2 k_B T] Exp[-v_cut/v]`.
    This is a Maxwell-Boltzmann-like distribution at temperature `T` with an additional
    cutoff speed `v_cut`.
    Cf. Eq. (4.27) of my thesis and comments therein.
    See also funtion `random_vel_maxwell`.
    """
    # Parameters for envelope function
    if n <= 3:
        A = 0.8
        B = 0.5
    elif n <= 4:
        A = 0.8
        B = 0.35
    elif n <= 5:
        A = 0.9
        B = 0.22

    # Calculate hydrogen mass
    macrMass = atomic_mass * scipy.constants.physical_constants['atomic mass constant'][0]
    # Calculate the most probable speed of gas atoms in three dimensions, v = sqrt(2kT/m)
    v_mp = np.sqrt(2*scipy.constants.k*T/macrMass)
    x_cut = v_cut/v_mp
    condition = True
    while condition:
        # Choose a random number between 0 and 1 - y value for integrated envelope function
        F0 = random.uniform(0, 1)
        # Get the corresponding x value
        x0 = np.sqrt(-np.log(1-F0)/B)
        # Calculate f(x) envelope for random x0
        fx = A*x0*np.exp(-B*x0**2)
        # Choose random y value beneath the envelope y value
        y = fx * random.uniform(0, 1)
        # If y value is bigger than p(x) value continue
        condition = y > x0**n * np.exp(-x0**2) * np.exp(-x_cut/x0)
    gasdev = x0 * v_mp
    return gasdev

def vector_thru_aperture(nozzle_radius, aperture1_width, aperture1_height,
                         aperture2_width, aperture2_height, aperture1_dist, aperture2_dist):
    """
    Find initial position and normalized velocity vector for a trajectory starting at a circular
    nozzle with radius `nozzle_radius` and passing through a two squares apertures at distances
    `aperture1_dist` and `aperture2_dist` with widths `aperture1_width` and `aperture2_width` and
    heights `aperture1_height` and `aperture2_height`, respectively.
    See also functions `random_xy_pos` and `random_theta_cos_law`.
    """
    # Random angle
    # Calculate the maximum polar angle through apertures
    theta_max1 = np.arctan(
        (nozzle_radius+np.max([aperture1_height, aperture1_width])/np.sqrt(2))/aperture1_dist)
    theta_max2 = np.arctan(
        (nozzle_radius+np.max([aperture2_height, aperture2_width])/np.sqrt(2))/aperture2_dist)
    theta_max = np.min([theta_max1, theta_max2])
#    print(theta_max)
    condition1 = True
    condition2 = True
    iC1 = 0
    while (condition1 or condition2):
        pos_nozzle = random_xy_pos(nozzle_radius)
        theta_nozzle = random_theta_cos_law(theta_max)
        phi_nozzle = random.uniform(0, 2*np.pi)
#        print(theta_nozzle)
#        print(phi_nozzle)
        trajAngles = np.array([np.cos(phi_nozzle), np.sin(phi_nozzle)]) * np.tan(theta_nozzle)
        pos_aperture2 = trajAngles * aperture2_dist + pos_nozzle
        pos_aperture1 = trajAngles * aperture1_dist + pos_nozzle
#        condition = not np.all(np.abs(posAperture) < apertureWidth/2)
        condition1 = (
            (pos_aperture2[0]**2 >= (aperture2_width/2)**2)
            or ((pos_aperture2[1])**2 >= (aperture2_height/2)**2))
        condition2 = (
            (pos_aperture1[0]**2 >= (aperture1_width/2)**2)
            or (pos_aperture1[1]**2 >= (aperture1_height/2)**2))
        iC1 = iC1 + 1

    trials = iC1

    return pos_nozzle, theta_nozzle, phi_nozzle, pos_aperture1, pos_aperture2, trials, theta_max

def random_traj(nozzle_radius, aperture1_width, aperture1_height, aperture2_width, aperture2_height,
               aperture1_dist, aperture2_dist, T, speed_dist_exp=3, exp_suppr_cutoff=0,
               atomic_mass=1.00794):
    """
    Find initial position and velocity vector for a trajectory starting at a circular
    nozzle with radius `nozzle_radius` and passing through a two squares apertures at distances
    `aperture1_dist` and `aperture2_dist` with widths `aperture1_width` and `aperture2_width` and
    heights `aperture1_height` and `aperture2_height`, respectively.
    The nozzle is held at temperature `T`, and the exponent of the velocity distribution is
    `speed_dist_exp`, and the cutoff speed is `exp_suppr_cutoff`.
    See also functions `random_xy_pos`, `random_theta_cos_law`, `random_vel_maxwell_exp_suppr`,
    `vector_thru_aperture`.
    """
    pos_nozzle, theta_nozzle, phi_nozzle, pos_aperture1, pos_aperture2, trials, theta_max = (
        vector_thru_aperture(
            nozzle_radius, aperture1_width, aperture1_height, aperture2_width, aperture2_height,
            aperture1_dist, aperture2_dist))
    pos_vector = np.append(pos_nozzle, 0)
    if speed_dist_exp <= 5:
        if exp_suppr_cutoff == 0:
            vel_magnitude = random_vel_maxwell(T, speed_dist_exp, atomic_mass)
        else:
            vel_magnitude = random_vel_maxwell_exp_suppr(
                T, speed_dist_exp, atomic_mass, exp_suppr_cutoff)
    else:
        print('speed_dist_exp > 5 not implemented yet')
        return False
    vel_vector = np.array([
        np.cos(phi_nozzle) * np.sin(theta_nozzle),
        np.sin(phi_nozzle) * np.sin(theta_nozzle),
        np.cos(theta_nozzle)
        ]) * vel_magnitude

    return pos_vector, vel_vector, vel_magnitude, trials, theta_max

def calc_fraction_spectroscopy_region(param):
    """
    Calculate the fraction of atoms emerging from the nozzle that make it
    into the spectroscopy region, assuming the cosine emission law.
    """
    # Fraction of angular distribution included in simulation
    angular_dist = lambda theta: np.sin(theta)*np.cos(theta)
    prob_angle_total = scipy.integrate.quad(
        angular_dist, 0, np.pi/2)[0]
    prob_angle_trajs = scipy.integrate.quad(
        angular_dist, 0, param['ThetaMax'])[0]

    # Fraction of trajectories making it into spectroscopy region
    return param['TrialProb']*prob_angle_trajs/prob_angle_total
