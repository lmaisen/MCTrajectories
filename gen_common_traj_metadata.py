# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 17:42:37 2020

@author: Lothar Maisenbacher/MPQ

Write parameters (metadata) of 2S trajectory set to JSON file.
This file is then read in by various script as base parameters to be further modified
or to generate the actual trajectory sets.
"""

import numpy as np
import datetime
import pandas as pd
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.trajs
import pyha.util.data

import traj_settings
config = traj_settings.load_config_file()
dir_metadata_input = config['MonteCarloTrajectories']['DirMetadataInput']

Trajs = pyha.trajs.Trajs()

#%% Set common parameters for single trajectory set
# and multiple trajectory sets

name = 'H2S6P2019_default'
filename = f'{name}.json'

# Delay set
delay_set_id = '2S6P'

# Load delays
delays = traj_settings.delay_sets[delay_set_id]
# Use only analysis delays
analysis_delays = {
    delay_id: delay for delay_id, delay in delays.items() if delay['Analysis']}

# Define dictionary with parameters
param = {
    # Number of trajectories
    'NTrajs': int(1e6),
    # Radius of the nozzle (m)
    'NozzleRadius': 1.00e-3,
    # Nozzle temperature (K)
    'NozzleTemp': 4.8,
    # Geometry identifier
    'Geometry': 'V9-3.2',
    # Distance to detection region from nozzle (m)
    # 2S-6P setup V9-3.2 (05-2019)
    'DetectionDist': 0.204,
    # Frequency of the 1S-2S preparation laser chopper (Hz)
    'ChopperFreq': 160,
    # Atomic detuning (Hz), i.e., detuning of sum of the two photons from the two-photon transition
    # frequency. The laser detuning is thus half this value.
    'AtomicDetuning': 970,
    # Waist radius of the 1S-2S preparation laser (m)
    'LaserWaistRadius': 297e-6,
    # Power of the 1S-2S preparation laser (intracavity, per direction) (W)
    'LaserPower': 1.1,
    # 243 nm waist position relative to nozzle orifice (m)
    # 2S-6P setup V9-3.2 (05-2019)
    'LaserWaistPos': 0.,
    ## Aperture 1 (first aperture after nozzle)
    # Aperture 1 width (m)
    'Aperture1Width': 2.4e-3,
    # Aperture 1 height (m)
    'Aperture1Height': 2.4e-3,
    # Distance to aperture 1 from nozzle (m)
    # 2S-6P setup V9-3.2 (05-2019)
    'Aperture1Dist': 69.0e-3,
    ## Aperture 2 (second aperture after nozzle)
    # Aperture 2 width (m)
    'Aperture2Width': 1.2e-3,
    # Aperture 2 height (m)
    'Aperture2Height': 2e-3,
    # Distance to aperture 2 from nozzle (m)
    # 2S-6P setup V9-3.2 (05-2019)
    'Aperture2Dist': 0.1536,
    # Total length of the detector (m)
    # used to calculate 2S decay probability during flight through detector
    # 2S-6P setup V9-3.2 (05-2019)
    'DetectorLength': 2*26.2e-3,
    # Speed distribution exponent
    'SpeedDistExp': 3,
    # Cutoff speed for exponential suppression of slow atoms
    # (prop. to exp(-v_cut/v)) (m/s)
    # Set to 0 to recover non-modified Maxwell-Boltzmann distribution
    'SpeedDistExpSupprCutOff': 65,
    ## Atomic species and mass
    # Hydrogen
    'Isotope': 'H',
    # Proton mass (u) (from CODATA 2018)
    'NucleusMass': 1.007276466621,
    # Hydrogen mass (u):
    # electron mass (u) * (1+1/(electron-to-proton mass ratio)+alpha^2/2))
    # (from CODATA 2018)
    'AtomicMass': 1.007825,
    # Deuterium
    # 'Isotope': 'D',
    # # Deuteron mass (u) (from CODATA 2018)
    # 'NucleusMass': 2.013553212745,
    # # Deuterium mass (u):
    # # electron mass (u) * (1+1/(electron-to-deuteron mass ratio)+alpha^2/2))
    # # (from CODATA 2018)
    # #atomicMass = 2.014102
    # # Hydrogen mass (u):
    # # electron mass in u * (1+1/(electron-to-proton mass ratio)+alpha^2/2))
    # # (from CODATA 2018)
    # 'AtomicMass': 2.014102,
    # Delay set
    'DelaySetID': delay_set_id,
    'NDelays': len(analysis_delays),
    'Timestamp': pd.Timestamp(datetime.datetime.utcnow()),
    'IntDelayWidth': 10e-6,
    'NMultipleChopperCycles': 0,
    'TrialProb': np.nan,
    'ThetaMax': np.nan,
    'FractionTrajsSpectroscopyRegion': np.nan,
    'NMinIntDelays': -1,
    }
sr_param_common = pd.Series(param)
sr_param_common.name = name

# Write to JSON file
filepath = Path(dir_metadata_input, filename)
Trajs.dump_series_to_json(sr_param_common, filepath, 'dfTrajParams')
logger.info('Wrote trajectory set parameters \'%s\' to JSON file at \'%s\'', name, filepath)
