# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 09:00:09 2023

@author: Lothar Maisenbacher/MPQ

Remove trajectory sets, analysis plots for given trajectory set UIDs.
"""

import argparse
import glob
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.trajs

import traj_settings

Trajs = pyha.trajs.Trajs()

#%% (Default) settings and parameters

default_traj_uids_to_remove = ['C1A3E9L3', 'Gsu6lXj6']

#%% Get input parameters

parser = argparse.ArgumentParser(
    description='Remove trajectory sets.')
parser.add_argument(
    '-t', '--Trajs', nargs='+', help='Trajectory set UIDs to remove',
    default=default_traj_uids_to_remove)

config = traj_settings.load_config_file(parser=parser)
command_line_args = vars(parser.parse_args())

traj_uids_to_remove = command_line_args['Trajs']
dir_traj_metadata = config['MonteCarloTrajectories']['DirMetadata']
dir_traj_plots = config['MonteCarloTrajectories']['DirPlots']
dir_traj_sets = config['MonteCarloTrajectories']['DirSets']

#%% Load existing trajectory set metadata

# Search for metadata files in given metadata directory
metadata_filepaths = glob.glob(str(Path(dir_traj_metadata, '* Params.dat')))
logger.info('Found %d metadata file(s)', len(metadata_filepaths))
analysis_filepaths = glob.glob(str(Path(dir_traj_metadata, '* Analysis.dat')))
logger.info('Found %d analysis file(s)', len(analysis_filepaths))

Trajs.load_trajs_metadata(metadata_filepaths)
Trajs.load_trajs_analysis_results(analysis_filepaths)
dfTrajParams = Trajs.dfTrajParams
dfTrajAnalysis = Trajs.dfTrajAnalysis

#%% Remove entries from metadata (to be implemented)

mask_metadata = dfTrajParams.index.isin(traj_uids_to_remove)
mask_analysis = dfTrajAnalysis.index.isin(traj_uids_to_remove)

#%% Remove files

for traj_uid in traj_uids_to_remove:

    # Delete calculated sets
    filepath = Path(dir_traj_sets, f'{traj_uid}.npz')
    if filepath.exists():
        logger.info('Removing file \'%s\'', filepath.name)
        filepath.unlink()

    # Delete analysis plots
    for suffix in ['', ' 2S exc prob', ' 2S ioni prob']:
        filepath = Path(dir_traj_plots, f'{traj_uid}{suffix}.pdf')
        if filepath.exists():
            logger.info('Removing file \'%s\'', filepath.name)
            filepath.unlink()

#%% Remove grid interpolations based on these trajectory sets (to be implemented)
