# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 11:41:18 2020

@author: Lothar Maisenbacher/MPQ

Test of numerical integration of OBEs describing 1S-2S excitation.
Particular care has to be taken to include the delayed detection correctly, which is tested
here in detail.
"""

import numpy as np
import matplotlib.pyplot as plt
import random
import time
import scipy.integrate
from pathlib import Path

# pyhs
import pyhs.fit

# pyha
import pyha.plot
import pyha.def_latex

# Load OBEs
import sys
sys.path.append(str(Path('../').resolve()))
import cython_obe.cOBE_1S2S as OBE_1S2S # Using cythonized 1S-2S OBEs
import traj_settings

import pyha.plts as plts
plts.set_params()

#%% Test functions for OBEs

nucleus_mass = 1.007276466621  # Proton mass in u (from CODATA 2018)

# Define dictionary with parameters
param = {
    'LaserWaistRadius': 297e-6,
    'LaserPower': 1.0,
    'LaserWaistPos': 0,
    'NucleusMass': nucleus_mass,
    'DetectionDist': 0.204,
    'ChopperFreq': 160,
    'DelaySetID': '2S6P',
}

myOBE1s2s = OBE_1S2S.OBE_1S2S(param['NucleusMass'])

# Define atom trajectory
#velVector = np.array([
#    1.86284374e-01,
#    -5.26765750e-01,
#    5.96226854e+01
#    ])
#posVector = np.array([
#    -3.21663477e-04,
#    3.71826233e-04,
#    0.
#    ])
alpha_offset = 4e-3
velVector = np.array([
    200.*np.sin(alpha_offset),
    0.,
    200.*np.cos(alpha_offset)
    ])
posVector = np.array([
    0.,
    0.,
    0.
    ])
myOBE1s2s.velVector = velVector
myOBE1s2s.startVector = posVector

# For all delays
myOBE1s2s.w0 = float(param['LaserWaistRadius'])
myOBE1s2s.power_laser = float(param['LaserPower'])
myOBE1s2s.z_waist = float(param['LaserWaistPos'])

fig_odeint = plt.figure(
    'Test odeint 1S-2S', constrained_layout=True)
fig_odeint.clf()
ax1 = fig_odeint.gca()

detection_dist = param['DetectionDist']
chopper_freq = param['ChopperFreq']

##%% Scan 1S-2S atomic detuning for given delay time

t_detect = detection_dist/velVector[2]

# Single trajectory for given delay time tau
tau = 0e-6

# Build integration times
dt = 1e-6
t_max = t_detect-tau
t_min = np.max([
    0, t_max-1/(2*chopper_freq)
    ])
ts_no_laser = np.linspace(0, t_min, int(t_min/dt))
ts_to_integrate = np.linspace(t_min, t_max, int((t_max-t_min)/dt))
dts_to_decay = np.linspace(0, tau, int((tau)/dt))
ts_ = np.hstack((ts_no_laser, ts_to_integrate, dts_to_decay+ts_to_integrate[-1]))
ts, ts_unique_inds = np.unique(ts_, return_index=True)

# Frequency scan
domegas = np.arange(-1e2, 1.1e2, 1e-1)
domegas = np.linspace(
    -5e3, 10e3, 201)
odeint_results = np.zeros((len(ts_to_integrate), 4, len(domegas)))
initial = [1, 0, 0, 0]
for i_domega, domega in enumerate(domegas):
    # Set atomic detuning
    myOBE1s2s.atomic_detuning = domega
    # Integrate OBEs
    odeint_results[:, :, i_domega] = scipy.integrate.odeint(
        myOBE1s2s.derivs, initial,
        ts_to_integrate)

i_domega_max = np.argmax(odeint_results[-1, 3, :])

# Decay over time not spent in laser beam
population_2s = odeint_results[-1, 3, i_domega_max]
exc_prob_decay = population_2s * np.exp(-myOBE1s2s.gamma_s * dts_to_decay)
exc_prob = np.hstack((
    np.zeros(len(ts_no_laser)),
    odeint_results[:, 3, i_domega_max],
    exc_prob_decay))
exc_prob = exc_prob[ts_unique_inds]

population_1s_growth = (
    odeint_results[-1, 0, i_domega_max]+population_2s-exc_prob_decay)
population_1s = np.hstack((
    np.ones(len(ts_no_laser)),
    odeint_results[:, 0, i_domega_max],
    population_1s_growth))
population_1s = population_1s[ts_unique_inds]

fig_odeint.clf()

# Plot population in excited state as function of laser detuning
ax1 = fig_odeint.add_subplot(211)
ax1.plot(domegas, odeint_results[-1, 3, :])

# Plot population in excited state as a function of time
ax2 = fig_odeint.add_subplot(212)
ax2.plot(
    ts,
    exc_prob)
ax2.plot(
    ts,
    1-(population_1s+exc_prob)
    )
#ax2.plot(
#    ts,
#    population_1s)
#ax2.plot(
#    ts,
#    np.sin(2*np.pi*531*ts)**2
#    )

#%% Excitation probability for different delays

fig_odeint.clf()
ax1 = fig_odeint.add_subplot(311)
ax2 = fig_odeint.add_subplot(312)
ax3 = fig_odeint.add_subplot(313)

domega = domegas[i_domega_max]

# Load delays
delays = traj_settings.delay_sets[param['DelaySetID']]
# Use only analysis delays
analysis_delays = {delay_id: delay for delay_id, delay in delays.items() if delay['Analysis']}

## Delays to use in numerical integration
dt = 50e-6
delays_min_start_time = np.min([delay['StartTime'] for _, delay in analysis_delays.items()])
delays_max_end_time = np.max([delay['EndTime'] for _, delay in analysis_delays.items()])

#num_int_delays = int(np.ceil((delays_max_end_time-dt0)/dt)+1)
#int_delays_start_time = np.array([0] + [elem*dt+10e-6 for elem in range(num_int_delays-1)])
#int_delays_end_time = np.array([dt0] + [elem*dt+10e-6+dt for elem in range(num_int_delays-1)])

num_int_delays = int(np.ceil((delays_max_end_time-delays_min_start_time)/dt))
int_delays_end_time = np.linspace(delays_min_start_time+dt, delays_max_end_time, num_int_delays)
int_delays_start_time = int_delays_end_time-dt

#%%

# Time at which trajectory starts, with zero time corresponding to position of
# nozzle output orifice
t_begin = 0
# Time at which trajectory crosses 2S-nP laser
t_detect = detection_dist/velVector[2]

# Neglecting the effect of very slow trajectories seeing more than one chopper cycle,
# i.e. only last chopper cycle included
if t_detect  > 1/chopper_freq:
    print('Trajectory sees more than one chopper cycle')

# Generate random time within delay constraints
taus_random = np.array([
    random.uniform(int_delays_start_time[i], int_delays_end_time[i])
    for i in range(num_int_delays)])
#taus_random = int_delays_start_time
ts_in_laser = t_detect-taus_random

# Density matrices for each delay
rhos = np.zeros((len(taus_random), 4))

# Prepare OBEs
initial = [1, 0, 0, 0]
myOBE1s2s.atomic_detuning = domega

#%% Integrate delays for which trajectories start when laser is on, i.e. at
# t <= 0. These delays can be integrated in one calculation, as the start time
# is identical for all and only the end time of the integration differs.

mask_starts_in_laser = (taus_random >= t_detect-1/chopper_freq/2)

if np.sum(mask_starts_in_laser) > 0:

    # Build integration times, always integrating for t > t_begin
    ts_to_integrate = np.flipud(ts_in_laser[mask_starts_in_laser])
    ts_to_integrate[ts_to_integrate < t_begin] = t_begin
    ts_to_integrate = np.insert(ts_to_integrate, 0, t_begin)

    start_time = time.time()

    ts_to_integrate_unique, unique_indices = np.unique(ts_to_integrate, return_inverse=True)
    odeint_results_unique = scipy.integrate.odeint(
        myOBE1s2s.derivs, initial,
        ts_to_integrate_unique)
    odeint_results = odeint_results_unique[unique_indices]

    print(f'Calc. time: {1e3*(time.time()-start_time):.3f} ms')

    # Flip results back to delay order and remove time zero
    rhos[mask_starts_in_laser] = np.flipud(odeint_results[1:, :])

#%%

# Plot population in excited state as a function of time
if np.sum(mask_starts_in_laser) > 0:
    ax1.plot(
        ts_to_integrate,
        odeint_results[:, 3],
        marker='o', markersize=2)

#%% Integrate delays for which laser is switched on after trajectory has left
# nozzle, i.e. for t > 0. Here, every delay has to be integrated individually,
# as both start and end time are different for each delay

start_time = time.time()

for i_tau, tau in enumerate(taus_random[~mask_starts_in_laser]):

    t_max = t_detect-tau
    t_min = np.max([
        0, t_max-1/(2*chopper_freq)
        ])
    ts_to_integrate = np.array([
        t_min, t_max])
#    print(ts_to_integrate)
#    print(np.ptp(ts_to_integrate)*1e3)

    odeint_result = scipy.integrate.odeint(
        myOBE1s2s.derivs, initial,
        ts_to_integrate,
        atol=1e-8, rtol=1e-8,
        )
    rhos[i_tau] = odeint_result[-1]

print(f'Calc. time: {1e3*(time.time()-start_time):.3f} ms')

#%%

# Decay over time not spent in laser beam
exc_prob = myOBE1s2s.decayOnly(rhos[:, 3], taus_random)

#%% Plot

# Plot excitation probability vs delay time tau
ax2.plot(
    taus_random,
    exc_prob,
    )

#%% Built experimental delays from simulation delays

# Built delays to export
int_delays_start_ind = np.array([
    np.argmin(np.abs(int_delays_start_time-delay['StartTime']))
    for _, delay in analysis_delays.items()])
int_delays_end_ind = np.array([
    np.argmin(np.abs(int_delays_end_time-delay['EndTime']))
    for _, delay in analysis_delays.items()])
num_int_delays_per_delay = int_delays_end_ind-int_delays_start_ind+1

expExcProbList = np.zeros((1, len(analysis_delays)))
for iDelay in range(len(analysis_delays)):
    expExcProbList[0, iDelay] = np.sum(
        exc_prob[int_delays_start_ind[iDelay]:int_delays_end_ind[iDelay]+1])

#%% Plot amplitude and excitation probability of 2S trajectories vs delays

#ax3.plot(
#    expExcProbList[0],
#    )
ax3.plot(
    expExcProbList[0]/num_int_delays_per_delay,
    )

#%% Integrate delays individually

#t_begin = 0
#t_detect = detection_dist/velVector[2]
#
#start_time = time.time()
#
#initial = [1, 0, 0, 0]
#odeint_results_dlys = np.zeros((len(analysis_delays), 4))
#for i_delay, (delay_id, delay) in enumerate(analysis_delays.items()):
#
#    t_max = t_detect-(delay['StartTime']+delay['EndTime'])/2
#    t_min = np.max([
#        0, t_max-1/(2*chopper_freq)
#        ])
#    ts_to_integrate = np.array([
#        t_min, t_max])
#    ts_to_integrate = np.linspace(*ts_to_integrate, 100)
#    print(delay_id)
##    print(ts_to_integrate*1e3)
#    print(np.ptp(ts_to_integrate)*1e3)
#
#    myOBE1s2s.atomic_detuning = domega
#    odeint_results = scipy.integrate.odeint(
#        myOBE1s2s.derivs, initial,
#        ts_to_integrate)
#    odeint_results_dlys[i_delay] = odeint_results[-1, :]
#
#print(f'Calc. time: {1e3*(time.time()-start_time):.3f} ms')
#
#ax2.cla()
#ax2.plot(
#    expExcProbList[0]/num_calc_delays,
#    )
#ax2.plot(
#    odeint_results_dlys[:, 3],
#    )

#%%

ts_to_integrate = np.array([0, 3e-3])
ts_to_integrate += -1.5e-3
print(np.ptp(ts_to_integrate))
odeint_results = scipy.integrate.odeint(
    myOBE1s2s.derivs, initial,
    ts_to_integrate)
print(odeint_results)

#ode_solver_method = 'Radau'
#obe_func = lambda t, y: myOBE1s2s.derivs(y, t)
#sol = scipy.integrate.solve_ivp(
#    obe_func, (np.min(ts_to_integrate), np.max(ts_to_integrate)),
#    initial, t_eval=ts_to_integrate, method=ode_solver_method,
#    atol=1e-10, rtol=5e-11, first_step=1e-12)
#print(sol.y)

#%%

# Generate random time within delay constraints
#tDelayRandom = np.array([
#    random.uniform(simDelayStartTime[i], simDelayEndTime[i])
#    for i in range(nSimDelays)])
#tInLaser = t_detect-tDelayRandom

#%%

## Delays to use in calculation
#dt = 10e-6
#delays_min_start_time = np.min([delay['StartTime'] for _, delay in analysis_delays.items()])
#delays_max_end_time = np.max([delay['EndTime'] for _, delay in analysis_delays.items()])
#num_int_delays = int(np.ceil((delays_max_end_time-delays_min_start_time)/dt)+1)
#simDelayStartTime = np.array([0] + [elem*dt+10e-6 for elem in range(nSimDelays-1)])
#simDelayEndTime = np.array([dt0] + [elem*dt+10e-6+dt for elem in range(nSimDelays-1)])
#
#int_delays_end_time = np.linspace(delays_min_start_time, delays_max_end_time, num_int_delays)
#int_delays_start_time = int_delays_end_time-dt
#
#tBegin = 0
#t_detect = detection_dist/velVector[2]
#
## Generate random time within delay constraints
#tDelayRandom = np.array([
#    random.uniform(simDelayStartTime[i], simDelayEndTime[i])
#    for i in range(nSimDelays)])
#tInLaser = t_detect-tDelayRandom

# FFT of decay and Lorentzian fit to extract natural linewidth
#myOBE1s2s = OBE1s2s()
##myOBE1s2s.domega = 0
#tlimit = [0, 10]
#dt = 1e-4
#t = np.arange(tlimit[0], tlimit[1], dt)
#initial = [0, 0, 0, 1]
#y = scipy.integrate.odeint(myOBE1s2s.derivs, initial, t)
#
#popt, pcov = scipy.optimize.curve_fit(fitfunc.ExpDecay, t, y[::, 3])
#plt.plot(t, y[::, 3])
#plt.plot(t, fitfunc.ExpDecay(t, popt[0]))
#print(popt)
#plt.figure()
#data = np.abs(y[::, 3])**(1/2) # Fourier transform of field!
#fft = np.abs(np.fft.rfft(data))
#freq = np.fft.rfftfreq(len(t), dt)
#
#freqext = np.concatenate((-np.flipud(freq)[0:-1], freq))
#fftext = np.concatenate((np.flipud(fft)[0:-1], fft))
#fftext2 = fftext**2 # Square to get intensity spectrum
#
#plt.plot(freqext, fftext2)
#plt.xlim([-10, 10])
#popt, pcov = scipy.optimize.curve_fit(fitfunc.Lorentzian, freqext, fftext2, [1, 0, np.max(fftext2), 0.5])
#print(popt)
#plt.plot(freqext, fitfunc.Lorentzian(freqext, popt[0], popt[1], popt[2], popt[3]))
#plt.xlim([-3, 3])

# Lorentzian fit
#flimit = [-1e2, 1e2]
#df = 0.1
#f = np.arange(flimit[0], flimit[1]+dt, df)
#fit = fitfunc.Lorentzian(f, 1, 0)
#plt.figure()
#plt.plot(f, fit)
#popt, pcov = scipy.optimize.curve_fit(fitfunc.Lorentzian, f, fit)
#plt.plot(f, fitfunc.Lorentzian(f, popt[0], popt[1]))

# Laser intensity profile
#myOBE1s2s = OBE1s2s()
#x = np.arange(-1e-3, 1e-3, 1e-5)
#z = np.arange(0, 35e-2, 1e-3)
#plt.figure()
#plt.plot(z, myOBE1s2s.intensityAtPos([0, 0, z]))
#plt.figure()
#xInt = myOBE1s2s.intensityAtPos([x, 0, 0])
#plt.plot(x, xInt)
##plt.plot(x, myOBE1s2s.intensityAtPos([0, x, 0]))
#popt, pcov = scipy.optimize.curve_fit(fitfunc.GaussFit, x, xInt, [0, 3e-6, np.max(xInt), 0])
#print(popt[1])
#plt.plot(x, fitfunc.GaussFit(x, popt[0], popt[1], popt[2], popt[3]))
#plt.figure()
#xInt = myOBE1s2s.intensityAtPos([x, 0, 34.5e-2])
#plt.plot(x, xInt)
##plt.plot(x, myOBE1s2s.intensityAtPos([0, x, 34.5e-2]))
#popt, pcov = scipy.optimize.curve_fit(fitfunc.GaussFit, x, xInt, [0, 3e-6, np.max(xInt), 0])
#print(popt[1])
#plt.plot(x, fitfunc.GaussFit(x, popt[0], popt[1], popt[2], popt[3]))
#print(myOBE1s2s.lambda_laser)

#%% Plot 2S excitation and ionization probability as a function of 1S-2S detuning
# and interaction time

myFit = pyhs.fit.Fit()

## Shorthands
# Latex
ltx = pyha.def_latex.defs

posVector = np.array([
    0.,
    0.,
    0.
    ])
myOBE1s2s.startVector = posVector

# For all delays
myOBE1s2s.w0 = float(param['LaserWaistRadius'])
myOBE1s2s.power_laser = float(param['LaserPower'])
myOBE1s2s.power_laser = 1
myOBE1s2s.z_waist = float(param['LaserWaistPos'])

detection_dist = param['DetectionDist']
chopper_freq = param['ChopperFreq']

# Single trajectory for given delay time tau
tau = 0e-6
# Time steps to sample
dt = 1e-6
# Frequency sampling
domegas = np.linspace(
    -6e3, 10e3, 201)

#fig_odeint = plt.figure(
#    'Test odeint 1S-2S', constrained_layout=True)
#fig_odeint.clf()
#ax1 = fig_odeint.add_subplot(211)
#ax2 = fig_odeint.add_subplot(212)

plot_params = {
    'FigName': '2S excitation and ionization probability',
    'SaveSuffix': '2S excitation and ionization probability',
    'FigSize': [plts.pagewidth*0.8, 3.6],
    'Save': False,
    'SaveOverwrite': True,
    'SubplotLabelY': 0.05,
    }

plot_params['AxParams0'] = {
#    'HeightRatio': 0.7,
    'CustomYLabel': '2S excitation\nprobability (%)',
    'CustomXLabel': (
        '1S-2S detuning '+ltx['DetOneSTwoS']
        +' (Hz)'
        ),
    'YLim': [-1, 17],
    'XLim': [-4e3, 7e3],
    'SubplotLabel': 'A',
    }
plot_params['AxParams1'] = {
    'HeightRatio': 0.7,
    'CustomYLabel': '2S excitation\nprobability (%)',
    'RemoveXTickLabels': True,
    'RemoveXTicks': True,
    'LinYTicks': [0, 5, 10, 15],
    'SubplotLabel': 'B',
    }
plot_params['AxParams2'] = {
    'HeightRatio': 0.7,
    'CustomYLabel': 'Ionization\nprobability (%)',
#    'YScaleType': 'log',
    'CustomXLabel': 'Interaction time $t$ (µs)',
    'LinYTicks': [0, 50, 100],
    'SubplotLabel': 'C',
    }

alpha_offsets = [0, 4e-3, 10e-3]
data_sets = {}
colors = plts.get_line_colors(cmap='tab10')
for i, alpha_offset in enumerate(alpha_offsets):

    velVector = np.array([
        200.*np.sin(alpha_offset),
        0.,
        200.*np.cos(alpha_offset)
        ])
    myOBE1s2s.velVector = velVector

    t_detect = detection_dist/velVector[2]

    t_max = t_detect-tau
    t_min = np.max([
        0, t_max-1/(2*chopper_freq)
        ])
    ts_no_laser = np.linspace(0, t_min, int(t_min/dt))
    ts_to_integrate = np.linspace(t_min, t_max, int((t_max-t_min)/dt))
    dts_to_decay = np.linspace(0, tau, int((tau)/dt))
    ts_ = np.hstack((ts_no_laser, ts_to_integrate, dts_to_decay+ts_to_integrate[-1]))
    ts, ts_unique_inds = np.unique(ts_, return_index=True)

    # Frequency scan
    odeint_results = np.zeros((len(ts_to_integrate), 4, len(domegas)))
    initial = [1, 0, 0, 0]
    for i_domega, domega in enumerate(domegas):
        # Set atomic detuning
        myOBE1s2s.atomic_detuning = domega
        # Integrate OBEs
        odeint_results[:, :, i_domega] = scipy.integrate.odeint(
            myOBE1s2s.derivs, initial,
            ts_to_integrate)

    # Fit resonance
    fit_func_id = 'Lorentzian'
    fit_func = myFit.fit_funcs[fit_func_id]
    fit_result, error_msg = myFit.do_fit(
        fit_func,
        domegas, odeint_results[-1, 3, :],
    #    pstart=pstart_para,
        out_func=print)
    sr_fit = myFit.fit_result_to_dict(
        fit_result, fit_func)
    i_omega_cfr = np.argmin(np.abs(sr_fit['CFR_Value']-domegas))

    i_domega_max = np.argmax(odeint_results[-1, 3, :])

    i_domega_plot = i_omega_cfr

    # Decay over time not spent in laser beam
    population_2s = odeint_results[-1, 3, i_domega_plot]
    exc_prob_decay = population_2s * np.exp(-myOBE1s2s.gamma_s * dts_to_decay)
    exc_prob = np.hstack((
        np.zeros(len(ts_no_laser)),
        odeint_results[:, 3, i_domega_plot],
        exc_prob_decay))
    exc_prob = exc_prob[ts_unique_inds]

    population_1s_growth = (
        odeint_results[-1, 0, i_domega_plot]+population_2s-exc_prob_decay)
    population_1s = np.hstack((
        np.ones(len(ts_no_laser)),
        odeint_results[:, 0, i_domega_plot],
        population_1s_growth))
    population_1s = population_1s[ts_unique_inds]

    ioni_prob = 1-(population_1s+exc_prob)


    # Plot population in excited state as function of laser detuning
    data_sets[f'Resonance{alpha_offset}mrad'] = {
        'Axes': 0,
        'XData': domegas,
        'YData': 100*odeint_results[-1, 3, :],
        'BasisParams': 'Line',
        'XYParams': {'color': colors[i]},
        'Label': (
            ltx['dalpha']+f' = {1e3*alpha_offset:.0f} mrad'
            +', '+ltx['CFR']+f' = {sr_fit["CFR_Value"]:.0f} Hz'
            +', '+ltx['GammaL']+f' = {1e-3*sr_fit["GammaL_Value"]:.0f} kHz'
            ),
        }
    data_sets[f'ResonanceFit{alpha_offset}mrad'] = {
        'Axes': 0,
        'XData': domegas,
        'YData': 100*fit_func['Func'](domegas, *fit_result['Popt']),
        'BasisParams': 'Line',
        'XYParams': {
            'color': colors[i], 'linestyle': '--', 'alpha': 0.5,},
        }
#    ax1.plot(domegas, odeint_results[-1, 3, :])

    # Plot population in excited state as a function of time
    data_sets[f'2SExcProb{alpha_offset}mrad'] = {
        'Axes': 1,
        'XData': ts,
        'YData': 100*exc_prob,
        'BasisParams': 'Line',
        'XYParams': {'color': colors[i]},
        'XPrefixExp': 1e-6,
        }
#    ax2.plot(
#        ts,
#        exc_prob)
#    ax2.plot(
#        ts,
#        1-(population_1s+exc_prob)
#        )
    data_sets[f'IoniProb{alpha_offset}mrad'] = {
        'Axes': 2,
        'XData': ts,
        'YData': 100*ioni_prob,
        'BasisParams': 'Line',
        'XYParams': {'color': colors[i], 'linestyle': '-'},
        'XPrefixExp': 1e-6,
        }
    #ax2.plot(
    #    ts,
    #    population_1s)
    #ax2.plot(
    #    ts,
    #    np.sin(2*np.pi*531*ts)**2
    #    )

fig_1s2s_traj = pyha.plot.PlotGen(
    plot_params=plot_params, data_sets=data_sets)
fig_1s2s_traj.gen_plot()

fig_1s2s_traj.fig.set_constrained_layout_pads(h_pad=0.01)

fig_1s2s_traj.save_plot()
