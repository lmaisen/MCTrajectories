# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 08:54:52 2022

@author: Lothar Maisenbacher/MPQ

Test functions of `random_traj.py`.
"""

import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import scipy.constants
from pathlib import Path

import sys
sys.path.append(str(Path('../').resolve()))
import random_traj

# pyhs
import pyhs.fitfunc

def test_xy_pos(num_trajs):
    """Test function `random_traj.random_xy_pos` with `num_traj` trajectories."""
    XYpos = np.zeros((num_trajs, 2))
    for i in range(num_trajs):
        XYpos[i] = random_traj.random_xy_pos(0.1)
    plt.figure('Test: Seeding of random position in circle', clear=True, constrained_layout=True)
    plt.axes(aspect='equal')
    plt.scatter(XYpos[::,0], XYpos[::,1])
    plt.xlabel('x (mm)')
    plt.ylabel('y (mm)')
    plt.title('Starting point distribution for trajectories on nozzle')
    plt.xlim([-0.12,0.12])
    plt.ylim([-0.12,0.12])

def test_theta_cos_law(num_trajs):
    """Test function `random_traj.random_theta_cos_law` with `num_traj` trajectories."""
    theta = np.zeros((num_trajs, 1))
    for i in range(num_trajs):
        theta[i] = random_traj.random_theta_cos_law(np.pi/4)
    plt.figure(
        'Trajectory test: Angular distribution according to cosine emission law',
        clear=True, constrained_layout=True)
    bins = plt.hist(theta, bins=50)
    x = np.arange(0, np.max(bins[1]), np.max(bins[1])/1000)
    y = np.cos(x) * np.sin(x) * np.max(bins[0]) * 2
    plt.xlabel(r'$\theta$ (rad)')
    plt.ylabel('Counts')
    plt.title(r'Cosine emission law distribution of polar angle $\theta$')
    plt.xlim([-0.1, 0.9])
    plt.ylim([0,400])

def test_vel_maxwell(num_trajs, n):
    """
    Test function `random_traj.random_vel_maxwell` with `num_traj` trajectories
    and with velocity distribution exponent `n`.
    """
    T = 4.8
    num_trajs = int(num_trajs)
    vel = np.zeros((num_trajs,1))
    for i in range(num_trajs):
        vel[i] = random_traj.random_vel_maxwell(T, n, 1.00794)
    plt.figure(
        'Trajectory test: Maxwell-Boltzmann velocity distribution',
        clear=True, constrained_layout=True)
    bins = plt.hist(vel, bins=50)
    x = np.arange(0, np.max(bins[1]), np.max(bins[1])/1000)
    hydrogenMass = 1.00794 * scipy.constants.physical_constants['atomic mass constant'][0]
    y = (
        x**n * np.exp(-x**2/(2*scipy.constants.k*T/hydrogenMass))
        * np.max(bins[0]) * 1/0.409916 / (2*scipy.constants.k*T/hydrogenMass)**(3/2))
    plt.plot(x,y)
    plt.xlabel('velocity [m/s]')
    plt.ylabel('Counts')
    plt.title(r'Maxwell-Boltzmann velocity distribution $\propto v^{:d}$ after nozzle'.format(n))
    plt.xlim([0, 1000])
#    plt.ylim([0,700])

def test_vel_maxwell_exp_suppr(num_trajs, n, v_cut):
    """
    Test function `random_traj.random_vel_maxwell_exp_suppr` with `num_traj` trajectories
    and with velocity distribution exponent `n` and cutoff speed `v_cut`.
    """
    T = 4.8
    num_trajs = int(num_trajs)
    vel = np.zeros((num_trajs,1))
    for i in range(num_trajs):
        vel[i] = random_traj.random_vel_maxwell_exp_suppr(T, n, 1.00794, v_cut)
    plt.figure(
        'Trajectory test: Maxwell-Boltzmann velocity distribution with cutoff speed',
        clear=True, constrained_layout=True)
    ax1 = plt.gca()
    hist, bin_edges = np.histogram(
        vel,
        bins=50,
        )
    x_bins = bin_edges[:-1] + np.diff(bin_edges)/2
    ax1.bar(
        x_bins,
        hist,
        width=np.diff(bin_edges),
        )
    x = np.linspace(0, np.max(x_bins), 1000)
    hydrogenMass = 1.00794 * scipy.constants.physical_constants['atomic mass constant'][0]
    dist_wo_suppr = (lambda x, a:
        a * x**n * np.exp(-x**2/(2*scipy.constants.k*T/hydrogenMass))
        * 1/0.409916 / (2*scipy.constants.k*T/hydrogenMass)**(3/2)
        )
    # Adjust amplitude
    popt_wo_suppr, _ = scipy.optimize.curve_fit(
        dist_wo_suppr,
        x_bins, hist,
        [np.max(hist)]
        )
    dist = (lambda x, a:
        a * x**n * np.exp(-x**2/(2*scipy.constants.k*T/hydrogenMass))
        * 1/0.409916 / (2*scipy.constants.k*T/hydrogenMass)**(3/2)
        * np.exp(-v_cut/x)
        )
    # Adjust amplitude
    popt, _ = scipy.optimize.curve_fit(
        dist,
        x_bins, hist,
        [np.max(hist)]
        )
    ax1.plot(
        x, dist_wo_suppr(x, *popt_wo_suppr), color='r',
        label='Maxwell-Boltzmann'
        )
    ax1.plot(
        x, dist(x, *popt), color='k',
        label='Maxwell-Boltzmann w/\nexponential suppression of slow atoms'
        )
    plt.xlabel('velocity (m/s)')
    plt.ylabel('Counts')
    plt.legend()
#    plt.title('Maxwell velocity distribution v^{:d} after nozzle'.format(n))
    plt.xlim([0, 1000])

def test_vector_thru_aperture(num_trajs):
    """Test function `random_traj.vector_thru_aperture` with `num_traj` trajectories."""
    nozzle_radius = 0.95
    aperture1_width =  2
    aperture1_height = 2
    aperture1_dist = 15
    aperture2_width= 2
    aperture2_height = 2
    aperture2_dist = 114

    num_trials = 0
    XY1pos = np.zeros((num_trajs,2))
    XY2pos = np.zeros((num_trajs,2))
    theta = np.zeros((num_trajs,1))
    for i in range(num_trajs):
        pos_nozzle, theta_nozzle, phi_nozzle, pos_aperture1, pos_aperture2, trials, theta_max = (
            random_traj.vector_thru_aperture(
                nozzle_radius, aperture1_width, aperture1_height, aperture2_width, aperture2_height,
                aperture1_dist, aperture2_dist))
        XY1pos[i] = pos_aperture1
        XY2pos[i] = pos_aperture2
        theta[i] = theta_nozzle
        num_trials += trials

    trial_prob = 1/(num_trials/num_trajs)

    plt.figure(
        'Trajectory test: Spatial distribution through aperture 1',
        clear=True, constrained_layout=True)
    plt.axes(aspect = 'equal')
    plt.scatter(XY1pos[::, 1], XY1pos[::, 0])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')
    plt.title(
        'Distribution of trajectories through aperture 1\n'
        +f'Width: {aperture1_width} mm, heigth: {aperture1_height} mm')

    plt.figure('Trajectory test: Spatial distribution through aperture 2')
    plt.clf()
    plt.axes(aspect = 'equal')
    plt.scatter(XY2pos[::, 1], XY2pos[::, 0])
    plt.xlabel('Width: x (mm)' )
    plt.ylabel('Height: y (mm)')
    plt.title(
        'Distribution of trajectories through aperture 2\n'
        +f'Width: {aperture2_width} mm, heigth: {aperture2_height} mm')

    plt.figure('Trajectory test: Angular distribution through aperture 1')
    plt.clf()
    bins = plt.hist(theta, bins=50,)
    plt.xlabel('theta (rad)')
    plt.ylabel('counts')
    plt.title(
        'Possible angles theta through aperture 1\n'
        +f'Width: {aperture1_width}  mm, height: {aperture1_height} mm')

    plt.figure('Trajectory test: Angular distribution through aperture 2')
    plt.clf()
    bins = plt.hist(theta, bins=50)
    plt.xlabel('theta (rad)')
    plt.ylabel('counts')
    plt.title(
        'Possible angles theta through aperture 2\n'
        +f'Width: {aperture2_width}  mm, height: {aperture2_height} mm')

def test_random_traj(num_trajs):
    """Test function `random_traj.random_traj` with `num_traj` trajectories."""
    wholeDataDict= {}
    dfColumnsData = ["apWidth1","apWidth2",'vxA', 'vxr', 'vyA', 'vyr'] #,  'vzA', 'vzr' ]
    wholeData = pd.DataFrame(columns= dfColumnsData)

    nozzle_radius = 0.95
    aperture1_width = 0.1
    aperture1_height = 2
    aperture1_dist = 15
    aperture2_width= 2.0
    aperture2_height = 2
    aperture2_dist = 114

    T = 5.8

    plt.figure(
        'Trajectory test: Random trajectories',
        clear=True, constrained_layout=True)

    pos_vectors = np.zeros((num_trajs,3))
    vel_vectors = np.zeros((num_trajs,3))
    vel_magnitudes = np.zeros((num_trajs,1))
    trialss = np.zeros((num_trajs,1))
    for i in range(0, 1):
        for j in range(num_trajs):
            pos_vector, vel_vector, vel_magnitude, trials, theta_max = (
                random_traj.random_traj(
                    nozzle_radius, aperture1_width, aperture1_height,
                    aperture2_width, aperture2_height,
                    aperture1_dist, aperture2_dist, T))
            pos_vectors[j] = pos_vector
            vel_vectors[j] = vel_vector
            vel_magnitudes[j] = vel_magnitude
            trialss[j] = trials

      #--  print('Mean number of trials: %.f'%mean(trialss))

    #     plt.figure()
    #    plt.axes(aspect = 'equal')
    #    plt.scatter(pos_vectors[::,0], pos_vectors[::,1])
    #    plt.figure()
        # Histogram of velocity magnitudes
        #bins = plt.hist(vel_magnitudes, bins=50, color = "g", label ="velocity magnitude"+ str(apertureWidth)+ " aperture width")
        # Histogram of velocity vector magnitude
        #bins = plt.hist(np.linalg.norm(vel_vectors, axis=1), bins=50, color = "r", label = "velocity vector mangnitude"+ str(apertureWidth)+ " aperture width")

#        plt.figure()
        # Histogram of velocity vector y component
        bins = plt.hist(
            vel_vectors[::, 1], bins=100,
            color ="m",
            label ="v_y, aperture: "+ str(aperture1_width)+ " mm width, "+ str(aperture1_height)+ " mm height, \n       aperture 2: "+ str(aperture2_width)+ " mm width, "+ str(aperture2_height)+ " mm height")
        xFit = np.linspace(-30,30,100)

        pstart = [np.max(bins[0]), 5]
        bins1c = np.zeros(len(bins[0]))
        for iBins in range(len(bins[0])):
            bins1c[iBins] = ((bins[1][iBins+1]+bins[1][iBins])/2)

        GaussFit = lambda x, amp, gamma_g: pyhs.fitfunc.Gaussian(x, 0, amp, 0, gamma_g)

        popt, pcov = scipy.optimize.curve_fit(GaussFit, bins1c, bins[0], pstart, maxfev = 1000)
        #popt, pcov = scipy.optimize.curve_fit(GaussFit, bins[1][:-1], bins[0], pstart, maxfev = 1000)
    #    print(popt[0])
    #    print(popt[1])

        plt.plot(
            xFit,
            GaussFit(xFit, *popt),
            color='r',
            label=f'Gaussian Fit: FWHM: {popt[1]:.1f} m/s')
#        plt.xlabel('velocity [m/s]')
#        plt.ylabel('counts')
#        plt.title('velocity distribution behind aperture')
#        plt.legend(fontsize ="11")
#        plt.ylim([0,1000])
        wholeDataDict["apWidth1"]=aperture1_width
        wholeDataDict["apWidth2"]=aperture2_width
        wholeDataDict["vyA"]=popt[0]
        wholeDataDict["vyr"]=np.abs(2*popt[1])
        wholeDataDict["fwhmy"]=np.abs(2*np.sqrt(2*popt[1]*popt[1]*np.log(2)))

        #vx graph#Gaussian fit
#        plt.figure()
        bins2 = plt.hist(
            vel_vectors[::, 0], bins=100,
            color="g",
            label ="v_x, aperture 1: " + str(aperture1_width)+ " mm width, "+ str(aperture1_height)+ " mm height, \n       aperture 2: "+ str(aperture2_width)+ " mm width, "+ str(aperture2_height)+ " mm height")
        xFit2 = np.linspace(-30,30,100)
        scipy.optimize.curve_fit.maxfev = 800

        bins2c = np.zeros(len(bins2[0]))
        for iBins2 in range(len(bins2c)):
            bins2c[iBins2] = ((bins2[1][iBins2]+bins2[1][iBins2+1])/2)
        pstart2 = [np.max(bins2[0]), 5]
        popt, pcov = scipy.optimize.curve_fit(GaussFit, bins2c, bins2[0], pstart2, maxfev = 1000)
        #popt, pcov = scipy.optimize.curve_fit(GaussFit, bins2[1][:-1], bins2[0], pstart2, maxfev = 1000)
#
        plt.plot(
            xFit2, GaussFit(xFit2, *popt),
            color='r',
            label=f'Gaussian Fit: FWHM: {popt[1]:.1f} m/s')
#        plt.xlabel('velocity [m/s]')
#        plt.ylabel('counts')
#        plt.title('velocity distribution behind aperture')
        plt.legend(fontsize=8)
#        plt.ylim([0,1000])
        wholeDataDict["vxA"]=popt[0]
        wholeDataDict["vxr"]=np.abs(2*popt[1])
        wholeDataDict["fwhmx"]=np.abs(2*np.sqrt(2*popt[1]*popt[1]*np.log(2)))
        wholeData = wholeData.append(wholeDataDict, ignore_index=True)
        #wholeData.to_csv('MaxwellFit.csv')
#        wholeData.to_csv('MaxwellFit1.csv', mode='a', header=False)
        aperture1_width = aperture1_width + 0.1

    # wholeData.to_csv('MaxwellFit5.csv')

def test_random_gauss(num_trajs):
    """Test function `random_traj.random_gauss` with `num_traj` trajectories."""
    sigma = 72
    xMax = 200
    N = num_trajs
    out = np.empty(N)
    for i in range(N):
        out[i] = random_traj.random_gauss(xMax, sigma)
    plt.figure(
        'Trajectory test: Drawing random numbers from Gaussian distribution',
        clear=True, constrained_layout=True)
    plt.hist(out, bins=100, density=True, stacked=True)

    x = np.linspace(-xMax*1.2, xMax*1.2, 1000)
    plt.plot(
        x, 2/np.sqrt(np.pi)/np.sqrt(2)/2/sigma*np.exp(-x**2/2/sigma**2),
        color='red', linewidth=2)

## Call test routines
# Number of trajectories
num_trajs = 10000
test_xy_pos(num_trajs)
test_theta_cos_law(num_trajs)
test_vel_maxwell(num_trajs, 3)
test_vel_maxwell_exp_suppr(num_trajs, 3, 150)
test_vector_thru_aperture(num_trajs)
# test_random_traj(num_trajs)
test_random_gauss(num_trajs)
