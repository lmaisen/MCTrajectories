# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 17:42:37 2020

@author: Lothar Maisenbacher/MPQ

Generate 2S trajectory set from metadata file on MPCDF cluster or local machine.
"""

import pandas as pd
import time
import argparse
import glob
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.trajs

# Load OBEs
import odeint_1s2s
import traj_settings

#%% (Default) settings and parameters

# Default setting for overwrite metadata file if it already exist (bool).
# If set to False, the current date and time will be prepended to the filename.
default_overwrite_metadata = False

# Default setting for whether already calculated trajectories (as identified by their UID) should be
# recalculated.
default_recalculate_trajs = False

#default_metadata_file = None
default_metadata_file = 'test2 Params.dat'
default_traj_uids = None

#%% Get input parameters

parser = argparse.ArgumentParser(
    description='Generate 2S trajectory set(s) from metadata file.')
parser.add_argument(
    '--MetadataInputFile', help='Input filename of trajectory set metadata file to process',
    default=default_metadata_file, type=str)
parser.add_argument(
    '--MetadataFile', help='Output filename of trajectory set metadata file to generate',
    default=None, type=str)
parser.add_argument(
    '--TrajUIDs', default=default_traj_uids, type=str)
parser.add_argument(
    '--OverwriteMetadata', default=default_overwrite_metadata,
    action=argparse.BooleanOptionalAction)
parser.add_argument(
    '--RecalculateTrajs', default=default_recalculate_trajs,
    action=argparse.BooleanOptionalAction)
config = traj_settings.load_config_file(parser=parser)
command_line_args = vars(parser.parse_args())

dir_metadata_input = config['MonteCarloTrajectories']['DirMetadataInput']
dir_metadata = config['MonteCarloTrajectories']['DirMetadata']
dir_sets = config['MonteCarloTrajectories']['DirSets']
metadata_input_file = command_line_args['MetadataInputFile']
dir_sim_req = config['MeasurementMetadata']['DirMetadata']

#%% Load existing trajectory set metadata

# Search for metadata files in given metadata directory
metadata_filepaths = glob.glob(str(Path(dir_metadata, '* Params.dat')))
metadata_filenames = [Path(filepath).name for filepath in metadata_filepaths]
logger.info('Found %d metadata file(s)', len(metadata_filepaths))

CalculatedTrajs = pyha.trajs.Trajs()
CalculatedTrajs.load_trajs_metadata(metadata_filepaths)
dfTrajParams_calculated = CalculatedTrajs.dfs['dfTrajParams']

#%% Load trajectory set metadata

Trajs = pyha.trajs.Trajs()
if metadata_input_file is None:
    raise Exception('No metadata input file specified')
scan_path = Path(dir_metadata_input, metadata_input_file)
Trajs.load_trajs_metadata(scan_path)
dfTrajParams = Trajs.dfs['dfTrajParams']

#%%

mask_params = (
    pd.Series(True, index=dfTrajParams.index)
    )

if command_line_args['TrajUIDs'] is not None:

    traj_uids = command_line_args['TrajUIDs'].split(';')
    logger.info('Limiting calculation to trajectory set UID(s): %s', ', '.join(traj_uids))
    mask_params &= (
        (dfTrajParams.index.isin(traj_uids))
        )

# Set output dir
dfTrajParams['Folder'] = [str(Path(dir_sets, subdir)) for subdir in dfTrajParams['Subdir']]

mask_not_calculated = (
    mask_params
    & (~dfTrajParams.index.isin(dfTrajParams_calculated.index))
    )

logger.info(
    '%d of %d trajectory sets have already been calculated',
    mask_params.sum()-mask_not_calculated.sum(), mask_params.sum())

if not command_line_args['RecalculateTrajs']:
    mask_calculate = mask_not_calculated
    logger.info(
        'Already calculated sets will not be recalculated (flag \'--RecalculateTrajs\' not set), '
        +'generating %d trajectory set(s)',
        mask_calculate.sum())
else:
    mask_calculate = mask_params
    logger.info(
        'Already calculated sets will be recalculated (flag \'--RecalculateTrajs\' set), '
        + 'generating %d trajectory set(s)',
        mask_calculate.sum())

#%% Generate trajectory sets

metadata_output_path = None
mask_calculated = pd.Series(False, index=dfTrajParams.index)
for i_traj, (traj_uid, sr_param) in enumerate(dfTrajParams[mask_calculate].iterrows()):

    logger.info(
        'Generating trajectory set \'%s\' (%d of %d)'
        + ', containing %d trajectories'
        + ' (filename: \'%s\')',
        traj_uid, i_traj+1, mask_calculate.sum(), sr_param['NTrajs'], sr_param['Filename']
        )
    sr_param_, myOBE1s2s = odeint_1s2s.gen_traj_set(sr_param)

    # Update DataFrame
    dfTrajParams.loc[traj_uid] = sr_param_
    mask_calculated.loc[traj_uid] = True

    # Write trajectory set metadata to file
    if metadata_output_path is None:
        metadata_file = (
            command_line_args['MetadataFile']
            if command_line_args['MetadataFile'] is not None
            else metadata_input_file)
        metadata_output_path = Path(dir_metadata, metadata_file)
        if metadata_output_path.exists() and not command_line_args['OverwriteMetadata']:
            filename_prefix = time.strftime('%Y-%m-%d %H-%M-%S')
            metadata_output_path = Path(
                metadata_output_path.parent, filename_prefix+' '+metadata_output_path.name)
    Trajs.write_df_to_csv(
        dfTrajParams[mask_calculated], 'dfTrajParams',
        metadata_output_path
        )
    logger.info('Wrote metadata to file \'%s\'', metadata_output_path)
