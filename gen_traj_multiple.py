# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 17:42:37 2020

@author: Lothar Maisenbacher/MPQ

Generate 2S trajectory set.
"""

import numpy as np
import time
import datetime
import pandas as pd
import itertools
import os

# pyhs
import pyhs.gen

# Load OBEs
import odeint_1s2s, traj_settings

# Output filename
def get_traj_filename(param, ext='gz'):

    filename = (
        '{} {} 2S traj {:.0f}mW dnua={:.0f}Hz {:.1f}M Aper{}x{} v{:.1f}{} {:.1f}K {}.{}'
        .format(
            param['Timestamp'].strftime('%Y-%m-%d %H-%M-%S'),
            param['Isotope'],
            param['LaserPower']*1e3,
            param['AtomicDetuning'],
            param['NTrajs']*1e-6,
            param['Aperture2Width']*1e3,
            param['Aperture2Height']*1e3,
            param['SpeedDistExp'],
            ' vcut{:.0f}'.format(
                param['SpeedDistExpSupprCutOff'])
                if param['SpeedDistExpSupprCutOff'] != 0 else '',
            param['NozzleTemp'],
            param.name,
            ext
            ))
    return filename

# Filepaths
# traj_path = '../Trajectories'
traj_path = 'C:/Users/Vit/ownCloud/Python/packages/Trajectories'

output_dir_sets = os.path.join(traj_path, 'sets')
output_dir_metadata = os.path.join(traj_path, 'metadata')

#%% Set common parameters for single trajectory set
# and multiple trajectory sets

# Number of trajectories
num_trajs = int(1e6)
# Radius of the nozzle (m)
nozzleRadius = 1.00e-3
#nozzleRadius = 0.70e-3
# Nozzle temperature (K)
# nozzleT = 4.8
nozzleT = 7.2
# Geometry identifier
geometry = 'V9-3.2'
# Distance to detection region from nozzle (m)
detectionDist = 0.204 # 2S-6P setup V9-3.2 (05-2019)
# Frequency of the laser light chopper (Hz)
chopperFreq = 160
# Atomic detuning (Hz)
# atomic_detuning = 970
# atomic_detuning = 1500
atomic_detuning = 750
# atomic_detuning = 1000
# Waist radius of the 1S-2S laser (m)
w0 = 297e-6
# Power of the 1S-2S laser (intracavity, per direction) (W)
#power_laser = 0.85
#power_laser = 0.75
power_laser = 1.5
# 243 nm waist position relative to nozzle orifice (m)
z_waist = 0. # 2S-6P setup V9-3.2 (05-2019)
## Aperture 1 (first aperture after nozzle)
# Aperture 1 width (m)
aperture1Width = 2.4e-3
# Aperture 1 height (m)
aperture1Height = 2.4e-3
# Distance to aperture 1 from nozzle (m)
aperture1Dist = 69.0e-3 # 2S-6P setup V9-3.2 (05-2019)
## Aperture 2 (second aperture after nozzle)
# Aperture 2 width (m)
aperture2Width = 1.2e-3
# Aperture 2 height (m)
aperture2Height = 2e-3
# Distance to aperture 2 from nozzle (m)
#aperture2Dist = 0.1148
aperture2Dist = 0.1536 # 2S-6P setup V9-3.2 (05-2019)
# Total length of the detector (m)
# used to calculate 2S decay probability during flight through detector
detector_length = 2*26.2e-3 # 2S-6P setup V9-3.2 (05-2019)
# Speed distribution exponent
speedDistExp = 3
# Cutoff speed for exponential suppression of slow atoms
# (prop. to exp(-v_cut/v)) (m/s)
# Set to 0 to recover non-modified Maxwell-Boltzmann distribution
speedDistExpSupprCutOff = 120
## Atomic species and mass
# Hydrogen
# isotope = 'H'
# nucleusMass = 1.007276466621 # Proton mass (u) (from CODATA 2018)
# Hydrogen mass (u):
# electron mass in u * (1+1/(electron-to-proton mass ratio)+alpha^2/2))
# (from CODATA 2018)
# atomicMass = 1.007825
# Deuterium
isotope = 'D'
nucleusMass = 2.013553212745 # Deuteron mass in u (from CODATA 2018)
# Deuterium mass in u:
# electron mass in u * (1+1/(electron-to-deuteron mass ratio)+alpha^2/2))
# (from CODATA 2018)
atomicMass = 2.014102
# Delay set
delay_set_id = '2S6P'

# Load delays
delays = traj_settings.delay_sets[delay_set_id]
# Use only analysis delays
analysis_delays = {
    delay_id: delay for delay_id, delay in delays.items()
    if delay['Analysis']}

# Define dictionary with parameters
param = {
    'NTrajs': num_trajs,
    'NozzleRadius': nozzleRadius,
    'NozzleTemp': nozzleT,
    'Geometry': geometry,
    'DetectionDist': detectionDist,
    'ChopperFreq': chopperFreq,
    'AtomicDetuning': atomic_detuning,
    'LaserWaistRadius': w0,
    'LaserPower': power_laser,
    'LaserWaistPos': z_waist,
    'Aperture1Width': aperture1Width,
    'Aperture1Height': aperture1Height,
    'Aperture1Dist': aperture1Dist,
    'Aperture2Width': aperture2Width,
    'Aperture2Height': aperture2Height,
    'Aperture2Dist': aperture2Dist,
    'DetectorLength': detector_length,
    'SpeedDistExp': speedDistExp,
    'SpeedDistExpSupprCutOff': speedDistExpSupprCutOff,
    'Isotope': isotope,
    'NucleusMass': nucleusMass,
    'AtomicMass': atomicMass,
    'DelaySetID': delay_set_id,
    'NDelays': len(analysis_delays),
    'Timestamp': pd.Timestamp(datetime.datetime.utcnow()),
    'IntDelayWidth': 10e-6,
    'NMultipleChopperCycles': 0,
    'TrialProb': np.nan,
    'ThetaMax': np.nan,
    'FractionTrajsSpectroscopyRegion': np.nan,
    'NMinIntDelays': -1,
    'Folder': '',
    'Subdir': '',
}
param_common = pd.Series(param)

#%% Generate single trajectory set with common parameters

# sr_param = param_common.copy()

# # Generate random identifier for this trajectory set
# traj_uid = pyhs.gen.get_uid()
# sr_param.name = traj_uid

# # Set output dir and filename
# sr_param['Folder'] = os.path.join(
#     output_dir_sets, param['Subdir'])
# sr_param['Filename'] = get_traj_filename(sr_param)

# # Generate trajectory set with parameters given in dictionary params
# print('Generating file {}'.format(sr_param['Filename']))
# sr_param, myOBE1s2s = odeint_1s2s.gen_traj_set(sr_param)

# # Add to DataFrame
# dfTrajParams = pd.DataFrame()
# dfTrajParams.index.name = 'TrajUID'
# dfTrajParams = dfTrajParams.append(sr_param)
# dfTrajParams['NTrajs'] = dfTrajParams['NTrajs'].astype(int)
# dfTrajParams['NDelays'] = dfTrajParams['NDelays'].astype(int)
# dfTrajParams['NMultipleChopperCycles'] = (
#     dfTrajParams['NMultipleChopperCycles'].astype(int))
# dfTrajParams['NMinIntDelays'] = dfTrajParams['NMinIntDelays'].astype(int)

# # Write trajectory set parameters to file
# dfTrajParams.to_csv(
#     os.path.join(output_dir_metadata, sr_param['Filename'] + ' Params.dat'))


#%% Generate multiple trajectory sets, scanning various parameters

aperture_widths = np.array([1.2e-3])
#powers_laser = np.array([0.2,0.4,0.6,0.8,1.0,1.2])
#powers_laser = np.array([1.6,2.0,2.4])
#powers_laser = np.array([0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2])
#powers_laser = np.array([1.0])
powers_laser = np.array([1.5])
#powers_laser = np.array([2.0])
atomic_detunings = np.array([1200]) #750 #2250
# atomic_detunings = np.array([-400,0,200,400,600,700,800,900,1000,1200,1400,1600,2000,2400])
# atomic_detunings = np.array([-14000,-8000,-4000,-2000,-1200,-800,2800,3200,4000,6000,10000,16000])
#atomic_detunings = np.array([-4000,-2000,-400,0,400,800,1000,1200,1600,2000,2400,2800,4000,6000])
# aperture_widths = np.array([1.2e-3])
# powers_laser = np.array([0.65])
# atomic_detunings = np.array([1000, 1500])
#atomic_detunings = np.array([-500, -250, 0, 250, 500, 750, 1000, 1500, 2000, 2500])
#atomic_detunings = np.hstack((
#    np.array([-1000, -500, -250, 0, 250, 1500, 2000, 2500]),
#    np.linspace(500, 1300, 9)))

#[0.9 1.  1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8]
#[ 788.53664088  854.26079922  919.98495756  985.70911591 1051.43327425
# 1117.1574326  1182.88159094 1248.60574928 1314.32990763 1380.05406597]
# [790, 850, 920, 990, 1050, 1180, 1310],
#atomic_detunings = np.hstack((
#    [-8000, -3000, -2000, 3000, 4000, 5000, 10000],
#    [850],
#    [-5000, 6000, 7000],
#    [-1000, -500, -250, 0, 250, 1500, 2000, 2500],
#    np.linspace(500, 1300, 9)
#    ))
#atomic_detunings = np.unique(atomic_detunings)
#atomic_detunings.sort()
#atomic_detunings = np.array([-5000, 6000, 7000])

w0s = np.array([w0])
speed_dist_exps = np.array([3])
speedDistExpSupprCutOffs=np.array([50,60,70,80,90,100])
#speedDistExpSupprCutOffs=np.array([120])
#speedDistExpSupprCutOffs=np.array([60,120])
nozzle_temps = np.array([7.1])
#nozzle_temps = np.array([7.1])
num_reps = 1

set_list = list(itertools.product(
    aperture_widths,
    powers_laser,
    atomic_detunings,
    w0s,
    speed_dist_exps,
    speedDistExpSupprCutOffs,
    nozzle_temps,
    range(num_reps),
    ))

dfTrajParams = pd.DataFrame()
dfTrajParams.index.name = 'TrajUID'

sr_param = param_common.copy()

#sr_param['Subdir'] = '2020-06 V9-3.2 2S6P Thesis'
sr_param['Subdir'] = ''
sr_param['Folder'] = os.path.join(
    output_dir_sets, sr_param['Subdir'])
filenamePrefix = (
    '{} {}'.format(time.strftime("%Y-%m-%d %H-%M-%S"), isotope)
    + f' {powers_laser[0]:.1f}W'
    )

for iSet, (
        aperture_width, power_laser, atomic_detuning, w0,
        speed_dist_exp, speedDistExpSupprCutOff, nozzle_temp, num_rep
        ) in enumerate(set_list):

    sr_param['Aperture2Width'] = aperture_width
    sr_param['LaserPower'] = power_laser
    sr_param['AtomicDetuning'] = atomic_detuning
    sr_param['LaserWaistRadius'] = w0
    sr_param['SpeedDistExp'] = speed_dist_exp
    sr_param['SpeedDistExpSupprCutOff'] = speedDistExpSupprCutOff
    sr_param['NozzleTemp'] = nozzle_temp
    sr_param['Timestamp'] = pd.Timestamp(datetime.datetime.utcnow())
#    sr_param['filename'] = filenamePrefix + ' Set%i.gz'%iSet
    sr_param.name = pyhs.gen.get_uid()
    sr_param['Filename'] = get_traj_filename(sr_param, 'gz')

    print('Calculating trajectory set %i of %i:'%(iSet+1, len(set_list)))
    print(sr_param)

    sr_param, myOBE1s2s = odeint_1s2s.gen_traj_set(sr_param)

    # Add to DataFrame
    dfTrajParams = dfTrajParams.append(sr_param)
    dfTrajParams['NTrajs'] = dfTrajParams['NTrajs'].astype(int)
    dfTrajParams['NDelays'] = dfTrajParams['NDelays'].astype(int)
    dfTrajParams['NMultipleChopperCycles'] = dfTrajParams['NMultipleChopperCycles'].astype(int)
    dfTrajParams['NMinIntDelays'] = dfTrajParams['NMinIntDelays'].astype(int)

dfTrajParams.to_csv(
    os.path.join(output_dir_metadata, filenamePrefix + ' Params.dat'))
