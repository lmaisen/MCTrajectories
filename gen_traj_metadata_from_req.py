# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 17:42:37 2020

@author: Lothar Maisenbacher/MPQ

Generate metadata for so far not calculated 2S trajectory sets from simulation requirements
DataFrame.
The generated metadata file can then serve as input to `gen_traj_from_metadata.py`, which
calculates 2S trajectory sets as defined in a metadata file.
"""

import time
import datetime
import pandas as pd
import argparse
import glob
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.trajs
import pyha.util.data
import pyha.sim_reqs

# Load OBEs
import odeint_1s2s
import traj_settings

Trajs = pyha.trajs.Trajs()

#%% (Default) settings and parameters

## Simulation requirements
# Default simulation requirements set UID
default_sim_req_set_uid = 'test2'

#%% Get input parameters

parser = argparse.ArgumentParser(
    description='Generate metadata for 2S trajectory set(s) from simulation requirement set.')
parser.add_argument(
    '-u', '--SimReqSetUID', help='Simulation requirement set UID',
    default=default_sim_req_set_uid, type=str)
config = traj_settings.load_config_file(parser=parser)
command_line_args = vars(parser.parse_args())

dir_metadata_input = config['MonteCarloTrajectories']['DirMetadataInput']
dir_metadata = config['MonteCarloTrajectories']['DirMetadata']
sim_req_set_uid = command_line_args['SimReqSetUID']
dir_sim_reqs = config['MeasurementMetadata']['DirSimReqs']
df_sim_req_filepath = Path(dir_sim_reqs, f'{sim_req_set_uid}_req.xlsx')

#%% Load simulation requirements from file

SimReqs = pyha.sim_reqs.SimReqs()
SimReqs.load_sim_requirements(df_sim_req_filepath)
dfSimReqs = SimReqs.dfSimReqs
dfSimReqs_on_disk = dfSimReqs.copy()

#%% Load existing trajectory set metadata

# Search for metadata files in given metadata directory
metadata_filepaths = glob.glob(str(Path(dir_metadata, '* Params.dat')))
metadata_filenames = [Path(filepath).name for filepath in metadata_filepaths]
logger.info('Found %d metadata file(s)', len(metadata_filepaths))

Trajs.load_trajs_metadata(metadata_filepaths)
dfTrajParams = Trajs.dfTrajParams

#%% Assign UIDs

# Find simulation requirements where no trajectory set UID (TrajUID) has been assigned
mask = dfSimReqs['dfTrajParams_TrajUID'] == ''
logger.info(
    'Found %d simulation requirement(s) for which no trajectory set UID has been assigned',
    mask.sum())
# Assign new TrajUIDs
new_uids = [pyhs.gen.get_uid() for i in range(mask.sum())]
dfSimReqs.loc[mask, 'dfTrajParams_TrajUID'] = new_uids
logger.info('Assigned %d trajectory set UID(s)', mask.sum())
dfSimReqs = SimReqs.cast_containers_to_data_format(dfSimReqs, 'dfSimReqs')
SimReqs.dfSimReqs = dfSimReqs

# Write updated simulation requirements DataFrame to file if it has changed
if not dfSimReqs.equals(dfSimReqs_on_disk):
    SimReqs.dfSimReqs.to_excel(Path(
        df_sim_req_filepath.parent,
        df_sim_req_filepath.stem+df_sim_req_filepath.suffix))
    logger.info(
        'Updated simulation requirements file \'%s\'', df_sim_req_filepath)
    dfSimReqs_on_disk = SimReqs.dfSimReqs

#%% Check which trajectory sets need to be calculated

# Keep only unique trajectory set UIDs
dfSimReqs_unique = dfSimReqs.drop_duplicates(subset='dfTrajParams_TrajUID')
logger.info('Keeping %d unique trajectory set UID(s)', len(dfSimReqs_unique))

# Check which required trajectory sets have already been calculated
mask_traj_exists = dfSimReqs_unique['dfTrajParams_TrajUID'].isin(dfTrajParams.index)
logger.info(
    '%d of %d trajectory set(s) have already been calculated (existing UID(s): %s)',
    mask_traj_exists.sum(), len(dfSimReqs_unique),
    ', '.join([f'{elem}' for elem
               in dfSimReqs_unique[mask_traj_exists]['dfTrajParams_TrajUID']]))

#%% Create trajectory set metadata from simulation requirements

# Build trajectory set parameters DataFrame for missing required trajectory sets
traj_df_id = 'dfTrajParams'
traj_columns = Trajs.get_columns(traj_df_id)
traj_columns_avail = [
    column for column in traj_columns
    if f'{traj_df_id}_{column}' in dfSimReqs_unique.columns]

# dfTrajParams_req = pd.DataFrame()
dfTrajParams_req = Trajs.df_templates['dfTrajParams']
for i, (common_json_file, df) in enumerate(
        dfSimReqs_unique[~mask_traj_exists].groupby('TrajParams_CommonJSONFile')):

    filepath = Path(dir_metadata_input, common_json_file)
    logger.info(
        'Loading common trajectory set parameters from file \'%s\' for UID(s) %s',
        common_json_file, ', '.join([f'{elem}' for elem in df.index]))

    # Copy relevant parameters from requirements
    dfTrajParams_req_ = Trajs.cast_containers_to_data_format(
        (df
         .rename(columns={f'{traj_df_id}_{column}': column for column in traj_columns_avail})
         [traj_columns_avail]
         .set_index('TrajUID', drop=True)
         ),
        traj_df_id)

    # Add default values for parameters not listed in requirements
    sr_param_common = Trajs.read_series_from_json(filepath, 'dfTrajParams')
    # Update timestamp
    sr_param_common['Timestamp'] = pd.Timestamp(datetime.datetime.utcnow())
    sr_param_missing_ = (
        sr_param_common[[
            column for column in traj_columns if column not in traj_columns_avail]])
    dfTrajParams_req_ = dfTrajParams_req_[traj_columns_avail].merge(
        pd.DataFrame(
            data=[sr_param_missing_.values]*len(dfTrajParams_req_[traj_columns_avail]),
            columns=sr_param_missing_.index, index=dfTrajParams_req_[traj_columns_avail].index),
        left_index=True, right_index=True)
    dfTrajParams_req = pd.concat([dfTrajParams_req, dfTrajParams_req_])

if len(dfTrajParams_req) > 0:
    # Set filenames
    dfTrajParams_req['Filename'] = dfTrajParams_req.apply(
        lambda sr: odeint_1s2s.get_traj_filename(sr)[0], axis=1)

# Write trajectory set parameters to file
filename_prefix = time.strftime('%Y-%m-%d %H-%M-%S')
filename_prefix = ''
filepath = Path(
    dir_metadata_input,
    f'{filename_prefix} ' if filename_prefix != '' else ''
    +f'{sim_req_set_uid} Params.dat')
Trajs.write_df_to_csv(
    dfTrajParams_req, 'dfTrajParams', filepath)
logger.info(
    'Wrote metadata for %d trajectory set(s) to file \'%s\'',
    len(dfTrajParams_req), filepath)
