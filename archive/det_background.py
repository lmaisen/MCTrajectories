# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Estimate fraction of 2S atoms hitting detector and causing background counts
(trajectory set must be loaded first).
"""

#delaysToFit = np.arange(1, 11, 1)
#
#eta = np.array([ 0.25317376,  0.25358951,  0.25697277,  0.26809521,  0.28885902,
#        0.31631848,  0.36023741,  0.4421262 ,  0.54559434,  0.67250531])
#
##delaysToFit = [1]
#
##ax1.cla()
#N2Scol = np.zeros(len(delaysToFit))
#frac2Scol = np.zeros(len(delaysToFit))
#for iDelay, delayID in enumerate(delaysToFit):
#
#    # Transverse position r at longitudina position z
#    z = 164.5e-3
#    #tau =
#    x = loadedData[:,0]+loadedData[:,3]*z/loadedData[:,5]
#    y = loadedData[:,1]+loadedData[:,4]*z/loadedData[:,5]
#    r = np.sqrt(x**2+y**2)
#    histdata = r
#
#    excProbListHistDelay = excProbListHist[:,delayID]
#    histweights = excProbListHistDelay
#    #histweights = np.ones(len(excProbListHistDelay))
#    histDefaultLimits = [0, 3e-3]
#
#    weighted_hist, bin_edges = np.histogram(histdata, bins=histNumbins, range=histDefaultLimits, weights=histweights)
#    x_bins = bin_edges[:-1] + np.diff(bin_edges)/2
##    ax1.plot(x_bins, weighted_hist/np.max(weighted_hist), label='Delay {:d}'.format(delayID))
#
#    rCone = 1.92e-3
#    dx = 0e-3
#    dy = 0e-3
#    mask = ((x-dx)**2+(y-dy)**2) >= rCone**2
#    print('Delay {:d}'.format(delayID))
#    N2Scol[iDelay] = np.sum(excProbListHistDelay[mask])
##    N2Scol[iDelay] = np.sum(excProbListHistDelay)
#    frac2Scol[iDelay] = np.sum(excProbListHistDelay[mask])/np.sum(excProbListHistDelay)
#    print('Fraction of 2S atoms hitting wall: {:.2e}'.format(frac2Scol[iDelay]))
#
#    BoverS = 1/eta[delayID-1]*frac2Scol[iDelay]*0.1
#    print(BoverS)
#    print('Observed background: {:.2e}'.format(BoverS))
#
#ax1.cla()
##ax1.plot(delaysToFit, N2Scol/N2Scol[0]) # Number of atoms hitting the wall, relative to delay 1
##ax1.plot(delaysToFit, frac2Scol/frac2Scol[0])
##ax1.plot(delaysToFit, frac2Scol)
#BoverS = 1/eta*frac2Scol*0.1 # Background relative to signal
#ax1.plot(delaysToFit, BoverS)
##ax1.set_ylim([0, ax1.get_ylim()[1]])