# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 20:53:01 2017

@author: Lothar Maisenbacher/Lothar

Generate trajectory set for light force shift paper.
"""

import numpy as np
import matplotlib.pyplot as plt
import random
import scipy.stats
import time

import random_traj

#%% Some plot prep

plt.figure(figsize =(12, 8))
plt.rc('font', **{'size': 16, 'family': 'serif', 'serif': ['Computer Modern']})
plt.rc('text', usetex=True)

#%%
for iFile in np.arange(1,11):
#for iFile in np.arange(1,2):
    random.seed()

    #%%

    # Wire tomography of deuterium beam gives 28 ° (1/e radius) for 1 mm radius nozzle
    a_em1r = 28
    v_em1r = np.tan(a_em1r*np.pi/180)
    em1rToFWHM = 2*np.sqrt(np.log(2))
    em1rToSigma = 1/np.sqrt(2)

    #%%
    # Parameters
    nozzleRadius = 0.8e-3 # Average weighted nozzle radius for 1D nozzle, with 2D nozzle radius 0.95 mm
    # Chosen such that it matches approx. delays from "2017-02-16 19-57-22 2S trajectories 1000mW dnu=1000Hz 1M Aper1.85x1.50b v4.0.dat",
    # Delay 1: vz_mean = 360 m/s, vx_FWHM = 5.88
    vz = 360
    aperture1Width = 1.9e-3
    # Delay 10: vz_mean = 104 m/s, vx_FWHM = 1.86
#    vz = 105
#    aperture1Width = 2.2e-3
    aperture1Dist = 115e-3
    vxMax = (aperture1Width/2+nozzleRadius)*vz/aperture1Dist
    vxem1r = 100 # 1/e radius of emission pattern (in m/s), approx. matching 28 ° 1/e radius of wire tomography (rounded from 106 m/s to 100 m/s)

    #%% Generate trajectories
    nTraj = 1000000
    xList = np.zeros((nTraj,1))
    vxList = np.zeros((nTraj,1))
    posAperList = np.zeros((nTraj,1))
    trials = np.zeros((nTraj,1))
    for i in range(nTraj):
        condition1 = True
        iC1 = 0
        while condition1:
            x = random.uniform(-nozzleRadius, nozzleRadius)
        #        print(x)
            # Transverse velocity
            vx = random_traj.random_gauss(vxMax, vxem1r*em1rToSigma)
        #        print(vx)
            posAperture1 = vx * aperture1Dist/vz + x
        #        print(posAperture1)
            condition1 = posAperture1**2 >= (aperture1Width/2)**2
            iC1 = iC1 + 1

        trials[i] = iC1
        #    print(trials)
        #    print(x)
        #    print(vx)
        #    print(posAperture1)
        xList[i] = x
        vxList[i] = vx
        posAperList[i] = posAperture1

    print('Mean number of trials: {}'.format(np.mean(trials)))

    #%% Write output
    filename = '{} Trajectories Wigner function delay 1 {}.dat'.format(time.strftime("%Y-%m-%d"), iFile)
    header = []
    header.append('Monte Carlo simulation of trajectories of atoms from nozzle, matching Wigner function for light force shift comparison with Yue (by Lothar, 19.05.2017)')
    header.append('File created on %s'%time.strftime("%Y-%m-%d %H:%M"))
    header.append('Number of trajectories: %i'%nTraj)
    header.append('Beam model: Gauss with 1/e radius {:.1f} m/s / FWHM {:.1f} m/s, corresponding to 1/e radius {:.1f} deg for vz = {:.1f} m/s'.format(vxem1r, em1rToFWHM*vxem1r, np.arctan(vxem1r/vz)*180/np.pi, vz))
    header.append('Nozzle orifice radius (step function): %f m'%nozzleRadius)
    header.append('Hard aperture (step function): Position relative to nozzle orifice: %f m, width: %f m'%(aperture1Dist,aperture1Width))
    columnNames = ['x (m)', 'z (m)', 'vx (m/s)', 'vz (m/s)']
    header.append(' '.join(columnNames))
    header = '\n'.join(header)
    np.savetxt(filename,
           np.hstack((xList, np.zeros((len(xList),1)), vxList, np.ones((len(xList),1))*vz)),
           header=header,
           fmt='%.8e')

    #%% Fit and plot results
    ax1 = plt.gca()
    ax1.cla()
    histDefaultLimits = [-7, 7]
    binnedVx, low_range, binsize, extrapoints = scipy.stats.histogram(vxList, numbins=100, defaultlimits=histDefaultLimits)
    xBins = np.array([histDefaultLimits[0]+i*binsize+(binsize/2) for i in range(len(binnedVx))])
    #ax1.plot(xBins, binnedVx)
    ax1.bar(xBins-(binsize/2), binnedVx, binsize, label='N = {:.1e}'.format(nTraj))

    GaussFWHM = lambda x, x0, FWHM, A: A * np.exp(-4*np.log(2)*(x-x0)**2/FWHM**2)
    popt, pcov = scipy.optimize.curve_fit(GaussFWHM, xBins, binnedVx, [np.mean(binnedVx*xBins)/np.mean(binnedVx), np.std(binnedVx*xBins)/(2*np.mean(binnedVx)), np.max(binnedVx)])
    xPlot = np.linspace(histDefaultLimits[0],histDefaultLimits[1],1000)
    ax1.plot(xPlot, GaussFWHM(xPlot, *popt), color='r', linewidth=2, label='FWHM: {:.2f} m/s'.format(popt[1]))
    print('FWHM: {:.2f} m/s'.format(popt[1]))

    ax1.set_xlim(histDefaultLimits)
    ax1.set_title(filename)
    ax1.set_xlabel('Transverse velocity (m/s)')
    ax1.set_ylabel('Number of trajectories')
    ax1.legend(loc='upper left', fancybox=True)
    plt.tight_layout()
    plt.savefig(filename + '.pdf')
