# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 17:44:39 2015

@author: Lothar
"""

import numpy as np
import scipy.stats
import fitfunc
import matplotlib.pylab as plt


# Load old results
#delphi = np.loadtxt('2s trajectories Lothar Cos model 1000mW dnu=0kHz 100k.dat', skiprows=16)
#delphiVelList = delphi[:,2:5]
#delphiExcProbList = delphi[:,-8:-1]
#delphiExcProbList = np.append(delphiExcProbList, np.zeros((len(delphiExcProbList),1)),1)
#delphiExcProbList = np.fliplr(delphiExcProbList)

# Load results
loadedData = np.loadtxt('2015-08-16 16-12 2S trajectories.dat')
velListHist = loadedData[:, 3:6]
excProbListHist = loadedData[:, 6:]

#velListHist = velList
#velListHist = velList[velList[:, 0] > 0, :]
#excProbListHist = excProbList
#excProbListHist = excProbList[velList[:, 0] >0,:]
#velListHist = delphiVelList
#excProbListHist = delphiExcProbList

alphaList = [i*0.05 for i in range(-8, -3+1)] + [i*0.02 for i in range(-7, 7+1)] + [i*0.05 for i in range(3, 8+1)]
dphiList = [i*5e-6 for i in range(-4, 4+1)]
angleList = np.zeros((len(alphaList)*len(dphiList), 2))
for i in range(len(alphaList)):
        angleList[i*+len(dphiList):(i+1)*len(dphiList),0] = alphaList[i]
        angleList[i*+len(dphiList):(i+1)*len(dphiList),1] = dphiList
slopeList = np.zeros(len(angleList))
offsetList = np.zeros(len(angleList))
averageAllList = np.zeros(len(angleList))
averageSlowList = np.zeros(len(angleList))
averageVerySlowList = np.zeros(len(angleList))
for iVar in range(len(angleList)):

    alpha = angleList[iVar,0]
    dphi = angleList[iVar,1]
    print(alpha)
    print(dphi)
    delaysToFit = np.arange(1, 11, 1)
    vzmean = np.zeros(len(delaysToFit))
    vzFWHM = np.zeros(len(delaysToFit))
    vxmean = np.zeros(len(delaysToFit))
    vxFWHM = np.zeros(len(delaysToFit))
    for iDelay in range(len(delaysToFit)):
    #    print('Delay %i'%delaysToFit[iDelay])
        # Histogram
        excProbListHistDelay = excProbListHist[:,delaysToFit[iDelay]]
        histVelComponent = 2
        histDefaultLimits = [0, 1200]
    #    histVelComponent = 0
    #    histDefaultLimits = [-10, 10]
        histNumbins = 100
        # Unweighted
    #    histogram, low_range, binsize, extrapoints = scipy.stats.histogram(velListHist[::,histVelComponent], numbins=histNumbins, defaultlimits=histDefaultLimits)
    #    xBins = np.array([histDefaultLimits[0]+i*binsize+(binsize/2) for i in range(len(histogram))])
        #plt.plot(xBins, histogram)
    #    popt, pcov = scipy.optimize.curve_fit(fitfunc.GaussFit, xBins, histogram, [np.mean(histogram*xBins)/np.mean(histogram), np.std(histogram*xBins)/(2*np.mean(histogram)), np.max(histogram), 0])
        #plt.plot(xBins, fitfunc.GaussFit(xBins, popt[0], popt[1], popt[2], popt[3]))
        #print('Points not in histogram: %i'%extrapoints)
        #print('Unweighted mean velocity: %f m/s'%(np.mean(histogram*xBins)/np.mean(histogram)))
        #print('Unweighted velocity FWHM: %f m/s'%(np.sqrt(2*np.log(2))*popt[1]))
        # Weighted with excitation probabilities
        # Forward velocity    
        histDefaultLimits = [0, 1200]    
        histdata = velListHist[:,2]
        weightedhistogram, low_range, binsize, extrapoints = scipy.stats.histogram(histdata, numbins=histNumbins, defaultlimits=histDefaultLimits,
                                                                                   weights=excProbListHistDelay/velListHist[:, 2])
        xBins = np.array([histDefaultLimits[0]+i*binsize+(binsize/2) for i in range(len(weightedhistogram))])                                                                               
    #    plt.figure()
    #    plt.xlabel('Forward velocity (m/s)')
    #    plt.ylabel('Number of 2S trajectories')    
    #    plt.plot(xBins, weightedhistogram)
        popt, pcov = scipy.optimize.curve_fit(fitfunc.GaussFit, xBins, weightedhistogram, [np.mean(weightedhistogram*xBins)/np.mean(weightedhistogram), np.std(weightedhistogram*xBins)/(2*np.mean(weightedhistogram)), np.max(weightedhistogram), 0])
    #    plt.plot(xBins, fitfunc.GaussFit(xBins, popt[0], popt[1], popt[2], popt[3]))
        vzmean[iDelay] = (np.mean(weightedhistogram*xBins)/np.mean(weightedhistogram))
        vzFWHM[iDelay] = (np.sqrt(2*np.log(2))*popt[1])
    #    print('Weighted mean z velocity: %f m/s'%vzmean[iDelay])
    #    print('Weighted z velocity FWHM: %f m/s'%vzFWHM[iDelay])
        # Transverse velocity
        histDefaultLimits = [-10, 10]    
    #    plt.figure()
    #    plt.xlabel('Transverse velocity (m/s)')
    #    plt.ylabel('Number of 2S trajectories')    
#        alpha = 0.04 # Angle between atomic beam and laser beam
#        dphi = -5e-6 # Misalignment between anti parallel beams
        dtheta = 0e-6 # Misalignment between anti parallel beams    
        phi = alpha/180*np.pi
        antiphi = phi + dphi
#        antiphi = phi + dphi
        histdata = velListHist[:,0]*np.cos(phi)+velListHist[:,2]*np.sin(phi)
#        antihistdata = -velListHist[:,0]*np.cos(antiphi)*np.cos(dtheta)+velListHist[:,1]*np.cos(antiphi)*np.sin(dtheta)+velListHist[:,2]*np.sin(antiphi)
        antihistdata = -velListHist[:,0]*np.cos(antiphi)-velListHist[:,2]*np.sin(antiphi)        
        histdata = np.hstack((histdata, antihistdata))
        histweights = excProbListHistDelay/velListHist[:, 2]
        histweights = np.hstack((histweights, histweights))
        weightedhistogram, low_range, binsize, extrapoints = scipy.stats.histogram(histdata, numbins=histNumbins, defaultlimits=histDefaultLimits,
                                                                                   weights=histweights)
        xBins = np.array([histDefaultLimits[0]+i*binsize+(binsize/2) for i in range(len(weightedhistogram))])    
    #    plt.plot(xBins, weightedhistogram)
        popt, pcov = scipy.optimize.curve_fit(fitfunc.GaussFit, xBins, weightedhistogram, [np.mean(weightedhistogram*xBins)/np.mean(weightedhistogram), np.std(weightedhistogram*xBins)/(2*np.mean(weightedhistogram)), np.max(weightedhistogram), 0])
    #    plt.plot(xBins, fitfunc.GaussFit(xBins, popt[0], popt[1], popt[2], popt[3]))
        vxmean[iDelay] = (np.mean(weightedhistogram*xBins)/np.mean(weightedhistogram))
        vxFWHM[iDelay] = (np.sqrt(2*np.log(2))*popt[1])
    #    print('Weighted mean transverse velocity: %f m/s'%vxmean[iDelay])
    #    print('Weighted transverse velocity FWHM: %f m/s'%vxFWHM[iDelay])
        # Excitation probability
        #plt.figure()
        #plt.plot(xBins, weightedhistogram/histogram)
        #scipy.signal.savgol_filter(weightedhistogram/histogram, 11, 1)
        
#    plt.figure()
    plt.xlabel('Delay mean forward velocity (m/s)')
    plt.ylabel('Expected Doppler shift (Hz)')
    plt.title(r'Expected Doppler shift for $\alpha$ = %2.2f ° and $\delta\phi$ = %1.2e µrad'%(phi*180/np.pi, dphi*1e6))
    vxdoppler = 1/486e-9 * vxmean
    plt.plot(vzmean, vxdoppler)
    popt, pcov = scipy.optimize.curve_fit(fitfunc.linearFit, vzmean, vxdoppler)
    slopeList[iVar] = popt[0]
    offsetList[iVar] = popt[1]    
    vzaxis = np.arange(0, 350, 0.1)
    plt.plot(vzaxis, fitfunc.linearFit(vzaxis, popt[0], popt[1]))
    print('Slope of linear fit: %f Hz/(m/s)'%popt[0])
    print('Offset of linear fit: %f Hz'%popt[1])
    averageAllList[iVar] = np.mean(vxdoppler)
    print('Average Doppler shift delays %i to %i: %f Hz'%(delaysToFit[0], delaysToFit[-1], np.mean(vxdoppler)))
    averageLimit = np.arange(8, 11)
    averageSlowList[iVar] = np.mean(vxdoppler[np.in1d(delaysToFit, averageLimit)])
    print('Average Doppler shift delays %i to %i: %f Hz'%(averageLimit[0], averageLimit[-1], np.mean(vxdoppler[np.in1d(delaysToFit, averageLimit)])))
    averageLimit = np.arange(10, 11)
    averageVerySlowList[iVar] = np.mean(vxdoppler[np.in1d(delaysToFit, averageLimit)])    
    print(np.arcsin(popt[0]*486e-9)*180/np.pi)

#%%

# Average of all results for slope within certain limits
slopeLimit = [-8, 18]
sel = (slopeList < slopeLimit[1]) & (slopeList > slopeLimit[0])
print(np.sum(sel))
print(np.mean(slopeList[sel]))
print(np.std(slopeList[sel]))
print(np.mean(offsetList[sel]))
print(np.mean(averageAllList[sel]))
print(np.std(averageAllList[sel]))
print(np.mean(averageSlowList[sel]))
print(np.mean(averageVerySlowList[sel]))

# Within slope limits, but locking to one side of the backcoupling curve
sel = (angleList[:,1]>-15e-6)
print(np.sum(sel))
print(np.mean(slopeList[sel]))
print(np.std(slopeList[sel]))
print(np.mean(offsetList[sel]))
print(np.mean(averageAllList[sel]))
print(np.std(averageAllList[sel]))
print(np.mean(averageSlowList[sel]))
print(np.mean(averageVerySlowList[sel]))

#%% Plots
offsetFig = plt.figure()
slopeFig = plt.figure()

#%% Slope vs alpha
slopeFig.clf()
plt.figure(slopeFig.number)
ax = plt.subplot(111)
pltHandles = [0] * len(dphiList)
for i in range(len(dphiList)):
    pltHandles[i], = ax.plot(angleList[angleList[:,1]==dphiList[i], 0], slopeList[angleList[:,1]==dphiList[i]])
plt.xlabel(r'Angle $\alpha$ between atomic beam and laser beams (°)')
plt.ylabel('Slope of frequency vs. mean delay velocity fit (Hz/(m/s))')
plt.xlim([-0.4, 0.4])
#['%1.0f]
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.legend(pltHandles, ['{:.0f}'.format(x*1e6) for x in dphiList], loc=2,  bbox_to_anchor=(1, 1))
plt.show()

#%% Offset vs alpha
offsetFig.clf()
plt.figure(offsetFig.number)
ax = plt.subplot(111)
for i in range(len(dphiList)):
    ax.plot(angleList[angleList[:,1]==dphiList[i], 0], offsetList[angleList[:,1]==dphiList[i]])
plt.xlabel(r'Angle $\alpha$ between atomic beam and laser beams (°)')
plt.ylabel('Offset of frequency vs. mean delay velocity fit (Hz)')
plt.xlim([-0.1, 0.1])
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.legend(pltHandles, ['{:.0f}'.format(x*1e6) for x in dphiList], loc=2,  bbox_to_anchor=(1, 1))
plt.show()

#%% Frequency shift vs alpha
plt.clf()
ax = plt.subplot(111)
for i in range(len(dphiList)):
    ax.plot(angleList[angleList[:,1]==dphiList[i], 0], averageVerySlowList[angleList[:,1]==dphiList[i]])
plt.xlabel(r'Angle $\alpha$ between atomic beam and laser beams (°)')
plt.ylabel('Frequency shift for delay 10 (Hz)')
plt.title('Crude model for AFR residuals: Frequency shift for delay 10')
plt.xlim([-0.4, 0.4])
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])
plt.legend(pltHandles, ['{:.0f}'.format(x*1e6) for x in dphiList],  title=r'AFR misalignment $\delta\phi$', loc=2,  bbox_to_anchor=(1, 1))
plt.show()

#%% Frequency shift vs alpha
#offsetFig = plt.figure()
plt.clf()
ax = plt.subplot(111)
for i in range(len(dphiList)):
    ax.plot(angleList[angleList[:,1]==dphiList[i], 0], averageAllList[angleList[:,1]==dphiList[i]])
plt.xlabel(r'Angle $\alpha$ between atomic beam and laser beams (°)')
plt.ylabel('Frequency shift for all delays (Hz)')
plt.title('Crude model for AFR residuals: Frequency shift for all delays')
plt.xlim([-0.4, 0.4])
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])
plt.legend(pltHandles, ['{:.0f}'.format(x*1e6) for x in dphiList], title=r'AFR misalignment $\delta\phi$', loc=2,  bbox_to_anchor=(1, 1))
plt.show()