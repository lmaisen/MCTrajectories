# -*- coding: utf-8 -*-
"""
Created on Thu May  2 17:46:14 2019

@author: Lothar Maisenbacher/MPQ

Convert legacy parameters files for 2S trajectory sets.
"""

import pandas as pd
import datetime

#%%

filename = 'results\\trajs\\2019-04-30 12-43-27 H 2S traj 1000mW dnua=1000Hz 1.0M Aper1.2x2.0 v3.0 4.8K rhBMCEBf Params.dat'
#dfParams = pd.read_csv(filename, index_col=0)
dfParams = pd.read_csv(filename, index_col='TrajUID')
dfParams['Timestamp'] = [datetime.datetime.strptime(row['Filename'][:19], '%Y-%m-%d %H-%M-%S') for _, row in dfParams.iterrows()]
dfParams['Geometry'] = 'V9-3.1'
#dfParams.index = dfParams['UID']
#dfParams.index.name = 'TrajUID'
#del dfParams['UID']
#dfParams['NDelays'] = 10
#dfParams['DelaySetID'] = '2S4P'
#dfParams['AtomicDetuning'] = dfParams['Detuning'] * 2
#del dfParams['Detuning']
#dfParams.to_csv(filename)