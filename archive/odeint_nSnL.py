# -*- coding: utf-8 -*-
"""
Created on Sun May 20 17:46:46 2018

@author: Lothar Maisenbacher/MPQ

Estimate of 2S-nS/D or 1S-nS/D excitation fraction, using two-level OBEs,
for a single trajectory,
including ionization, ac-Stark and second-order Doppler,
for either transverse or longitudinal excitation
"""

import scipy.integrate
import scipy.constants
import numpy as np
import matplotlib.pylab as plt
import matplotlib as mpl

import cOBE_1S2S as OBE_1S2S
import fitfunc

#plt.figure(figsize =(8, 8))

pltparams = {'text.usetex' : False,
          'font.size' : 16,
          'font.family' : 'Times New Roman',
          'mathtext.fontset' : 'stix',
          }
plt.rcParams.update(pltparams)
plotFolder = 'plots\\nS-nL\\'

nucleusMass = 1.007276466879 # Proton mass in u (from CODATA 2014)
myOBE1snS = OBE_1S2S.OBE_1S2S(nucleusMass)

w0 = 0.5e-3
power_laser = 0.1
velocity = 200

# Transverse excitation, atom flies along x direction crossing laser beam
exc_label = 'transverse'
myOBE1snS.startVector = np.array([-3*w0, 0, 0], dtype=float)
myOBE1snS.velVector = np.array([velocity, 0, 0], dtype=float)
t_max = 6*w0/velocity
# Longitudinal excitation, atom flies along z direction and along laser beam
#exc_label = 'longitudinal'
#t_max = 165e-3/velocity
#myOBE1snS.startVector = np.array([0, 0, 0], dtype=float)
#myOBE1snS.velVector = np.array([0, 0, velocity], dtype=float)
# Static
#exc_label = 'static'
#t_max = 1e-3
#myOBE1snS.startVector = np.array([0, 0, 0], dtype=float)
#myOBE1snS.velVector = np.array([0, 0, 0], dtype=float)

# Set laser beam parameters
myOBE1snS.w0 = w0
myOBE1snS.power_laser = power_laser

# 2S-6S
#trans_label = '2S-6S'
#myOBE1snS.beta_ge = -6.89568e-5 * myOBE1snS.m_r_corr
#myOBE1snS.beta_ioni = 2.28478e-4 * myOBE1snS.m_r_corr
#myOBE1snS.gamma_s = 1754385
#myOBE1snS.beta_AC_g = -1.36379e-3 * myOBE1snS.m_r_corr
#myOBE1snS.beta_AC_e = 1.52064e-3 * myOBE1snS.m_r_corr
#myOBE1snS.lambda_laser = 820e-9
#myOBE1snS.nu_eg = scipy.constants.c/myOBE1snS.lambda_laser

# 1S-3S
trans_label = '1S-3S'
myOBE1snS.beta_ge = 1.00333e-5 * myOBE1snS.m_r_corr
myOBE1snS.beta_ioni = 2.02241e-5 * myOBE1snS.m_r_corr
myOBE1snS.gamma_s = 6.32e6
myOBE1snS.beta_AC_g = -3.02104e-5 * myOBE1snS.m_r_corr
myOBE1snS.beta_AC_e = 9.80847e-5 * myOBE1snS.m_r_corr
myOBE1snS.lambda_laser = 205e-9
myOBE1snS.nu_eg = scipy.constants.c/myOBE1snS.lambda_laser

# Switches to turn off certain physics
myOBE1snS.ionizationOn = 1
myOBE1snS.dopplerOn = 1
myOBE1snS.ACStarkOn = 1

# Times to solve OBEs at
tToIntegrateUnique = np.linspace(0,t_max,200)

## Solve as function of detuning
detunings = np.linspace(-1e6,1e6,400)
detunings_atomic = 2 * detunings
#print(0.44/t_max)
results = np.zeros((len(detunings),4))
for i, detuning in enumerate(detunings):
    myOBE1snS.detuning = detuning
#    odeintresultUnique = scipy.integrate.odeint(myOBE1snS.derivs2SnP, [1,0,0,0], tToIntegrateUnique)
    odeintresultUnique = scipy.integrate.odeint(myOBE1snS.derivs, [1,0,0,0], tToIntegrateUnique)
    results[i] = odeintresultUnique[-1]
    results[i,3] = np.sum(odeintresultUnique[:,3]) * myOBE1snS.gamma_s/2/np.pi
# Depletion of 2S state
#detunings_y = 1-results[:,0]
# Signal (must be summed up over time above)
detunings_y = results[:,3]
detuning_peak = detunings[np.argmax(detunings_y)]

## Solve as function of time
myOBE1snS.detuning = detuning_peak
#odeintresultUnique = scipy.integrate.odeint(myOBE1snS.derivs2SnP, [1,0,0,0], tToIntegrateUnique)
odeintresultUnique = scipy.integrate.odeint(myOBE1snS.derivs, [1,0,0,0], tToIntegrateUnique)

## Fit results
pstart = [2 * detuning_peak, np.max(detunings_y), 0, myOBE1snS.gamma_s/2/np.pi, myOBE1snS.gamma_s/2/np.pi]
popt, pcov = scipy.optimize.curve_fit(fitfunc.Voigt, detunings_atomic, detunings_y, pstart)

## Plot results
plt.clf()
gs = mpl.gridspec.GridSpec(2, 1,
                       width_ratios=[1],
                       height_ratios=[1,1]
                       )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

#ax1.plot(tToIntegrateUnique*1e6, 1-odeintresultUnique[:,0])
ax1.plot(tToIntegrateUnique, odeintresultUnique[:,3])
ax1.set_xlabel('Time ($\mu$s)')
ax1.set_ylabel('Excitation probability')

ax2.plot(detunings_atomic, detunings_y)
label = r'Voigt fit, $\nu_0$ = {:.1f} kHz, $\Gamma_\mathrm{{L}}$ = {:.0f} kHz, $\Gamma_\mathrm{{G}}$ = {:.0f} kHz'.format(popt[0]*1e-3, popt[3]*1e-3, popt[4]*1e-3)
ax2.plot(detunings_atomic, fitfunc.Voigt(detunings_atomic, *popt), '--', label=label)
print(popt)
#ax2.set_ylabel('Excitation probability')
ax2.set_ylabel('Photons emitted')
ax2.set_xlabel('Atomic detuning in lab frame (Hz)')

ax2.legend(loc='lower left')

ax1.set_title(trans_label + ' ' + exc_label + ' excitation, $w_0$ = {:.2} mm, $P$ = {:.2f} W, $v$ = {:.0f} m/s'.format(w0*1e3, power_laser, velocity))
plt.tight_layout(pad=1.5)

filePrefix = trans_label + ' ' + exc_label + ' exc, w0={:.2}mm, P={:.2f}W, v={:.0f}mps'.format(w0*1e3, power_laser, velocity)
plt.savefig(plotFolder + filePrefix + '.pdf')
plt.savefig(plotFolder + filePrefix + '.png', dpi=300)