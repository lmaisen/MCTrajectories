# -*- coding: utf-8 -*-
"""
Created on Wed May 11 22:01:32 2016

@author: Lothar Maisenbacher/MPQ

Numerical integration of optical Bloch equations describing excitation of
1S-2S two-photon transition, following description in
Haas et al., Phys. Rev. A. 73, 052501 (2006).
"""

import scipy.constants
import numpy as np
from cpython cimport bool
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as np
# We now need to fix a datatype for our arrays. I've used the variable
# DTYPE for this, which is assigned to the usual NumPy runtime
# type info object.
DTYPE = np.double
# "ctypedef" assigns a corresponding compile-time type to DTYPE_t. For
# every type in the numpy module there's a corresponding compile-time
# type with a _t-suffix.
ctypedef np.double_t DTYPE_t
# "def" can type its arguments but not have a return type. The type of the
# arguments for a "def" function is checked at run-time when entering the
# function.

from libc.math cimport sqrt
from libc.math cimport exp
from libc.math cimport M_PI

class OBE_1S2S:

    def __init__(self, double m_n):

        cdef np.ndarray[DTYPE_t, ndim=1] startVector, velVector
        cdef double beta_ge, beta_ioni, gamma_s, beta_AC_g, beta_AC_e, m_e, m_r, m_r_corr, nu_eg, lambda_laser, power_laser, atomic_detuning, w0, z_waist
        cdef int dopplerOn, ACStarkOn, ionizationOn

        # Isotope dependent correction due to reduced mass
        m_e = 5.48579909065e-4 # Electron mass in u (from CODATA 2018)
        m_r = m_n * m_e / (m_n + m_e)
        m_r_corr = (m_e/m_r)**3

        # OBE coefficients
        beta_ge = 3.68111e-5 * m_r_corr
        beta_ioni = 1.20208e-4 * m_r_corr
        # 2S lifetime, from S. Klarsfeld, Physics Letters A 30A, 382 (1969)
        gamma_s = 8.2283
        beta_AC_g = -2.67827e-5 * m_r_corr
        beta_AC_e = 1.39927e-4 * m_r_corr

        # Experimental parameters
        nu_eg = 2466.061413e12 # Approx. 1S-2S transition frequency (in Hz)
        lambda_laser = 2 * scipy.constants.c / nu_eg # Laser wavelength (in m)
        power_laser = 1 # Intracavity laser power per direction (in W)
        atomic_detuning = 0 # Atomic detuning at the transition frequency (in Hz)
        w0 = 0.000294717 # Cavity beam waist size (1/e^2 intensity radius) (in m)
        z_waist = -0.068 # Position of cavity beam waist relative to nozzle (in m)

        # Trajectory
        startVector = np.array([0, 0, 0], dtype=np.double)
        velVector = np.array([0, 0, 0], dtype=np.double)

        # Switches
        dopplerOn = 1
        ACStarkOn = 1
        ionizationOn = 1

        self.startVector = startVector
        self.velVector = velVector
        self.beta_ge = beta_ge
        self.beta_ioni = beta_ioni
        self.gamma_s = gamma_s
        self.beta_AC_g = beta_AC_g
        self.beta_AC_e = beta_AC_e
        self.m_e = m_e
        self.m_n = m_n
        self.m_r = m_r
        self.m_r_corr = m_r_corr
        self.nu_eg = nu_eg
        self.lambda_laser = lambda_laser
        self.power_laser = power_laser
        self.atomic_detuning = atomic_detuning
        self.w0 = w0
        self.z_waist = z_waist
        self.dopplerOn = dopplerOn
        self.ACStarkOn = ACStarkOn
        self.ionizationOn = ionizationOn

    def posAtTime(self, double t):

        cdef np.ndarray[DTYPE_t, ndim=1] posVector, startVector, velVector
        startVector = self.startVector
        velVector = self.velVector
        posVector = self.startVector + t * self.velVector
        return posVector

    def intensityAtPos(self, np.ndarray[DTYPE_t, ndim=1] pos):

        cdef double zr, w0, lambda_laser, wz2, z_waist, intensity
        w0 = self.w0
        lambda_laser = self.lambda_laser
        z_waist = self.z_waist
        power_laser = self.power_laser

        # Beam waist at position posVector, relative to nozzle
        zr = M_PI * w0**2 / lambda_laser
        wz2 = w0**2 * (1 + ((pos[2]-z_waist)/zr)**2)
        # Average intensity, which is twice the intensity per beam, given by
        # 2 * power_laser / (pi w_z^2)
        intensity = 4 * power_laser / (M_PI*wz2) * exp(-2*(pos[1]**2+pos[0]**2)/wz2)
        return intensity

    # OBEs for 1S-nS excitation
    def derivs(self, np.ndarray[DTYPE_t, ndim=1] y, double t):

        cdef np.ndarray[DTYPE_t, ndim=1] velVector, posVector
        cdef double velVectorNorm, beta_ge, beta_ioni, gamma_s, beta_AC_e, beta_AC_g, intensity, omega, gamma_i, dopplerShift, ACStarkShift, domega, nu_eg, atomic_detuning
        cdef int dopplerOn, ACStarkOn, ionizationOn

        dopplerOn = self.dopplerOn
        ACStarkOn = self.ACStarkOn
        ionizationOn = self.ionizationOn

        ## Coefficients
        beta_ge = self.beta_ge
        beta_ioni = self.beta_ioni
        gamma_s = self.gamma_s
        beta_AC_e = self.beta_AC_e
        beta_AC_g = self.beta_AC_g

        # Beam intensity
        posVector = self.posAtTime(t)
        intensity = self.intensityAtPos(posVector)

        # Rabi frequency
        omega = 2*(2*M_PI*beta_ge)*intensity

        # Ionization coefficient
        gamma_i = ionizationOn * 2*M_PI*beta_ioni*intensity

        ## Detuning
        nu_eg = self.nu_eg
        velVector = self.velVector
        # Atomic detuning from on-resonance frequency
        # This differs by a factor of two from the laser detuning at 243 nm (plus corrections)
        atomic_detuning = self.atomic_detuning
        velVectorNorm = 0
        for i in range(3):
            velVectorNorm = velVectorNorm + velVector[i] * velVector[i]
        velVectorNorm = sqrt(velVectorNorm)
        # Second-order Doppler shift, as given in Eq. 12 in Haas et al.
        dopplerShift = nu_eg/2*(np.linalg.norm(velVector)/scipy.constants.c)**2
        # Detuning from atomic resonance, as given in Eq. 11 in Haas et al.
        # ac-Stark shift detuning
        ACStarkShift = - (beta_AC_e - beta_AC_g) * intensity
        # Detuning in atom's rest frame
        domega = 2*M_PI * (atomic_detuning  + dopplerOn * dopplerShift + ACStarkOn * ACStarkShift)

        ## Derivatives
        # As given in Eq. 10 in Haas et al.
        cdef np.ndarray[DTYPE_t, ndim=1] dydt = np.zeros(4, dtype=DTYPE)
        dydt[0] = - omega * y[2] + gamma_s * y[3]
        dydt[1] = domega * y[2] - (gamma_i + gamma_s)/2 * y[1]
        dydt[2] = - domega * y[1] + omega/2 * (y[0]-y[3]) - (gamma_i + gamma_s)/2 * y[2]
        dydt[3] = omega * y[2] - (gamma_i + gamma_s) * y[3]

        return dydt

    def derivsDecayOnly(self, double y, double t):

        cdef double gamma_s
        gamma_s = self.gamma_s
        return -gamma_s * y

    def decayOnly(self, np.ndarray[DTYPE_t, ndim=1] population, np.ndarray[DTYPE_t, ndim=1] dt):

        cdef double gamma_s
        gamma_s = self.gamma_s
        for i in range(len(population)):
            population[i] = population[i] * exp(-gamma_s * dt[i])
        return population

    # Same as derivs, but without decay back to the initial state
    # This is a reasonable approximation for 2S-nS/D excitation,
    # where only a small fraction decays back to the 2S initial state
    def derivs2SnP(self, np.ndarray[DTYPE_t, ndim=1] y, double t):

        cdef np.ndarray[DTYPE_t, ndim=1] velVector, posVector
        cdef double velVectorNorm, beta_ge, beta_ioni, gamma_s, beta_AC_e, beta_AC_g, intensity, omega, gamma_i, dopplerShift, ACStarkShift, domega, nu_eg, atomic_detuning
        cdef int dopplerOn, ACStarkOn, ionizationOn

        dopplerOn = self.dopplerOn
        ACStarkOn = self.ACStarkOn
        ionizationOn = self.ionizationOn

        ## Coefficients
        beta_ge = self.beta_ge
        beta_ioni = self.beta_ioni
        gamma_s = self.gamma_s
        beta_AC_e = self.beta_AC_e
        beta_AC_g = self.beta_AC_g

        # Beam intensity
        posVector = self.posAtTime(t)
        intensity = self.intensityAtPos(posVector)

        # Rabi frequency
        omega = 2*(2*M_PI*beta_ge)*intensity

        # Ionization coefficient
        gamma_i = ionizationOn * 2*M_PI*beta_ioni*intensity

        ## Detuning
        nu_eg = self.nu_eg
        velVector = self.velVector
        # Atomic detuning from on-resonance frequency
        # This differs by a factor of two from the laser detuning at 243 nm (plus corrections)
        atomic_detuning = self.atomic_detuning
        velVectorNorm = 0
        for i in range(3):
            velVectorNorm = velVectorNorm + velVector[i] * velVector[i]
        velVectorNorm = sqrt(velVectorNorm)
        # Second-order Doppler shift, as given in Eq. 12 in Haas et al.
        dopplerShift = nu_eg/2*(np.linalg.norm(velVector)/scipy.constants.c)**2
        # Detuning from atomic resonance, as given in Eq. 11 in Haas et al.
        # ac-Stark shift detuning
        ACStarkShift = - (beta_AC_e - beta_AC_g) * intensity
        # Detuning in atom's rest frame
        domega = 2*M_PI * (atomic_detuning  + dopplerOn * dopplerShift + ACStarkOn * ACStarkShift)

        ## Derivatives
        # As given in Eq. 10 in Haas et al.
        cdef np.ndarray[DTYPE_t, ndim=1] dydt = np.zeros(4, dtype=DTYPE)
        dydt[0] = - omega * y[2]
        dydt[1] = domega * y[2] - (gamma_i + gamma_s)/2 * y[1]
        dydt[2] = - domega * y[1] + omega/2 * (y[0]-y[3]) - (gamma_i + gamma_s)/2 * y[2]
        dydt[3] = omega * y[2] - (gamma_i + gamma_s) * y[3]

        return dydt
