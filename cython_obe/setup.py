# -*- coding: utf-8 -*-
"""
Created on Sun Apr 10 20:17:25 2016

@author: Lothar Maisenbacher/MPQ
"""
# run python setup.py build_ext --inplace

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import numpy
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True # Generate HTML file with annotations

ext_modules = [
    Extension(
        'cOBE_1S2S',
        ['cOBE_1S2S.pyx'],
        include_dirs=[numpy.get_include()],
        define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")],
        )
    ]

setup(
    name= 'Generic model class',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)
