# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 21:11:55 2022

@author: Lothar Maisenbacher/MPQ

Convert legacy trajectory set analysis results.
"""

import pandas as pd
import glob
from pathlib import Path

# pyhs
import pyhs.fit
import pyhs.gen

# pyha
import pyha.trajs

logger = pyhs.gen.set_up_command_line_logger()

Trajs = pyha.trajs.Trajs()

#%% Load trajectory set analysis results

dir_metadata = Path('')

# Search for metadata files in given metadata directory
analysis_filepaths = glob.glob(str(Path(dir_metadata, '* Analysis.dat')))
#metadata_filepaths = metadata_filepaths[-1:]

logger.info('Found %d analysis file(s)', len(analysis_filepaths))

#%% Convert analysis results

for analysis_filepath in analysis_filepaths:

    dfTrajAnalysis = pd.read_csv(
        analysis_filepath, index_col=0)
    if 'TrajUID' in dfTrajAnalysis.columns:
        dfTrajAnalysis.index = dfTrajAnalysis['TrajUID']
    dfTrajAnalysis = Trajs.cast_containers_to_data_format(dfTrajAnalysis, 'dfTrajAnalysis')

    Path(analysis_filepath).rename(analysis_filepath+'.old')

    Trajs.write_df_to_csv(
        dfTrajAnalysis, 'dfTrajAnalysis',
        Path(analysis_filepath))
